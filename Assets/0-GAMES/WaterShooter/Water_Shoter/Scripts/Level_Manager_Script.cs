﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using VacuumShaders.CurvedWorld;
using CodeStage.AntiCheat.Storage;
using CodeStage.AntiCheat.ObscuredTypes;

public class Level_Manager_Script : MonoBehaviour
{
    public static Level_Manager_Script instance;

    GameObject Player;

    public GameObject[] Walls;
    public GameObject[] Jump_Objects;
    public GameObject[] Enemy_Objects;
    public GameObject[] Power_Ups;
    public GameObject[] Water_Baloons;
    public GameObject[] Hard_Barrier_Objects;
    public GameObject[] Bonus_Character_Buttons;
    public GameObject[] Bonus_Characters;
    public GameObject[] Bonus_Preview_Characters;


    public GameObject Start_Panel;
    public GameObject Main_Boss_Characters_Preview;
    public GameObject Main_Character_Preview;
    public GameObject Bonus_HighScore_Texts;
    public GameObject Magnet;
    public GameObject Jump_Boots;
    public GameObject Slower;
    public GameObject Play_Panel;
    public GameObject Shop_Panel;
    public GameObject HighScore_Text;
    public GameObject Water_Baloon_Count_Text;
    public GameObject Need_Water_Baloon_Text;
    public GameObject Bonus_Timer_Object;
    public GameObject Tutorial_Image;
    public GameObject Watch_Adds_Button;
    public GameObject Adds_Panel;
    public GameObject Boss_Turn_Effect;
    public GameObject Continue_Panel;
    public GameObject Yarisma_Button;
    public GameObject Odul_Button;
    public GameObject Voice_Turn_On;
    public GameObject Voice_Turn_Off;
    public GameObject Pause_Panel;
    public GameObject Baloon_Error;
    public GameObject Confetti;
    public GameObject Play_Buttton;
    public GameObject Watch_Adds_ForCharacters_Button;
    public GameObject Back_Button_Object;
    public GameObject Last_Life_Image;
    public GameObject Extra_Life_Button;
    public GameObject Gift_Object;
    public GameObject Gift_Button;
    public GameObject Gift_Text;

    public Text High_Score_Text;
    public Text High_Score_Index_Text;
    public Text High_Score_InGame_Index_Text;
    public Text Extra_Life_Text;

    public Text nicknameText;

    public AudioClip new_Track_Boss;
    public AudioClip old_Track;

    private AudioManager audio_man;

    public bool Is_Start;
    public bool Is_Click_Shop_Button;
    public bool Is_Take_Bigger_Bonus;
    public bool Is_Passed_Tutorial;
    public bool Is_Wall_Can_Spawn;
    public bool Is_Take_Jumper;
    public bool Is_Camera_Can_Move;

    public float Power_Ups_Time_Jumper = 8f;
    public float Power_Ups_Time_Magnet = 8f;
    public float Power_Ups_Time_Slower = 8f;
    public float Power_Up_Jump_Force = 8f;
    public float Left = -5f;
    public float Right = 5f;
    public float Middle = 0f;
    public float Up = 1f;
    public float Spawn_Time = 0.3f;
    public float Bonus_Timer = 10;
    public float car_Speed = 100f;
    public float Current_Run_Speed;

    public ObscuredInt High_Score_Count;
    public int Water_Baloons_Int = 0;
    public int Needs_Water_Baloon_Count = 0;

    private float Power_Up_Time_Jumper;
    private float Power_Up_Time_Magnet;
    private float Power_Up_Time_Slower;
    private float Bonus_Timer_Left;
    private float Player_Jump;
    private float Score_Counter_Speed = 2f;

    private int Tutorial_Index;
    private int Bonus_Character_Index;
    private int High_Score_Index;
    private int Water_Shooter_Audio_Index;
    private int Game_Timer = 1;
    private int Take_A_Extra_Life_Count;

    private bool Is_CliCK_Bonus_Button;

    public GameObject popUpMenu;

    public Text newGameTimerText;
    private static int rateUsCounter = 0;
    void Awake()
    {
        instance = this;
        Player = GameObject.Find("Main_Player");
        Tutorial_Index = PlayerPrefs.GetInt("Tutorial_Index");
        High_Score_Index = ObscuredPrefs.GetInt("High_Score_Index");
        audio_man = FindObjectOfType<AudioManager>();

        /*
        if (!PlayerPrefs.HasKey("popup_watershooter"))
        {
            popUpMenu.SetActive(true);
            PlayerPrefs.SetInt("popup_watershooter", 1);
        }
        */

        rateUsCounter++;

        if(rateUsCounter == 4)
        {
            if (!PlayerPrefs.HasKey("NeverRateUs"))
                rateUsPanel.SetActive(true);
        }
    }
    void Start()
    {
        ReklamScript.BannerGoster();

        InvokeRepeating("Random_Spawner", 0, Spawn_Time);
        //InvokeRepeating("Car_Move", 0, 0.03f);
        InvokeRepeating("HighScore_Counter", 0, Score_Counter_Speed);
        //InvokeRepeating("Create_Walls", 0, 10f);
        //InvokeRepeating("Creat_Power_Ups", 0, 20f);
        High_Score_Index_Text.text = High_Score_Index.ToString("");
        High_Score_InGame_Index_Text.text = High_Score_Index.ToString("");
        Bonus_Timer_Left = Bonus_Timer;
        if (Tutorial_Index > 0)
        {
            Is_Passed_Tutorial = true;
        }
        Is_CliCK_Bonus_Button = false;
        Is_Wall_Can_Spawn = true;
        Player_Jump = Player_Script.instance.Jump_force;
        Time.timeScale = 1f;

        nicknameText.text = PlayerPrefs.GetString("username", "PLAYER");

        Water_Baloons_Int = 15;
        Water_Baloon_Count_Text.GetComponent<Text>().text = Water_Baloons_Int.ToString("");
        Check_Audio();
        //Object_Spawner(Water_Baloons[Random.Range(0, Water_Baloons.Length)], Middle, Player.transform.position.z + 750);

        //TimeManager.Instance.RemainTime();
    }

    public void Object_Spawner(GameObject Spawn_Object, float x_Axis , float z_Axis)
    {
        GameObject newObject = Instantiate(Spawn_Object, new Vector3(x_Axis, Spawn_Object.transform.position.y, z_Axis), Spawn_Object.transform.rotation);

        Destroy(newObject, 60f);
    }
    public void Game_Timer_Func()
    {
        if (Is_Start == true && Is_Passed_Tutorial == true)
        {
            Game_Timer++;
        }
    }
    public void Random_Spawner()
    {
        if (Is_Start == true && Is_Passed_Tutorial == true)
        {
            Game_Timer++;
            Debug.Log(Game_Timer);
            if (Game_Timer % 120 != 0)
            {
                if (Game_Timer % 100 != 0)
                {
                    if (Game_Timer % 65 != 0)
                    {
                        int random_number = Random.Range(9, 30);

                        /*if (random_number > 0 && random_number < 3)
                        {
                            Object_Spawner(Water_Baloons[Random.Range(0, Water_Baloons.Length)], Left , Player.transform.position.z + 750);
                        }
                        if (random_number > 3 && random_number < 6)
                        {
                            Object_Spawner(Water_Baloons[Random.Range(0, Water_Baloons.Length)], Right , Player.transform.position.z + 750);
                        }
                        if (random_number > 6 && random_number < 9)
                        {
                            Object_Spawner(Water_Baloons[Random.Range(0, Water_Baloons.Length)], Middle , Player.transform.position.z + 750);
                        }*/

                        if (random_number > 9 && random_number < 18)
                        {
                            Object_Spawner(Enemy_Objects[Random.Range(0, Enemy_Objects.Length)], Left, Player.transform.position.z + 450);
                        }
                        if (random_number > 18 && random_number < 27)
                        {
                            Object_Spawner(Enemy_Objects[Random.Range(0, Enemy_Objects.Length)], Right, Player.transform.position.z + 450);
                        }
                        if (random_number > 22 && random_number < 30)
                        {
                            Object_Spawner(Enemy_Objects[Random.Range(0, Enemy_Objects.Length)], Middle, Player.transform.position.z + 450);
                        }

                        /*if (random_number > 30 && random_number < 35)
                        {
                            Object_Spawner(Jump_Objects[Random.Range(0, Jump_Objects.Length)], Middle,  Player.transform.position.z + 450);
                        }*/
                    }
                    else
                    {
                        Object_Spawner(Hard_Barrier_Objects[Random.Range(0, Hard_Barrier_Objects.Length)], Middle, Player.transform.position.z + 450);
                    }
                }
                else
                {
                    Creat_Power_Ups();
                }
            }
            else
            {
                Creat_Gift();
            }
        }
    }
    public void Create_Walls()
    {
        if (Is_Start == true && Is_Passed_Tutorial == true)
        {
            if (Is_Wall_Can_Spawn == true)
            {
                if (Is_Take_Bigger_Bonus == false)
                {
                    Object_Spawner(Walls[Random.Range(0, Walls.Length)], Middle, Player.transform.position.z + 1500);
                    Is_Wall_Can_Spawn = false;
                }
            }
        }
    }
    public void Creat_Power_Ups()
    {
        int random_number_1 = Random.Range(0, 9);

        if (random_number_1 > 0 && random_number_1 < 3)
        {
            if (Is_Take_Bigger_Bonus == false)
            {
                Object_Spawner(Power_Ups[Random.Range(0, Power_Ups.Length)], Left, Player.transform.position.z + 450);
            }
        }

        if (random_number_1 > 3 && random_number_1 < 6)
        {
            if (Is_Take_Bigger_Bonus == false)
            {
                Object_Spawner(Power_Ups[Random.Range(0, Power_Ups.Length)], Right, Player.transform.position.z + 450);
            }
        }
        if (random_number_1 > 6 && random_number_1 < 9)
        {
            if (Is_Take_Bigger_Bonus == false)
            {
                Object_Spawner(Power_Ups[Random.Range(0, Power_Ups.Length)], Middle, Player.transform.position.z + 450);
            }
        }
    }
    public void Creat_Gift()
    {
        int random_number_2 = Random.Range(0, 9);

        if (random_number_2 > 0 && random_number_2 < 3)
        {
            if (Is_Take_Bigger_Bonus == false)
            {
                Object_Spawner(Gift_Object, Left, Player.transform.position.z + 450);
            }
        }
        if (random_number_2 > 3 && random_number_2 < 6)
        {
            if (Is_Take_Bigger_Bonus == false)
            {
                Object_Spawner(Gift_Object, Right, Player.transform.position.z + 450);
            }
        }
        if (random_number_2 > 6 && random_number_2 < 9)
        {
            if (Is_Take_Bigger_Bonus == false)
            {
                Object_Spawner(Gift_Object, Middle, Player.transform.position.z + 450);
            }
        }
    }
    public void Car_Move()
    {
        foreach(GameObject cars in GameObject.FindGameObjectsWithTag("Cars"))
        {
            cars.transform.Translate(0, 0, car_Speed * Time.deltaTime);
        }
    }
    public void Start_Power_Up_Jumper()
    {
        Power_Up_Time_Jumper = Power_Ups_Time_Jumper;
        InvokeRepeating("Power_Ups_Timer_Jumper", 0 , 1f);
    }
    public void Start_Power_Up_Magnet()
    {
        Power_Up_Time_Magnet = Power_Ups_Time_Magnet;
        InvokeRepeating("Power_Ups_Timer_Magnet", 0, 0.01f);
    }
    public void Start_Power_Up_Slower()
    {
        Power_Up_Time_Slower = Power_Ups_Time_Slower;
        //Score_Counter_Speed = 0.01f;
        InvokeRepeating("Power_Ups_Timer_Slower", 0, 1f);
    }
    public void Power_Ups_Timer_Jumper()
    {
        Power_Up_Time_Jumper --;
        Player_Script.instance.Jump_force = Power_Up_Jump_Force * 50f * Time.deltaTime;
        Jump_Boots.SetActive(true);
        Player.gameObject.GetComponent<TrailRenderer>().enabled = true;
        Is_Take_Jumper = true;
        if (Power_Up_Time_Jumper < 1)
        {
            Stop_Power_Up_Timer_Jumper();
        }
    }
    public void Power_Ups_Timer_Magnet()
    {
        Power_Up_Time_Magnet -= Time.deltaTime;
        Magnet.SetActive(true);
        foreach (GameObject water_baloons in GameObject.FindGameObjectsWithTag("Water_Baloon"))
        {
            Vector3 directions = water_baloons.transform.position - Player.transform.position;
            if (directions.magnitude <= 55f)
            {
                water_baloons.transform.position = Vector3.Slerp(water_baloons.transform.position,
                    new Vector3(Player.transform.position.x, water_baloons.transform.position.y, Player.transform.position.z + 1f), 15 * Time.deltaTime);
            }
        }
        if (Power_Up_Time_Magnet < 1)
        {
            Stop_Power_Up_Timer_Magnet();
        }
    }
    public void Power_Ups_Timer_Slower()
    {
        Power_Up_Time_Slower--;
        Slower.SetActive(true);
        Time.timeScale = 0.7f;
        if (Power_Up_Time_Slower <= 0)
        {
            Stop_Power_Up_Timer_Slower();
        }
    }
    public void Stop_Power_Up_Timer_Jumper()
    {
        Player.gameObject.GetComponent<TrailRenderer>().enabled = false;
        Jump_Boots.SetActive(false);
        Player_Script.instance.Jump_force = Player_Jump;
        Is_Wall_Can_Spawn = true;
        Is_Take_Jumper = false;
        CancelInvoke("Power_Ups_Timer_Jumper");
    }
    public void Stop_Power_Up_Timer_Magnet()
    {
        Magnet.SetActive(false);
        CancelInvoke("Power_Ups_Timer_Magnet");
    }
    public void Stop_Power_Up_Timer_Slower()
    {
        Slower.SetActive(false);
        //Score_Counter_Speed = 0.1f;
        Time.timeScale = 1f;
        CancelInvoke("Power_Ups_Timer_Slower");
    }
    public void HighScore_Counter()
    {
        if (Is_Start == true && Is_Passed_Tutorial == true)
        {
            High_Score_Count++;
            HighScore_Text.GetComponent<Text>().text = High_Score_Count.ToString("");
            High_Score_Text.text = High_Score_Count.ToString("");

            if (High_Score_Count % 17 == 0)
            {
                //        if (VacuumShaders.CurvedWorld.CurvedWorld_Controller.get != null)  

            //    firstCurve = VacuumShaders.CurvedWorld.CurvedWorld_Controller.get.GetBend();
            //    aimCurve = new Vector2(Random.Range(-1.0f, 0.5f), Random.Range(-1.0f, 1.0f));
            //    curveDegıstır = true;
            //        VacuumShaders.CurvedWorld.CurvedWorld_Controller.get.SetBend(Vector2.Lerp(VacuumShaders.CurvedWorld.CurvedWorld_Controller.get.GetBend(), new Vector2(Random.Range(-1.0f, 0.5f), Random.Range(-1.0f, 1.0f)),4f * Time.deltaTime));

               CurvedWorld();
            }
            if(High_Score_Count % 1000 == 0)
            {
                GameObject newObject = Instantiate(Confetti, new Vector3( 8, Confetti.transform.position.y, Player.transform.position.z + 200), Quaternion.identity);
                GameObject newObject1 = Instantiate(Confetti, new Vector3(- 8, Confetti.transform.position.y, Player.transform.position.z + 200), Quaternion.identity);
                Destroy(newObject.gameObject, 5f);
                Destroy(newObject1.gameObject, 5f);
            }
        }
    }

    private Vector2 firstCurve, aimCurve;
    private bool curveDegıstır = false;
    private Vector2 shadercurve;
    public void CurvedWorld()
    {
        if (VacuumShaders.CurvedWorld.CurvedWorld_Controller.get != null)
        {
            curveDegıstır = true;

            shadercurve = VacuumShaders.CurvedWorld.CurvedWorld_Controller.get.GetBend();

            print(shadercurve);


            DOTween.To(() => shadercurve, x => shadercurve = x, new Vector2(Random.Range(-0.25f, 0.3f), Random.Range(-0.4f, 0.4f)), 7f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                curveDegıstır = false;
                print("değişti");

            });

            VacuumShaders.CurvedWorld.CurvedWorld_Controller.get.SetBend(shadercurve);

        }
    }

    private void Update()
    {
        if (curveDegıstır)
        {
            /*
           VacuumShaders.CurvedWorld.CurvedWorld_Controller.get.SetBend(
               Vector2.Lerp(firstCurve, 
               aimCurve,
               4f * Time.deltaTime));

            Invoke("Stopp", 4f);
            */

            VacuumShaders.CurvedWorld.CurvedWorld_Controller.get.SetBend(shadercurve);

        }

        newGameTimerText.text = TimeManager.Instance.GetRemainingTimeText();

    }
    public void Stopp()
    {
        curveDegıstır = false;
    }

    public void Click_Play_Button()
    {
   //     ReklamScript.BannerGizle();

        Is_Start = true;
        Is_Click_Shop_Button = false;
        Is_Camera_Can_Move = true;
        Play_Panel.SetActive(false);
        Back_Button_Object.SetActive(false);
        if (Is_Passed_Tutorial == false)
        {
            Tutorial_Image.SetActive(true);
        }
        if (Is_CliCK_Bonus_Button == true)
        {
            Bonus_Character_Buttons[Bonus_Character_Index].SetActive(true);
        }
        else
        {
            foreach(GameObject bonus_buttons in Bonus_Character_Buttons)
            {
                bonus_buttons.SetActive(false);
            }
        }

        Adds_Panel.SetActive(false);
    }
    public void Click_Shop_Button()
    {
        Is_Start = false;
        Is_Click_Shop_Button = true;
        Play_Panel.SetActive(false);
        Shop_Panel.SetActive(true);
    }
    public void Start_Bonus_Timer()
    {  
        Is_Take_Bigger_Bonus = true;
        InvokeRepeating("Bonus", 0, 0.1f);
        foreach (GameObject walls in GameObject.FindGameObjectsWithTag("Walls"))
        {
            Destroy(walls);
        }
        foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
        {
            Destroy(power_ups);
        }
    }
    public void Bonus()
    {
        if (Is_Take_Bigger_Bonus)
        {
            if (Bonus_Timer_Left >= 0)
            {
                if (new_Track_Boss != null)
                    audio_man.Change_BGM(new_Track_Boss);

                Player_Script.instance.Run_Speed = Current_Run_Speed + 20;
                Bonus_Characters[Bonus_Character_Index].GetComponent<Animator>().Play("Sprint");
                Bonus_Timer_Left -= Time.deltaTime;
                Bonus_Timer_Object.GetComponent<Image>().fillAmount = Bonus_Timer_Left / Bonus_Timer;
                Bonus_Timer_Object.SetActive(true);
                Bonus_Characters[Bonus_Character_Index].SetActive(true);
                Player.transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.SetActive(false);
                Camera_Script.instance.StartCameraShaking();
            }
            else
            {
                if (old_Track != null)
                    audio_man.Change_BGM(old_Track);

                Is_Wall_Can_Spawn = true;
                Player_Script.instance.Run_Speed = Current_Run_Speed;  
                Bonus_Timer_Object.SetActive(false);
                Bonus_Characters[Bonus_Character_Index].SetActive(false);
                Player.transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.SetActive(true);
                Is_Take_Bigger_Bonus = false;
                Bonus_Timer_Object.GetComponent<Image>().fillAmount = 1;
                Camera_Script.instance.StopCameraShaking();
                Player.transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.GetComponent<Animator>().Play("Run");
                GameObject new_Partical = Instantiate(Boss_Turn_Effect, new Vector3(Player.transform.position.x,
                    Player.transform.position.y, Player.transform.position.z + 2f), Quaternion.identity);
                new_Partical.transform.parent = Player.transform;
                Destroy(new_Partical, 4f);
                CancelInvoke("Bonus");
            }
        }
    }
    public void Bonus_Character_Button_1()
    {
        Is_CliCK_Bonus_Button = true;
        Bonus_Character_Index = 0;

        Bonus_Preview_Characters[0].SetActive(true);
        Bonus_Preview_Characters[1].SetActive(false);
        Bonus_Preview_Characters[2].SetActive(false);
        Bonus_Preview_Characters[3].SetActive(false);
        Bonus_Preview_Characters[4].SetActive(false);
        Bonus_Preview_Characters[5].SetActive(false);

        Play_Panel.SetActive(false);
        Watch_Adds_Button.SetActive(true);

        Main_Character_Preview.SetActive(false);
    }
    public void Bonus_Character_Button_2()
    {
        Is_CliCK_Bonus_Button = true;
        Bonus_Character_Index = 1;

        Bonus_Preview_Characters[0].SetActive(false);
        Bonus_Preview_Characters[1].SetActive(true);
        Bonus_Preview_Characters[2].SetActive(false);
        Bonus_Preview_Characters[3].SetActive(false);
        Bonus_Preview_Characters[4].SetActive(false);
        Bonus_Preview_Characters[5].SetActive(false);

        Play_Panel.SetActive(false);
        Watch_Adds_Button.SetActive(true);


        Main_Character_Preview.SetActive(false);
    }
    public void Bonus_Character_Button_3()
    {
        Is_CliCK_Bonus_Button = true;
        Bonus_Character_Index = 2;

        Bonus_Preview_Characters[0].SetActive(false);
        Bonus_Preview_Characters[1].SetActive(false);
        Bonus_Preview_Characters[2].SetActive(true);
        Bonus_Preview_Characters[3].SetActive(false);
        Bonus_Preview_Characters[4].SetActive(false);
        Bonus_Preview_Characters[5].SetActive(false);

        Play_Panel.SetActive(false);
        Watch_Adds_Button.SetActive(true);

        Main_Character_Preview.SetActive(false);
    }
    public void Bonus_Character_Button_4()
    {
        Is_CliCK_Bonus_Button = true;
        Bonus_Character_Index = 3;

        Bonus_Preview_Characters[0].SetActive(false);
        Bonus_Preview_Characters[1].SetActive(false);
        Bonus_Preview_Characters[2].SetActive(false);
        Bonus_Preview_Characters[3].SetActive(true);
        Bonus_Preview_Characters[4].SetActive(false);
        Bonus_Preview_Characters[5].SetActive(false);

        Play_Panel.SetActive(false);
        Watch_Adds_Button.SetActive(true);

        Main_Character_Preview.SetActive(false);
    }
    public void Bonus_Character_Button_5()
    {
        Is_CliCK_Bonus_Button = true;
        Bonus_Character_Index = 4;

        Bonus_Preview_Characters[0].SetActive(false);
        Bonus_Preview_Characters[1].SetActive(false);
        Bonus_Preview_Characters[2].SetActive(false);
        Bonus_Preview_Characters[3].SetActive(false);
        Bonus_Preview_Characters[4].SetActive(true);
        Bonus_Preview_Characters[5].SetActive(false);

        Play_Panel.SetActive(false);
        Watch_Adds_Button.SetActive(true);

        Main_Character_Preview.SetActive(false);
    }
    public void Bonus_Character_Button_6()
    {
        Is_CliCK_Bonus_Button = true;
        Bonus_Character_Index = 5;

        Bonus_Preview_Characters[0].SetActive(false);
        Bonus_Preview_Characters[1].SetActive(false);
        Bonus_Preview_Characters[2].SetActive(false);
        Bonus_Preview_Characters[3].SetActive(false);
        Bonus_Preview_Characters[4].SetActive(false);
        Bonus_Preview_Characters[5].SetActive(true);

        Play_Panel.SetActive(false);
        Watch_Adds_Button.SetActive(true);

        Main_Character_Preview.SetActive(false);
    }
    public void Watch_Adds()
    {
        ReklamScript.RewardedReklamGoster(null);

        Play_Panel.SetActive(true);
        Watch_Adds_Button.SetActive(false);
        
        Main_Character_Preview.SetActive(true);
        Bonus_Preview_Characters[0].SetActive(false);
        Bonus_Preview_Characters[1].SetActive(false);
        Bonus_Preview_Characters[2].SetActive(false);
        Bonus_Preview_Characters[3].SetActive(false);
        Bonus_Preview_Characters[4].SetActive(false);
        Bonus_Preview_Characters[5].SetActive(false);
    }
    public void Bonus_Character_1()
    {
        Bonus_Character_Index = 0;
        Start_Bonus_Timer();
        Bonus_Character_Buttons[0].SetActive(false);
        GameObject newPartical = Instantiate(Boss_Turn_Effect, new Vector3(Player.transform.position.x,
                    Player.transform.position.y, Player.transform.position.z + 2f), Quaternion.identity);
        newPartical.transform.parent = Player.transform;
        Destroy(newPartical, 2f);
    }
    public void Bonus_Character_2()
    {
        Bonus_Character_Index = 1;
        Start_Bonus_Timer();
        Bonus_Character_Buttons[1].SetActive(false);
        GameObject newPartical = Instantiate(Boss_Turn_Effect, new Vector3(Player.transform.position.x,
                      Player.transform.position.y, Player.transform.position.z + 2f), Quaternion.identity);
        newPartical.transform.parent = Player.transform;
        Destroy(newPartical, 2f);
    }
    public void Bonus_Character_3()
    {
        Bonus_Character_Index = 2;
        Start_Bonus_Timer();
        Bonus_Character_Buttons[2].SetActive(false);
        GameObject newPartical = Instantiate(Boss_Turn_Effect, new Vector3(Player.transform.position.x,
                   Player.transform.position.y, Player.transform.position.z + 2f), Quaternion.identity);
        newPartical.transform.parent = Player.transform;
        Destroy(newPartical, 2f);
    }
    public void Bonus_Character_4()
    {
        Bonus_Character_Index = 3;
        Start_Bonus_Timer();
        Bonus_Character_Buttons[3].SetActive(false);
        GameObject newPartical = Instantiate(Boss_Turn_Effect, new Vector3(Player.transform.position.x,
                    Player.transform.position.y, Player.transform.position.z + 2f), Quaternion.identity);
        newPartical.transform.parent = Player.transform;
        Destroy(newPartical, 2f);
    }
    public void Bonus_Character_5()
    {
        Bonus_Character_Index = 4;
        Start_Bonus_Timer();
        Bonus_Character_Buttons[4].SetActive(false);
        GameObject newPartical = Instantiate(Boss_Turn_Effect, new Vector3(Player.transform.position.x,
                     Player.transform.position.y, Player.transform.position.z + 2f), Quaternion.identity);
        newPartical.transform.parent = Player.transform;
        Destroy(newPartical, 2f);
    }
    public void Bonus_Character_6()
    {
        Bonus_Character_Index = 5;
        Start_Bonus_Timer();
        Bonus_Character_Buttons[5].SetActive(false);
        GameObject newPartical = Instantiate(Boss_Turn_Effect, new Vector3(Player.transform.position.x,
                   Player.transform.position.y, Player.transform.position.z + 2f), Quaternion.identity);
        newPartical.transform.parent = Player.transform;
        Destroy(newPartical, 4f);
    }
    public void Vibrations()
    {
        Handheld.Vibrate();
    }

    public GameObject ForceUpdatePanel;
    public void Start_Panels()
    {
        /*
        if (GameManager.Instance.newVersion > GameManager.Instance.currentVersion)
        {
            ForceUpdatePanel.SetActive(true);
        }
        else
        {
            Start_Panel.SetActive(false);
        }*/

        Start_Panel.SetActive(false);
    }
    public void Guncelle()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.MedusaGames.GameofWinners");
    }
    public void Click_Yarisma_Button()
    {
        Yarisma_Button.SetActive(false);
        Odul_Button.SetActive(true);
    }
    public void return_back()
    {
        Yarisma_Button.SetActive(true);
        Odul_Button.SetActive(false);
    }
    public void Close_Voice()
    {
        Water_Shooter_Audio_Index = 1;
        Voice_Turn_Off.SetActive(true);
        Voice_Turn_On.SetActive(false);
        audio_man.GetComponent<AudioSource>().volume = 0f;
        PlayerPrefs.SetInt("Check_Audio", Water_Shooter_Audio_Index);
    //    FindObjectOfType<AudioManager>().gameObject = false;
    }
    public void Open_Voice()
    {
        Water_Shooter_Audio_Index = 0;
        Voice_Turn_Off.SetActive(false);
        Voice_Turn_On.SetActive(true);
        audio_man.GetComponent<AudioSource>().volume = 1f;
        PlayerPrefs.SetInt("Check_Audio", Water_Shooter_Audio_Index);
        //    FindObjectOfType<AudioManager>().enabled = true;
    }
    public void Check_Audio()
    {
        Water_Shooter_Audio_Index = PlayerPrefs.GetInt("Check_Audio");

        if (Water_Shooter_Audio_Index == 0)
        {
            audio_man.GetComponent<AudioSource>().volume = 1f;
            Voice_Turn_Off.SetActive(false);
            Voice_Turn_On.SetActive(true);
        }
        else if(Water_Shooter_Audio_Index == 1)
        {
            audio_man.GetComponent<AudioSource>().volume = 0f;
            Voice_Turn_Off.SetActive(true);
            Voice_Turn_On.SetActive(false);
        }
    }
    public void Rewarded_Extra_Life()
    {
        ReklamScript.RewardedReklamGoster(Extra_Life_Func);
    }
    public void Extra_Life_Func(GoogleMobileAds.Api.Reward reward)
    {
        if (Player_Script.instance.Extra_Life <= 0)
        {
            Player_Script.instance.Extra_Life++;
            Extra_Life_Text.text = Player_Script.instance.Extra_Life.ToString("");
            Time.timeScale = 1f;
            Extra_Life_Button.SetActive(false);
            //Start_Panels();
            //Click_Play_Button();
        }
    }
    public void Exit_Extra_Life_Button_Func()
    {
        Extra_Life_Button.SetActive(false);
        Time.timeScale = 1f;
    }
    public void Less_Extra_Life()
    {
        if (Player_Script.instance.Extra_Life > 0)
        {
            Last_Life_Image.SetActive(false);
            Player_Script.instance.Extra_Life--;
            Extra_Life_Text.text = Player_Script.instance.Extra_Life.ToString("");
            if (Player_Script.instance.Extra_Life <= 0)
            {
                Last_Life_Image.SetActive(true);
            }
        }
    }
    public void Back_Button()
    {
        Start_Panel.SetActive(true);
    }
    public void Restart()
    {
        var deadCounter = PlayerPrefs.GetInt("deadCounter_WaterShooter");

        if (deadCounter == 2)
        {
            ReklamScript.InsterstitialGoster();
            PlayerPrefs.SetInt("deadCounter_WaterShooter", 0);
        }
        else
        {
            PlayerPrefs.SetInt("deadCounter_WaterShooter", deadCounter + 1);
        }

        ReklamScript.BannerGoster();

        if (High_Score_Count > High_Score_Index)
        {
            ObscuredPrefs.SetInt("High_Score_Index", High_Score_Count);
            //if(FireStoreManager.Instance != null) FireStoreManager.Instance.CreateWaterShooterScore();
            if(FireStoreManager.Instance != null) FireStoreManager.Instance.CreateCurrentGameScore();
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void Pause_Restart()
    {
        ReklamScript.BannerGoster();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void Pause()
    {
        Pause_Panel.SetActive(true);
        Time.timeScale = 0f;
    }
    public void Continue_Game_OnPause()
    {
        //     ReklamScript.BannerGizle();
        Time.timeScale = 1f;
        Pause_Panel.SetActive(false);
    }
    public void Close_Gift_Button()
    {
        Time.timeScale = 1f;
        Gift_Button.SetActive(false);
    }

    public void continue_game()
    {
        ReklamScript.RewardedReklamGoster(continue_game_reward);
    }
    public void continue_game_reward(GoogleMobileAds.Api.Reward reward)
    {
        Time.timeScale = 1f;
        Pause_Panel.SetActive(false);
    }
    public void Gift_Func_Reward()
    {
        ReklamScript.RewardedReklamGoster(Gift_Func);
    }
    public void Gift_Func(GoogleMobileAds.Api.Reward reward)
    {
        int random_number = Random.Range(0, 50);

        if (random_number > 0 && random_number < 10)
        {
            if (Take_A_Extra_Life_Count <= 0)
            {
                Player_Script.instance.Extra_Life++;
                Gift_Text.SetActive(true);
                Extra_Life_Text.text = Player_Script.instance.Extra_Life.ToString("");
                Gift_Text.GetComponent<Text>().text = "TEBRİKLER +1 CAN KAZANDIN!!!";
                Gift_Text.GetComponent<Animator>().Play("Move");
                Gift_Button.SetActive(false);
                Take_A_Extra_Life_Count++;
                Time.timeScale = 1;
            }
            else
            {
                High_Score_Count += 25;
                Gift_Text.SetActive(true);
                Gift_Text.GetComponent<Text>().text = "TEBRİKLER +25 PUAN KAZANDIN!!!";
                Gift_Text.GetComponent<Animator>().Play("Move");
                Gift_Button.SetActive(false);
                Time.timeScale = 1;
            }
            
        }
        if (random_number > 10 && random_number < 20)
        {
            High_Score_Count += 100;
            Gift_Text.SetActive(true);
            Gift_Text.GetComponent<Text>().text = "TEBRİKLER +100 PUAN KAZANDIN!!!";
            Gift_Text.GetComponent<Animator>().Play("Move");
            Gift_Button.SetActive(false);
            Time.timeScale = 1;
        }
        if (random_number > 20 && random_number < 30)
        {
            High_Score_Count += 250;
            Gift_Text.SetActive(true);
            Gift_Text.GetComponent<Text>().text = "TEBRİKLER +250 PUAN KAZANDIN!!!";
            Gift_Text.GetComponent<Animator>().Play("Move");
            Gift_Button.SetActive(false);
            Time.timeScale = 1;
        }

        if (random_number > 30 && random_number < 40)
        {
            High_Score_Count += 25;
            Gift_Text.SetActive(true);
            Gift_Text.GetComponent<Text>().text = "TEBRİKLER +25 PUAN KAZANDIN!!!";
            Gift_Text.GetComponent<Animator>().Play("Move");
            Gift_Button.SetActive(false);
            Time.timeScale = 1;
        }
        if (random_number > 40 && random_number < 50)
        {
            High_Score_Count += 75;
            Gift_Text.SetActive(true);
            Gift_Text.GetComponent<Text>().text = "TEBRİKLER +75 PUAN KAZANDIN!!!";
            Gift_Text.GetComponent<Animator>().Play("Move");
            Gift_Button.SetActive(false);
            Time.timeScale = 1;

        }
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(enemy.gameObject);
        }

    }

    public GameObject oneToTenPrizesPanel;
    public void OdullerButton()
    {
        oneToTenPrizesPanel.SetActive(true);
    }

    public Transform prizesParent;
    public void PrizeButtons(int i)
    {
        prizesParent.GetChild(i).gameObject.SetActive(true);
    }

    public GameObject MenuPanel;
    public GameObject RankPanel;
    public GameObject rankForWatchAdPanel;
    public void RankButton()
    {
        //    rankForWatchAdPanel.SetActive(true);
    //    ReklamScript.InsterstitialGoster();
        RankReward();
    }
    public void yesWatchAdForRankButton()
    {
        RankPanel.SetActive(true);
        MenuPanel.SetActive(false);
        rankForWatchAdPanel.SetActive(false);

        //if (FireStoreManager.Instance != null) FireStoreManager.Instance.GetCurrentGameRanking(FireStoreManager.Instance.watershooter);

    //    ReklamScript.RewardedReklamGoster(RankReward);
        ReklamScript.RewardedReklamGoster(null);
    }
    public void RankReward() // public void RankReward(GoogleMobileAds.Api.Reward reward)
    {
        RankPanel.SetActive(true);
        MenuPanel.SetActive(false);
        rankForWatchAdPanel.SetActive(false);

        //if (FireStoreManager.Instance != null) FireStoreManager.Instance.GetCurrentGameRanking();
    }

    public void RankBackButton()
    {
        ReklamScript.InsterstitialGoster();
    }

    public GameObject zatenGirisYaptinPanel;
    public void RegisterButton()
    {
        if (!PlayerPrefs.HasKey("LoggedIn"))
        {
            SceneManager.LoadScene("Login New");
        }
        else
        {
            zatenGirisYaptinPanel.SetActive(true);
        }
    }
    public void OpenRegisterScene()
    {
        SceneManager.LoadScene("Login New");
    }


    public GameObject continueButton, multipleScoreButton;
    public void MultipleScoreButton()
    {
        ReklamScript.RewardedReklamGoster(MultipleScore_Reward);
    }
    public void MultipleScore_Reward(GoogleMobileAds.Api.Reward reward)
    {
        High_Score_Count *= 3;

          if (High_Score_Count > High_Score_Index)
        {
            ObscuredPrefs.SetInt("High_Score_Index", High_Score_Count);
            //if(FireStoreManager.Instance != null) FireStoreManager.Instance.CreateWaterShooterScore();
            if(FireStoreManager.Instance != null) FireStoreManager.Instance.CreateCurrentGameScore();
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ReklamScript.BannerGoster();

    }
    public void OpenInstagram()
    {
        Application.OpenURL("https://www.instagram.com/gameofwinners_/");
    }

    public GameObject rateUsPanel;
    public GameObject starsParent;
    public void NeverButtoninRateUs()
    {
        PlayerPrefs.SetInt("NeverRateUs", 1);
    }

    int star = 0;
    public void StarButtons(int which)
    {
        for (int i = 0; i < 5; i++){starsParent.transform.GetChild(i).GetComponent<Image>().color = new Color32(75,75,75,255);}

        star = which;

        for(int i =0; i<=which; i++)
        {
            starsParent.transform.GetChild(i).GetComponent<Image>().color = Color.blue;
        }
    }

    public void OylaButton()
    {
        if (star > 2)
        {
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.MedusaGames.GameofWinners");
            PlayerPrefs.SetInt("NeverRateUs", 1);
            rateUsPanel.SetActive(false);
        }
        else
            rateUsPanel.SetActive(false);
    }
}
