﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Script : MonoBehaviour
{
    GameObject Player;

    public Vector3 Camera_Position;
    public Vector3 Shop_Camera_Position;

    [Range(0.05f, 2f)]
    public float SmoothFactor;

    Vector3 cameraInitialPosition;
    public float shakeMagnetude = 0.05f, shakeTime = 0.5f;

    public static Camera_Script instance;

    void Awake()
    {
        instance = this;
        Player = GameObject.Find("Main_Player");
    }
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        if (!Level_Manager_Script.instance.Is_Camera_Can_Move)
        {
            Vector3 new_Cam_Position = Player.transform.position + Camera_Position;

            transform.position = Vector3.Lerp(transform.position, new_Cam_Position, SmoothFactor);
        }
        else
        {
            Vector3 new_Cam_Position_Shop = Player.transform.position + Shop_Camera_Position;

            transform.position = Vector3.Lerp(transform.position, new_Cam_Position_Shop, SmoothFactor);
        }
    }
    public void ShakeIt()
    {
        cameraInitialPosition = transform.position;
        InvokeRepeating("StartCameraShaking", 0f, 0.005f);
    }
    public void ShakeIt_By_Time()
    {
        cameraInitialPosition = transform.position;
        InvokeRepeating("StartCameraShaking", 0f, 0.005f);
        Invoke("StopCameraShaking", shakeTime);
    }

    public void StartCameraShaking()
    {
        float cameraShakingOffsetX = Random.value * shakeMagnetude * 2 - shakeMagnetude;
        float cameraShakingOffsetY = Random.value * shakeMagnetude * 2 - shakeMagnetude;
        Vector3 cameraIntermadiatePosition = transform.position;
        cameraIntermadiatePosition.x += cameraShakingOffsetX;
        cameraIntermadiatePosition.y += cameraShakingOffsetY;
        transform.position = cameraIntermadiatePosition;
    }

    public void StopCameraShaking()
    {
        CancelInvoke("StartCameraShaking");
    }
}
