﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cost_Manager_Script : MonoBehaviour
{
    public static Cost_Manager_Script instance;
    public GameObject[] CharacterList;
    public GameObject[] CityList;
    public int CharacterIndex;
    private int City_Index;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        CharacterIndex = PlayerPrefs.GetInt("CharacterSelected");
        City_Index = PlayerPrefs.GetInt("CitySelected");

        foreach (GameObject characters in CharacterList)
        {
            characters.SetActive(false);
        }

        if (CharacterList[CharacterIndex])
        {
            CharacterList[CharacterIndex].SetActive(true);
        }

        foreach (GameObject city in CityList)
        {
            city.SetActive(false);
        }

        if (CityList[City_Index])
        {
            CityList[City_Index].SetActive(true);
        }

        //Check_Character_Index();
    }
    public void ToogleLeft_Character()
    {
        CharacterList[CharacterIndex].SetActive(false);
        CharacterIndex--;
        if (CharacterIndex < 0)
            CharacterIndex = CharacterList.Length - 1;
        CharacterList[CharacterIndex].SetActive(true);
        //Check_Character_Index();
    }

    public void ToogleRight_Character()
    {
        CharacterList[CharacterIndex].SetActive(false);
        CharacterIndex++;
        if (CharacterIndex == CharacterList.Length)
            CharacterIndex = 0;
        CharacterList[CharacterIndex].SetActive(true);
        //Check_Character_Index();
    }
    public void ToogleLeft_City()
    {
        CityList[City_Index].SetActive(false);
        City_Index--;
        if (City_Index < 0)
            City_Index = CityList.Length - 1;
        CityList[City_Index].SetActive(true);

    }

    public void ToogleRight_City()
    {
        CityList[City_Index].SetActive(false);
        City_Index++;
        if (City_Index == CityList.Length)
            City_Index = 0;
        CityList[City_Index].SetActive(true);

    }
    /*public void CharacterSelection_One()
    {
        CharacterList[CharacterIndex].SetActive(false);

        CharacterIndex = 0;
        if (CharacterIndex == 0)
        {
            CharacterList[CharacterIndex].SetActive(true);
        }
    }
    public void CharacterSelection_Two()
    {
        CharacterList[CharacterIndex].SetActive(false);

        CharacterIndex = 1;
        if (CharacterIndex == 1)
        {
            CharacterList[CharacterIndex].SetActive(true);
        }
    }
    public void CharacterSelection_Three()
    {
        CharacterList[CharacterIndex].SetActive(false);

        CharacterIndex = 2;
        if (CharacterIndex == 2)
        {
            CharacterList[CharacterIndex].SetActive(true);
        }
    }
    public void City_Select_One()
    {
        CityList[City_Index].SetActive(false);

        City_Index = 0;
        if (City_Index == 0)
        {
            CityList[City_Index].SetActive(true);
        }
    }
    public void City_Select_Two()
    {
        CityList[City_Index].SetActive(false);

        City_Index = 1;
        if (City_Index == 1)
        {
            CityList[City_Index].SetActive(true);
        }
    }
    public void City_Select_Three()
    {
        CityList[City_Index].SetActive(false);

        City_Index = 2;
        if (City_Index == 2)
        {
            CityList[City_Index].SetActive(true);
        }
    }*/
    /*public void Check_Character_Index()
    {
        if (CharacterIndex == 0)
        {
            Level_Manager_Script.instance.Watch_Adds_ForCharacters_Button.SetActive(false);
            Level_Manager_Script.instance.Play_Buttton.SetActive(true);
        }
        else
        {
            Level_Manager_Script.instance.Watch_Adds_ForCharacters_Button.SetActive(true);
            Level_Manager_Script.instance.Play_Buttton.SetActive(false);
        }
    }*/
    public void Select_Object()
    {
        PlayerPrefs.SetInt("CitySelected", City_Index);
        PlayerPrefs.SetInt("CharacterSelected", CharacterIndex);
        Level_Manager_Script.instance.Is_Click_Shop_Button = false;
        //Level_Manager_Script.instance.Play_Panel.SetActive(true);
        Level_Manager_Script.instance.Shop_Panel.SetActive(false);
    }
    public void Watch_Adds_Rewarded_Video()
    {
        //ReklamScript.RewardedReklamGoster(Watch_Adds_For_Characters);
    }
    public void Watch_Adds_For_Characters()//  public void Watch_Adds_For_Characters(GoogleMobileAds.Api.Reward reward)
    {
        //watch_adds

        PlayerPrefs.SetInt("CharacterSelected", CharacterIndex);
        Level_Manager_Script.instance.Watch_Adds_ForCharacters_Button.SetActive(false);
        Level_Manager_Script.instance.Play_Buttton.SetActive(true);
    }
}
