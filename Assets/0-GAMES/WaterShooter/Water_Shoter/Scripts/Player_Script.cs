﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Player_Script : MonoBehaviour
{
    public static Player_Script instance;
    Animator anim;
    Rigidbody rb;
    GameObject Enemy;
    GameObject Continue_Panels;
    GameObject Extra_Life_Buttons;

    public GameObject Water_Baloons;
    public GameObject Water_Shooter;

    public bool Is_in_Wall;
    public bool Is_in_Boss;

    public int Extra_Life = 0;

    public float Jump_force;
    public float Run_Speed = 100f;
    public float laneDistance = 2.2f;//The distance between tow lanes

    public Text Need_Water_Text;

    private Vector3 First_Pos;

    private bool Is_Rolling;

    private bool Is_Swipe_Left;
    private bool Is_Swipe_Right;
    private bool Is_Swipe_Up;
    private bool Is_Swipe_Down;
    private bool Is_Player_Wants_Continue;
    //private bool Is_Top_Touch;

    private int Level_Index;
    private int desiredLane = 1;//0:left, 1:middle, 2:right
    private int Speed_Countrer = 1;
    

    private float First_Transform_Y;
    private float First_Collieder_float;

    public List<Tween> jumps = new List<Tween>();

    [SerializeField]
    Ease Jump_Style = Ease.OutCubic;

    [SerializeField]
    Ease Fall_Style = Ease.InQuad;


    float Timer = 0.25f;

    void Awake()
    {
        instance = this;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }
    void Start()
    {
        First_Collieder_float = GetComponent<CapsuleCollider>().height;
        First_Transform_Y = transform.position.y;
        InvokeRepeating("Run", 0, 3);

        videoAmount = 1;

        Need_Water_Text = GameObject.FindGameObjectWithTag("Need_Water_Text").GetComponent<Text>();

        Continue_Panels = Level_Manager_Script.instance.Continue_Panel;
        Extra_Life_Buttons = Level_Manager_Script.instance.Extra_Life_Button;

        First_Pos = transform.position; 
    }


    void FixedUpdate()
    {

        if (Level_Manager_Script.instance.Is_Start == true && Level_Manager_Script.instance.Is_Click_Shop_Button == false)
        {
            transform.Translate(0, 0, Run_Speed * Time.fixedDeltaTime);
        }
    }
    void Update()
    {


        //Gather the inputs on which lane we should be
        if (Level_Manager_Script.instance.Is_Start == true)
        {
            if (Swipe_Manager_Script.swipeRight)
            {
                desiredLane++;
                if (desiredLane == 3)
                    desiredLane = 2;
                Is_Swipe_Right = true;
                if (Is_Swipe_Down == true && Is_Swipe_Up == true && Is_Swipe_Left == true && Is_Swipe_Right == true)
                {
                    Level_Manager_Script.instance.Is_Passed_Tutorial = true;
                    Level_Manager_Script.instance.Tutorial_Image.SetActive(false);
                    PlayerPrefs.SetInt("Tutorial_Index", 1);
                }
            }
            if (Swipe_Manager_Script.swipeLeft)
            {
                desiredLane--;
                if (desiredLane == -1)
                    desiredLane = 0;
                Is_Swipe_Left = true;
                if (Is_Swipe_Down == true && Is_Swipe_Up == true && Is_Swipe_Left == true && Is_Swipe_Right == true)
                {
                    Level_Manager_Script.instance.Is_Passed_Tutorial = true;
                    Level_Manager_Script.instance.Tutorial_Image.SetActive(false);
                    PlayerPrefs.SetInt("Tutorial_Index", 1);
                }
            }
            if (Swipe_Manager_Script.swipeUp)
            {
                Jump();
                Is_Swipe_Up = true;
                if (Is_Swipe_Down == true && Is_Swipe_Up == true && Is_Swipe_Left == true && Is_Swipe_Right == true)
                {
                    Level_Manager_Script.instance.Is_Passed_Tutorial = true;
                    Level_Manager_Script.instance.Tutorial_Image.SetActive(false);
                    PlayerPrefs.SetInt("Tutorial_Index", 1);
                }
            }
            if (Swipe_Manager_Script.swipeDown)
            {
                Down();
                if (jumps.Count == 1)
                {
                    jumps[0].Kill();
                    jumps.RemoveAt(0);
                }
                else
                {
                    for (int i = jumps.Count - 1; i > 0; i--)
                    {
                        jumps[i].Pause();
                        jumps[i].Kill();
                        jumps.RemoveAt(i);
                    }
                }
                Is_Swipe_Down = true;
                Is_Rolling = true;
                if (Is_Swipe_Down == true && Is_Swipe_Up == true && Is_Swipe_Left == true && Is_Swipe_Right == true)
                {
                    Level_Manager_Script.instance.Is_Passed_Tutorial = true;
                    Level_Manager_Script.instance.Tutorial_Image.SetActive(false);
                    PlayerPrefs.SetInt("Tutorial_Index", 1);
                }
            }
        }
       

        //Calculate where we should be in the future
        Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;
        if (desiredLane == 0)
        {
            targetPosition += Vector3.left * laneDistance;
        }

        else if (desiredLane == 2)
        {
            targetPosition += Vector3.right * laneDistance;
        }
        transform.position = Vector3.Lerp(transform.position, targetPosition, 7 * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Walls")
        {
            if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                if(Level_Manager_Script.instance.Water_Baloons_Int < Level_Manager_Script.instance.Needs_Water_Baloon_Count)
                {
                    Time.timeScale = 0f;
                    Level_Manager_Script.instance.Continue_Panel.SetActive(true);
                }
                else if(Level_Manager_Script.instance.Water_Baloons_Int >= Level_Manager_Script.instance.Needs_Water_Baloon_Count)
                {
                    GameObject newObject = Instantiate(Enemy_Script.instance.Breaking_Object, collision.gameObject.transform.position, Enemy_Script.instance.Breaking_Object.transform.rotation);
                    Destroy(newObject, 1f);
                    Destroy(collision.gameObject);
                
                    Stop_Shoot();
                }
            }
            else
            {
                Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Empty");
                collision.gameObject.AddComponent<Rigidbody>();
                collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-90000, 90000), Random.Range(30000, 70000), 250000) * Time.deltaTime);
                Destroy(collision.gameObject, 5f);
                int randomint = Random.Range(0, 10);
                if (randomint > 0 && randomint < 5)
                {
                    Level_Manager_Script.instance.High_Score_Count += 5;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "5X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                if (randomint > 5 && randomint < 10)
                {
                    Level_Manager_Script.instance.High_Score_Count += 10;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "10X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                Level_Manager_Script.instance.Vibrations();
            }
            if (collision.gameObject.tag == "Ground")
            {
                if (Level_Manager_Script.instance.Is_Start == true && Level_Manager_Script.instance.Is_Click_Shop_Button == false)
                {
                        Turn_Collieder_Normal();
                }
                else
                {
                    transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.GetComponent<Animator>().Play("Dance");
                }
            }

        }
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Cars")
        {
            Level_Manager_Script.instance.Vibrations();

            Player_Takes_Extra_Life();

            /*if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                if (Extra_Life <= 0)
                {
                    Time.timeScale = 0f;
                    Level_Manager_Script.instance.Continue_Panel.SetActive(true);
                }
                else
                {
                    Level_Manager_Script.instance.Less_Extra_Life();

                    foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
                    {
                        Destroy(enemy.gameObject);
                    }
                    //collision.gameObject.AddComponent<Rigidbody>();
                    //collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-90000, 90000), Random.Range(30000, 70000), 450000) * Time.deltaTime);
                }

            }
            else
            {
                Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Empty");
                collision.gameObject.AddComponent<Rigidbody>();
                collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-90000, 90000), Random.Range(30000, 70000), 250000) * Time.deltaTime);
                Destroy(collision.gameObject, 5f);
                int randomint = Random.Range(0, 10);
                if (randomint > 0 && randomint < 5)
                {
                    Level_Manager_Script.instance.High_Score_Count += 5;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "5X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                if (randomint > 5 && randomint < 10)
                {
                    Level_Manager_Script.instance.High_Score_Count += 10;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "10X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                Level_Manager_Script.instance.Vibrations();
            }*/
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        
        if (collision.gameObject.tag == "Ground")
        {
            if (Level_Manager_Script.instance.Is_Start == true && Level_Manager_Script.instance.Is_Click_Shop_Button == false)
            {
                Turn_Collieder_Normal();  
            }
            else
            {
                transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.GetComponent<Animator>().Play("Dance");
            }
        }
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Cars")
        {
            Player_Takes_Extra_Life();
        }
        }
    private void OnCollisionExit(Collision collision)
    {
        transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.GetComponent<Animator>().Play("Jump");

        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Cars")
        {
            Player_Takes_Extra_Life();
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {

        /*if (other.gameObject.tag == "Non_Jumper_First" || other.gameObject.tag == "Non_Jumper_Second")
        {
            Is_Rolling = true;
            //factor = (float)Cubic.easeInOut(Time.time - timeStamp, 0, 1, duraion);
        }*/

        if (other.gameObject.tag == "Jump")
        {
            Level_Manager_Script.instance.Start_Power_Up_Jumper();
           foreach(GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
           {
               Destroy(power_ups);
           }
        }
        if (other.gameObject.tag == "Magnet")
        {
            Level_Manager_Script.instance.Start_Power_Up_Magnet();
            foreach(GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
        if (other.gameObject.tag == "Slower")
        {
            Level_Manager_Script.instance.Start_Power_Up_Slower();
            foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
        if (other.gameObject.tag == "Water_Baloon")
        {
            Level_Manager_Script.instance.Water_Baloons_Int++;
            Level_Manager_Script.instance.Water_Baloon_Count_Text.GetComponent<Text>().text = Level_Manager_Script.instance.Water_Baloons_Int.ToString("");
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag == "Extra_Life_Add")
        {
            if (Level_Manager_Script.instance.Is_Passed_Tutorial == true)
            {
                //Level_Manager_Script.instance.Vibrations();
                Extra_Life_Panels();
                Time.timeScale = 0f;
                Destroy(other.gameObject);
            }
            else
            {
                Destroy(other.gameObject);
            }
         
        }
        if (other.gameObject.name == "Road_First")
        {
            GameObject Road_Second = GameObject.Find("Road_Second");
            GameObject Road_First = GameObject.Find("Road_First");
            Road_Second.gameObject.transform.position = new Vector3(Road_First.transform.position.x,
           Road_First.transform.position.y, Road_First.transform.position.z + 591.1f);
        }
        if (other.gameObject.name == "Road_Second")
        {
            GameObject Road_First = GameObject.Find("Road_First");
            GameObject Road_Second = GameObject.Find("Road_Second");

            Road_First.gameObject.transform.position = new Vector3(Road_Second.transform.position.x,
           Road_Second.transform.position.y, Road_Second.transform.position.z + 591.1f);
        }
        if (other.gameObject.tag == "Enemy_Wall")
        {
            Is_in_Wall = true;
            if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                Level_Index++;
                if (Level_Index >= 0 && Level_Index < 3)
                {
                    Level_Manager_Script.instance.Needs_Water_Baloon_Count = Random.Range(5, 8);
                    Need_Water_Text.text = Level_Manager_Script.instance.Needs_Water_Baloon_Count.ToString("");
                }

                if (Level_Index >= 3 && Level_Index <= 5)
                {
                    Level_Manager_Script.instance.Needs_Water_Baloon_Count = Random.Range(8, 12);
                    Need_Water_Text.text = Level_Manager_Script.instance.Needs_Water_Baloon_Count.ToString("");
                }
                if (Level_Index > 5)
                {
                    Level_Manager_Script.instance.Needs_Water_Baloon_Count = Random.Range(12, 16);
                    Need_Water_Text.text = Level_Manager_Script.instance.Needs_Water_Baloon_Count.ToString("");
                }

                Enemy = other.gameObject;

                InvokeRepeating("Shoot_Baloons", 0, 0.06f);
            }
           
        }
        if (other.gameObject.tag == "Enemy_Boss")
        {
            Is_in_Boss = true;
            if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                Enemy_Script.instance.Start_Enemy_Shoot();
                Level_Index++;
                if (Level_Index >= 0 && Level_Index < 3)
                {
                    Level_Manager_Script.instance.Needs_Water_Baloon_Count = Random.Range(1, 2);
                    Need_Water_Text.text = Level_Manager_Script.instance.Needs_Water_Baloon_Count.ToString("");
                }

                if (Level_Index >= 3 && Level_Index <= 5)
                {
                    Level_Manager_Script.instance.Needs_Water_Baloon_Count = Random.Range(2, 4);
                    Need_Water_Text.text = Level_Manager_Script.instance.Needs_Water_Baloon_Count.ToString("");
                }
                if (Level_Index > 5)
                {
                    Level_Manager_Script.instance.Needs_Water_Baloon_Count = Random.Range(4, 7);
                    Need_Water_Text.text = Level_Manager_Script.instance.Needs_Water_Baloon_Count.ToString("");
                }

                Enemy = other.gameObject;

                InvokeRepeating("Shoot_Baloons", 0, 0.06f);
            }
        }
        if (other.gameObject.tag == "Gift")
        {
            Level_Manager_Script.instance.Gift_Text.SetActive(false);
            Level_Manager_Script.instance.Gift_Button.SetActive(true);
            Time.timeScale = 0;
            Destroy(other.gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        /*if (other.gameObject.tag == "Non_Jumper_First" || other.gameObject.tag == "Non_Jumper_Second")
        {
            Is_Rolling = true;
            //factor = (float)Cubic.easeInOut(Time.time - timeStamp, 0, 1, duraion);
        }*/


        if (other.gameObject.tag == "Jump")
        {
            Level_Manager_Script.instance.Start_Power_Up_Jumper();
            foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
        if (other.gameObject.tag == "Magnet")
        {
            Level_Manager_Script.instance.Start_Power_Up_Magnet();
            foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
        if (other.gameObject.tag == "Slower")
        {
            Level_Manager_Script.instance.Start_Power_Up_Slower();
            foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
        if (other.gameObject.tag == "Gift")
        {
            Level_Manager_Script.instance.Gift_Button.SetActive(true);
            Time.timeScale = 0;
            Destroy(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        /*if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Cars")
        {
            if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                if (Extra_Life <= 0)
                {
                    Time.timeScale = 0f;
                    Level_Manager_Script.instance.Continue_Panel.SetActive(true);
                }
                else
                {
                    other.gameObject.AddComponent<Rigidbody>();
                    other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-90000, 90000), Random.Range(30000, 70000), 450000) * Time.deltaTime);
                }

            }
            else
            {
                Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Empty");
                other.gameObject.AddComponent<Rigidbody>();
                other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-90000, 90000), Random.Range(30000, 70000), 250000) * Time.deltaTime);
                Destroy(other.gameObject, 5f);
                int randomint = Random.Range(0, 10);
                if (randomint > 0 && randomint < 5)
                {
                    Level_Manager_Script.instance.High_Score_Count += 5;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "5X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                if (randomint > 5 && randomint < 10)
                {
                    Level_Manager_Script.instance.High_Score_Count += 10;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "10X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                Level_Manager_Script.instance.Vibrations();
            }
        }*/
        if (other.gameObject.tag == "Jump")
        {
            Level_Manager_Script.instance.Start_Power_Up_Jumper();
            foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
        if (other.gameObject.tag == "Magnet")
        {
            Level_Manager_Script.instance.Start_Power_Up_Magnet();
            foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
        if (other.gameObject.tag == "Slower")
        {
            Level_Manager_Script.instance.Start_Power_Up_Slower();
            foreach (GameObject power_ups in GameObject.FindGameObjectsWithTag("Power_Ups"))
            {
                Destroy(power_ups);
            }
        }
    }
    /*private void OnTriggerExit(Collider other)
    {
        Stop_Shoot();
    }*/
    private void Jump_Dotween()
    {
        //transform.DOMove(new Vector3(transform.position.x,2f,transform.position.z), 2f).OnComplete(() => Jump_Dotween(First_Pos));
        //transform.DOMoveY(5f, 1).SetEase(Ease.OutCubic).OnComplete(() => transform.DOMoveY(First_Pos.y, 1).SetEase(Ease.OutCubic));
        jumps.Add(transform.DOMoveY(Jump_force, 0.5f).SetEase(Jump_Style).SetRelative().OnComplete(() => jumps.Add(transform.DOMoveY(First_Pos.y, 0.5f).SetEase(Fall_Style))));
    }
    public void Stop_Shoot()
    {
        Is_in_Boss = false;
        Is_in_Wall = false;
        Level_Manager_Script.instance.Is_Wall_Can_Spawn = true;
        Level_Manager_Script.instance.Baloon_Error.SetActive(false);
        CancelInvoke("Shoot_Baloons");
    }
    public void Shoot_Baloons()
    {
        if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
        {
            if (Level_Manager_Script.instance.Water_Baloons_Int > 0)
            {
                Invoke("Close", 3f);

                Level_Manager_Script.instance.Baloon_Error.SetActive(false);
                if (Level_Manager_Script.instance.Needs_Water_Baloon_Count > 0)
                {
                    Level_Manager_Script.instance.Needs_Water_Baloon_Count--;
                    Need_Water_Text.text = Level_Manager_Script.instance.Needs_Water_Baloon_Count.ToString("");
                }
                else if(Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 0)
                {
                    Need_Water_Text.text = "";
                }
                Level_Manager_Script.instance.Water_Baloons_Int--;
                Level_Manager_Script.instance.Water_Baloon_Count_Text.GetComponent<Text>().text = Level_Manager_Script.instance.Water_Baloons_Int.ToString("");

                GameObject new_Baloons = Instantiate(Water_Baloons, Water_Shooter.transform.position, Water_Baloons.transform.rotation);
                Vector3 direction = Enemy.transform.position - Water_Shooter.gameObject.transform.position;
                new_Baloons.GetComponent<Rigidbody>().AddForce(new Vector3(direction.x, Water_Shooter.transform.position.y, Water_Shooter.transform.position.z) * 300 * Time.deltaTime);
                //new_Baloons.GetComponent<Rigidbody>().velocity = new Vector3(direction.x, direction.y, Water_Shooter.transform.position.z) * 20 * Time.deltaTime;
                //new_Baloons.transform.position = Vector3.Slerp(new_Baloons.transform.position, GameObject.FindGameObjectWithTag("Walls").transform.position, 20 * Time.deltaTime);
                //new_Baloons.AddComponent<SphereCollider>();
                Destroy(new_Baloons, 5f);
            }
            else if (Level_Manager_Script.instance.Water_Baloons_Int <= 0)
            {
                Level_Manager_Script.instance.Baloon_Error.SetActive(true);
            }

        }
        if (this.transform.position.y > 20)
        {
            Stop_Shoot();
        }
    }
    public void Extra_Life_Panels()
    {
        Extra_Life_Buttons.SetActive(true);
    }
    public void Player_Takes_Extra_Life()
    {
        if (Extra_Life <= 0)
        {
            Time.timeScale = 0f;
            Continue_Panels.SetActive(true);
        }
        else
        {
            Level_Manager_Script.instance.Less_Extra_Life();

            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Destroy(enemy.gameObject);
            }
        }
    }
    public void Close()
    {
        Need_Water_Text.text = "";
        CancelInvoke("Close");
    }

    public void Jump()
    {
        if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
        {
            if (transform.position.y <= First_Transform_Y + 1f)
            {
                //rb.AddForce(Vector3.up * Jump_force * 10000f * Time.deltaTime);
                Jump_Dotween();
            }
        }
    }
    public void Down()
    {
        if (transform.position.y >= First_Transform_Y + 0.5f)
        {
            rb.AddForce(Vector3.up * -150000f * Time.deltaTime);
        }
        else
        {
            Is_Rolling = true;
            transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.GetComponent<Animator>().Play("Roll");
            GetComponent<CapsuleCollider>().height = 2f;
            GetComponent<CapsuleCollider>().center = new Vector3(0, 0.6f, 0); 
        }

    }
    public void Turn_Collieder_Normal()
    {
        if (Is_Rolling == false)
        {
            if(Is_in_Wall == false || Is_in_Boss == false)
            {
                transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.GetComponent<Animator>().Play("Run");
            }
            else if(Is_in_Wall == true || Is_in_Boss == true)
            {
                transform.GetChild(Cost_Manager_Script.instance.CharacterIndex).gameObject.GetComponent<Animator>().Play("Shoot");
            }
        }
        else
        {
            Down();
            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                GetComponent<CapsuleCollider>().height = First_Collieder_float;

                GetComponent<CapsuleCollider>().center = new Vector3(0, 3f, 0);

                Is_Rolling = false;
                Timer = 0.25f;
            }
            else if (Swipe_Manager_Script.swipeUp)
            {
                Jump();
                Is_Swipe_Up = true;
                if (Is_Swipe_Down == true && Is_Swipe_Up == true && Is_Swipe_Left == true && Is_Swipe_Right == true)
                {
                    Level_Manager_Script.instance.Is_Passed_Tutorial = true;
                    Level_Manager_Script.instance.Tutorial_Image.SetActive(false);
                    PlayerPrefs.SetInt("Tutorial_Index", 1);
                }
            }
        }
    }
    public void Run()
    {
        if (Level_Manager_Script.instance.Is_Start == true)
        {
            Speed_Countrer++;
            if (Run_Speed <= 140)
            {
                Run_Speed += 10f;
                Level_Manager_Script.instance.Current_Run_Speed = Run_Speed;
            }
            if (Speed_Countrer >= 20)
            {
                Run_Speed = 160;
            }
            /*if(Level_Manager_Script.instance.Spawn_Time >= 0.1f)
            {
                Level_Manager_Script.instance.Spawn_Time -= 0.1f;
            }*/
            if (Level_Manager_Script.instance.car_Speed <= 250)
            {
                Level_Manager_Script.instance.car_Speed += 10f;
            }
        }
 
    }

    public void Continue()
    {
        ReklamScript.RewardedReklamGoster(ContinueReward);
        //ContinueReward();//Bunu Ben Ekledim İkbal Deneme amacli silebilirsin
    }

    private int videoAmount = 1;
    public void ContinueReward(GoogleMobileAds.Api.Reward reward)
    {
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(enemy.gameObject);
        }
        Level_Manager_Script.instance.Continue_Panel.SetActive(false);
        Is_Player_Wants_Continue = false;
        Level_Manager_Script.instance.Is_Wall_Can_Spawn = true;

        Level_Manager_Script.instance.continueButton.SetActive(false);
        Level_Manager_Script.instance.multipleScoreButton.SetActive(true);
        /*if (videoAmount > 0)
        {
            videoAmount--;

    //        ReklamScript.BannerGizle();

            Time.timeScale = 1f;
            
            foreach (GameObject walls in GameObject.FindGameObjectsWithTag("Walls"))
            {
                Destroy(walls.gameObject);
            }
        /*foreach (GameObject cars in GameObject.FindGameObjectsWithTag("Cars"))
        {
            Destroy(cars.gameObject);
        }

        foreach (GameObject enemy_walls in GameObject.FindGameObjectsWithTag("Enemy_Wall"))
        {
            Destroy(enemy_walls.gameObject);
        }
        foreach (GameObject destroy_cars in GameObject.FindGameObjectsWithTag("Destroy_Car"))
        {
            Destroy(destroy_cars.gameObject);
        }
        foreach (GameObject water_balls in GameObject.FindGameObjectsWithTag("Water_Baloon"))
        {
            Destroy(water_balls.gameObject);
        }
        //Is_Player_Wants_Continue = true;
    
        }*/
    }

}
