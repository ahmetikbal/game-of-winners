﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource Background_Music;

public void Change_BGM(AudioClip music)
    {
        if (Background_Music.clip.name == music.name)
            return;

        Background_Music.Stop();
        Background_Music.clip = music;
        Background_Music.Play();
    }
}
