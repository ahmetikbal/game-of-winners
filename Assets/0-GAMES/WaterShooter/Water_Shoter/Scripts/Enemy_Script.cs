﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Enemy_Script : MonoBehaviour
{
    public static Enemy_Script instance;

    GameObject Player;

    public GameObject Enemy_Water_Baloons;
    public GameObject Breaking_Object;
    void Awake()
    {
        instance = this;
        Player = GameObject.Find("Main_Player");
    }
    void Start()
    {

    }

    void Update()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Walls")
        {
            Destroy(collision.gameObject);
        }
        /*if (collision.gameObject.name == "Main_Player")
        {
            if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Empty");
                this.gameObject.AddComponent<Rigidbody>();
                this.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-90000, 90000), Random.Range(30000, 70000), 150000) * Time.deltaTime);
                Destroy(this.gameObject, 5f);
                int randomint = Random.Range(0, 10);
                if (randomint > 0 && randomint < 5)
                {

                    Level_Manager_Script.instance.High_Score_Count += 5;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "5X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                if (randomint > 5 && randomint < 10)
                {
                    Level_Manager_Script.instance.High_Score_Count += 10;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "10X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                Level_Manager_Script.instance.Vibrations();
            }
        }*/
        if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
        {
            if (collision.gameObject.tag == "Player_Ball" || collision.gameObject.name == "Main_Player")
            {

                if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <=0)
                {
                    Destroy(this.gameObject);
                    GameObject newObject = Instantiate(Breaking_Object, this.gameObject.transform.position, Breaking_Object.transform.rotation);
                    Destroy(newObject, 1f);
                    Level_Manager_Script.instance.Is_Wall_Can_Spawn = true;
                    Player_Script.instance.Stop_Shoot();
                }

                if (Player_Script.instance.Is_in_Boss == true)
                {
                    Start_Enemy_Shoot();

                    if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 0)
                    {
                        CancelInvoke("Enemy_Shoot_Baloon");
                        Destroy(this.gameObject);
                        GetComponent<Animator>().Play("Dead");
                        Level_Manager_Script.instance.Is_Wall_Can_Spawn = true;
                        Player_Script.instance.Stop_Shoot();
                        this.gameObject.GetComponent<Collider>().enabled = false;
                    }
                }
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        /*if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
        {
            if (collision.gameObject.tag == "Player_Ball")
            {
                if (Player_Script.instance.Is_in_Wall == true)
                {
                    if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 0)
                    {
                        Destroy(this.gameObject);
                        GameObject newObject = Instantiate(Breaking_Object, this.gameObject.transform.position, Breaking_Object.transform.rotation);
                        Destroy(newObject, 1f);
                        Player_Script.instance.Stop_Shoot();
                    }
                }
                if (Player_Script.instance.Is_in_Boss == true)
                {
                    Start_Enemy_Shoot();

                    if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 0)
                    {
                        CancelInvoke("Enemy_Shoot_Baloon");
                        Destroy(this.gameObject);
                        GetComponent<Animator>().Play("Dead");
                        Player_Script.instance.Stop_Shoot();
                        this.gameObject.GetComponent<Collider>().enabled = false;
                    }
                }
                Destroy(collision.gameObject);
            }
        }*/
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Destroy_Car"/* || other.gameObject.tag == "Walls" || other.gameObject.tag == "Enemy_Wall"*/)
        {
            Destroy(this.gameObject);

         //   if(other.gameObject.tag == "Enemy_Wall") Destroy(other.gameObject);
        }
        

        /*if (other.gameObject.name == "Main_Player")
        {
            if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Empty");
                this.gameObject.AddComponent<Rigidbody>();
                this.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-90000, 90000), Random.Range(30000, 70000), 550000) * Time.deltaTime);
                Destroy(this.gameObject, 5f);
                int randomint = Random.Range(0, 10);
                if (randomint > 0 && randomint < 5)
                {

                    Level_Manager_Script.instance.High_Score_Count += 5;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "5X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                if (randomint > 5 && randomint < 10)
                {
                    Level_Manager_Script.instance.High_Score_Count += 10;
                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Text>().text = "10X";

                    Level_Manager_Script.instance.Bonus_HighScore_Texts.gameObject.GetComponent<Animator>().Play("Text_Anim");
                }
                Level_Manager_Script.instance.Vibrations();
            }
            if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
            {
                if (other.gameObject.tag == "Player_Ball")
                {
                    if (Player_Script.instance.Is_in_Wall == true)
                    {
                        if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 1)
                        {
                            Destroy(this.gameObject);
                            GameObject newObject = Instantiate(Breaking_Object, this.gameObject.transform.position, Breaking_Object.transform.rotation);
                            Destroy(newObject, 1f);
                            Player_Script.instance.Stop_Shoot();
                        }
                    }
                    if (Player_Script.instance.Is_in_Boss == true)
                    {
                        Start_Enemy_Shoot();

                        if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 0)
                        {
                            CancelInvoke("Enemy_Shoot_Baloon");
                            Destroy(this.gameObject);
                            GetComponent<Animator>().Play("Dead");
                            Player_Script.instance.Stop_Shoot();
                            this.gameObject.GetComponent<Collider>().enabled = false;
                        }
                    }
                    Destroy(other.gameObject);
                }
            }
        }*/
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.AddComponent<Rigidbody>();
            other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-140000, 140000), Random.Range(40000, 70000), 0) * Time.deltaTime);
        }

        if (Level_Manager_Script.instance.Is_Take_Bigger_Bonus == false)
        {
            if (other.gameObject.tag == "Player_Ball" || other.gameObject.name == "Main_Player")
            {

                if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 0)
                {
                    Destroy(this.gameObject);
                    GameObject newObject = Instantiate(Breaking_Object, this.gameObject.transform.position, Breaking_Object.transform.rotation);
                    Destroy(newObject, 1f);
                    Player_Script.instance.Stop_Shoot();
                }

                if (Player_Script.instance.Is_in_Boss == true)
                {
                    Start_Enemy_Shoot();

                    if (Level_Manager_Script.instance.Needs_Water_Baloon_Count <= 0)
                    {
                        CancelInvoke("Enemy_Shoot_Baloon");
                        Destroy(this.gameObject);
                        GetComponent<Animator>().Play("Dead");
                        Player_Script.instance.Stop_Shoot();
                        this.gameObject.GetComponent<Collider>().enabled = false;
                    }
                }
            }
        }

    }
        private void OnTriggerStay(Collider other)
    {
        /*if (other.gameObject.tag == "Enemy_Wall")
        {
            Destroy(other.gameObject);
        }*/
        
    }
    public void Start_Enemy_Shoot()
    {
        InvokeRepeating("Enemy_Shoot_Baloon", 0, 0.5f);
    }
    public void Enemy_Shoot_Baloon()
    {
        GameObject new_Enemy_Bloon = Instantiate(Enemy_Water_Baloons, new Vector3(transform.position.x, 5f, transform.position.z)
            , Enemy_Water_Baloons.transform.rotation);
        Vector3 direction = this.gameObject.transform.position - Player.transform.position;

        new_Enemy_Bloon.GetComponent<Rigidbody>().AddForce(new Vector3(-direction.x, new_Enemy_Bloon .transform.position.y, -transform.position.z)
            * 200 * Time.deltaTime);
    }
}
