﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.UI;
using CodeStage.AntiCheat.Storage;
using CodeStage.AntiCheat.ObscuredTypes;

public class GameController_BlockMerger : MonoBehaviour {
    public static GameController_BlockMerger instance;

    public static Action<string> slide;

    [SerializeField] GameObject fillPrefab;
    [SerializeField] List<Cell> allCells;

    public Color[] fillColors;

    [Header("Unity Connections")]
    [SerializeField] GameObject gameOverPanel;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI highScoreText;
    [SerializeField] TextMeshProUGUI timerText;

    [Header("Deck Hand")]
    [SerializeField] Cell[] bottomCells;
    [SerializeField] Cell handCell;

    [SerializeField] List<int> baseLevel = new List<int> { 2, 2, 2, 2, 2, 4, 4, 4, 4, 8, 8, 16 };

    [SerializeField] Fill lastCell;

    [Header("Game Over Panel")]
    [SerializeField] TextMeshProUGUI gameOverScoreText;
    [SerializeField] TextMeshProUGUI gameOverHighScoreText;

    [Header("Pause Panel")]
    [SerializeField] GameObject pausePanel;

    [Header("Block Level Panel")]
    [SerializeField] GameObject blockLevelPanel;
    [SerializeField] Cell blockLevelHighest;
    [SerializeField] private ObscuredInt nextHighestBlock = 128;
    [SerializeField] private ObscuredInt addBaseLevel = 32;

    [Header("Powers")]
    [SerializeField] GameObject throwButtonsGrid;
    [SerializeField] GameObject blockDestroyButtonsGrid;
    [SerializeField] GameObject blockSwitchButtonsGrid;
    [SerializeField] GameObject blockDestroyButton;
    [SerializeField] GameObject blockSwitchButton;
    [SerializeField] GameObject extraSecondsButton;
    public Cell firstCell = null, secondCell = null;
    ObscuredInt firstCellValue, secondCellValue;
    Image firstImage, secondImage;
    private ObscuredFloat powersCooldown = 60, fillCooldown = 0f;
    ObscuredBool IsExtraSecondsUsed = false;


    [Header("Main")]
    public ObscuredInt score;

    public ObscuredFloat stopWatch = 180;

    private void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ReklamScript.BannerGoster();

        highScoreText.text = ObscuredPrefs.GetInt("BlockMerger_Highscore", 0).ToString("n0");

        StartSpawnHandFill();
    }

    // Update is called once per frame
    void Update()
    {
        if (stopWatch <= 1) {
            GameOver();
        } else {
            stopWatch -= Time.deltaTime;

            int minutes = Mathf.FloorToInt(stopWatch / 60F);
            int seconds = Mathf.FloorToInt(stopWatch - minutes * 60);
            string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);
            timerText.text = niceTime;
        }

        if (powersCooldown >= 60) {
            blockDestroyButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
            blockDestroyButton.GetComponent<Button>().interactable = true;

            blockSwitchButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
            blockSwitchButton.GetComponent<Button>().interactable = true;

            if (IsExtraSecondsUsed) {
                extraSecondsButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
                extraSecondsButton.GetComponent<Button>().interactable = false;
            } else {
                extraSecondsButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
                extraSecondsButton.GetComponent<Button>().interactable = true;
            }
                

            fillCooldown = 0;
        } else {
            blockSwitchButton.GetComponent<Button>().interactable = false;
            blockDestroyButton.GetComponent<Button>().interactable = false;
            extraSecondsButton.GetComponent<Button>().interactable = false;

            fillCooldown -= Time.deltaTime * (float)0.016;
            blockDestroyButton.transform.GetChild(1).GetComponent<Image>().fillAmount = fillCooldown;
            blockSwitchButton.transform.GetChild(1).GetComponent<Image>().fillAmount = fillCooldown;
            extraSecondsButton.transform.GetChild(1).GetComponent<Image>().fillAmount = fillCooldown;

            powersCooldown += Time.deltaTime;
        }

        /* Debug
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            GameObject tempFill = Instantiate(fillPrefab, handCell.transform);
            Fill tempFillComp = tempFill.GetComponent<Fill>();

            handCell.GetComponent<Cell>().fill = tempFillComp;

            tempFillComp.fillValueUpdate(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            GameObject tempFill = Instantiate(fillPrefab, handCell.transform);
            Fill tempFillComp = tempFill.GetComponent<Fill>();

            handCell.GetComponent<Cell>().fill = tempFillComp;

            tempFillComp.fillValueUpdate(4);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            GameObject tempFill = Instantiate(fillPrefab, handCell.transform);
            Fill tempFillComp = tempFill.GetComponent<Fill>();

            handCell.GetComponent<Cell>().fill = tempFillComp;

            tempFillComp.fillValueUpdate(8);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            SlideUp();
        }
        */
    }

    public void SlideUp() {
        slide("w");

        StartCoroutine(CheckCombinationsDelay());
    }

    IEnumerator CheckCombinationsDelay() {
        yield return new WaitForSeconds(.3f);
        CheckCombinations();
    }

    public void CheckCombinations() {
        print(Cell.isMerged);
        Cell.isMerged = false;

        slide("CheckTriple");
        slide("CheckDouble");

        GameOverCheck();
    }

    public void TripleCombo() {
        if (lastCell == null)
            return;

        Cell currentCell = lastCell.transform.parent.GetComponent<Cell>();

        if (currentCell == null)
            return;

        if (currentCell.fill == null)
            return;

        if (currentCell.up != null && currentCell.left != null && currentCell.right != null) {
            if (currentCell.up.fill != null && currentCell.right.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.left.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    Debug.Log("QUADRA BABY!!!! - GameController");
                    currentCell.fill.Quadruple();

                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.left.fill = null;

                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.right.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    return;
                }
            }
            /* Triple variation 2
             * x  y
             * () x
             * 
             * */
            if (currentCell.up.fill != null && currentCell.right.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    Debug.Log("TRIPLE BABY! - GameController");
                    currentCell.fill.Triple();

                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.right.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    print("Triple and Quadra Situation - Done - GameController");
                    return;
                }
            }
            /* Triple variation 3
             * y  x
             * x  ()
             * 
             * */
            if (currentCell.up.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.left.fill.value) {
                    Debug.Log("TRIPLE BABY! - GameController");
                    currentCell.fill.Triple();

                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.left.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    print("Triple and Quadra Situation - Done - GameController");
                    return;
                }
            }
            /* Triple variation 4
             * x  ()  x
             * */
            if (currentCell.left != null && currentCell.right != null) {
                if (currentCell.right.fill != null && currentCell.left.fill != null) {
                    if (currentCell.fill.value == currentCell.left.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                        currentCell.left.fill.transform.SetParent(currentCell.transform);
                        currentCell.fill = currentCell.left.fill;
                        currentCell.left.fill = null;


                        currentCell.right.fill.transform.SetParent(currentCell.transform);
                        currentCell.fill = currentCell.right.fill;
                        currentCell.right.fill = null;

                        Debug.Log("TRIPLE BABY! - GameController");
                        currentCell.fill.Triple();
                        return;
                    }
                }

                print("Triple variation 4 - GameController");
            }

            print("Triple and Quadra Situation - GameController");

        }
        /* Triple variation 2
         * x  y
         * () x
         * 
         * */
        else if (currentCell.up != null && currentCell.right != null) {
            if (currentCell.up.fill != null && currentCell.right.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    Debug.Log("TRIPLE BABY! - GameController");
                    currentCell.fill.Triple();

                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.right.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    print("Triple and Quadra Situation - Done - GameController");
                    return;
                }
            }

            print("Triple variation 2 - GameController");
        }
        /* Triple variation 3
         * y  x
         * x  ()
         * 
         * */
        else if (currentCell.up != null && currentCell.left != null) {
            if (currentCell.up.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.left.fill.value) {
                    Debug.Log("TRIPLE BABY! - GameController");
                    currentCell.fill.Triple();

                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.left.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    return;
                }
            }

            print("Triple variation 3 - GameController");
        }
        /* Triple variation 4
         * x  ()  x
         * */
        else if (currentCell.left != null && currentCell.right != null) {
            if (currentCell.right.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.left.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.left.fill;
                    currentCell.left.fill = null;


                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.right.fill;
                    currentCell.right.fill = null;

                    Debug.Log("TRIPLE BABY! - GameController");
                    currentCell.fill.Triple();
                    return;
                }
            }

            print("Triple variation 4 - GameController");
        }
    }

    public void DoubleCombo() {
        if (lastCell == null)
            return;

        Cell currentCell = lastCell.transform.parent.GetComponent<Cell>();

        if (currentCell == null)
            return;

        if (currentCell.fill == null)
            return;

        // Double Variation 1
        if (currentCell.up != null) {
            if (currentCell.up.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value) {
                    currentCell.fill.Double();
                    Debug.Log("Doubled up - GameController");
                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    return;
                }
                print("Double Variation 1 - GameController");
            }
        }
        // Double Variation 2
        if (currentCell.left != null) {
            if (currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.left.fill.value) {
                    currentCell.left.fill.Double();
                    Debug.Log("Doubled right - GameController");
                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.left.fill;
                    currentCell.left.fill = null;
                    return;
                }
                print("Double Variation 2 - GameController");
            }
        }
        // Double Variation 3
        if (currentCell.right != null) {
            if (currentCell.right.fill != null) {
                if (currentCell.fill.value == currentCell.right.fill.value) {
                    currentCell.right.fill.Double();
                    Debug.Log("Doubled left - GameController");
                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.right.fill;
                    currentCell.right.fill = null;
                    return;
                }
                print("Double Variation 3 - GameController");
            }
        }
    }

    public void CheckAndMerge() {
        if (lastCell == null)
            return;

        StartCoroutine(TripleMergeDelay());

        StartCoroutine(DoubleMergeDelay());
    }

    IEnumerator DoubleMergeDelay() {
        yield return new WaitForSeconds(.3f);
        DoubleCombo();
    }

    IEnumerator TripleMergeDelay() {
        yield return new WaitForSeconds(.2f);
        TripleCombo();
    }

    public void StartSpawnHandFill() {
        if (handCell.transform.childCount != 0) {
            Debug.Log("Hand cell is already filled.");
            return;
        }

        GameObject tempFill = Instantiate(fillPrefab, handCell.transform);
        Fill tempFillComp = tempFill.GetComponent<Fill>();

        handCell.GetComponent<Cell>().fill = tempFillComp;
        tempFillComp.fillValueUpdate(2);
    }

    public void ThrowHand(int a) {
        if (bottomCells[a].GetComponent<Cell>().fill != null)
            return;

        if (handCell.fill == null)
            return;

        GameObject tempFill = Instantiate(fillPrefab, bottomCells[a].transform);

        Fill tempFillComp = tempFill.GetComponent<Fill>();

        bottomCells[a].GetComponent<Cell>().fill = tempFillComp;
        tempFillComp.fillValueUpdate(handCell.fill.value);

        handCell.fill = null;

        lastCell = bottomCells[a].GetComponent<Cell>().fill;

        SlideUp();

        CheckAndMerge();
        StartCoroutine(ChangeHandDelay());
    }

    IEnumerator ChangeHandDelay() {
        yield return new WaitForSeconds(.5f);
        ChangeHandFill();
    }

    public void ChangeHandFill() {
        // Destroy(handCell.transform.GetChild(0).gameObject);

        GameObject tempFill = Instantiate(fillPrefab, handCell.transform);
        Fill tempFillComp = tempFill.GetComponent<Fill>();

        handCell.GetComponent<Cell>().fill = tempFillComp;

        tempFillComp.fillValueUpdate(baseLevel[UnityEngine.Random.Range(0, baseLevel.Count)]);
    }

    public void ScoreUpdate(int scoreIn)
    {
        score += scoreIn;
        scoreText.text = score.ToString("n0");
    }

    public void GameOverCheck()
    {
        // Check for bottom cells fill
        foreach (var cells in bottomCells) {
            if (cells.fill == null)
                return;
        }

        GameOver();
    }

    public void GameOver() {
        gameOverPanel.SetActive(true);

        gameOverScoreText.text = score.ToString("n0");

        if (ObscuredPrefs.GetInt("BlockMerger_Highscore", 0) < score) {
            ObscuredPrefs.SetInt("BlockMerger_Highscore", score);
            gameOverHighScoreText.text = score.ToString("n0");

            FireStoreManager.Instance.CreateCurrentGameScore(300.0f - stopWatch);
        }

    }

    public void Pause() {
        Time.timeScale = 0f;
    }

    public void Resume() {
        Time.timeScale = 1f;
    }

    public void Restart()
    {
        ReklamScript.InsterstitialGoster();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadScene(string scene) {
        ReklamScript.InsterstitialGoster();

        SceneManager.LoadScene(scene);
    }

    public void ReklamGoster(float chanceToWatch) {
        if (UnityEngine.Random.value > chanceToWatch) {
            ReklamScript.InsterstitialGoster();
        }
    }

    public void WinningCheck(int highestFill)
    {
        if (highestFill >= nextHighestBlock)
        {
            nextHighestBlock = highestFill * 2;
            baseLevel.Add(addBaseLevel);
            addBaseLevel *= 2;

            GameObject tempFill = Instantiate(fillPrefab, blockLevelHighest.transform);
            Fill tempFillComp = tempFill.GetComponent<Fill>();

            blockLevelHighest.fill = tempFillComp;
            tempFillComp.fillValueUpdate(highestFill);

            StartCoroutine(BlockLevelPanelDelay());
        }
    }

    IEnumerator BlockLevelPanelDelay() {
        yield return new WaitForSeconds(.4f);
        Pause();
        blockLevelPanel.SetActive(true);
    }

    public void BlockLevelUp() {
        score += 50;
        scoreText.text = score.ToString("n0");
    }

    public void PowerExtraSeconds() {
        ReklamScript.RewardedReklamGoster(PowerExtraSecondsActive);
    }

    public void PowerExtraSecondsActive(GoogleMobileAds.Api.Reward reward) {
        if (IsExtraSecondsUsed)
            return;

        powersCooldown = 0;
        fillCooldown = 1;

        stopWatch += 30;
        IsExtraSecondsUsed = true;
        extraSecondsButton.GetComponent<Button>().interactable = false;
    }

    public void PowerBlockDestroyOpen() {
        ReklamScript.RewardedReklamGoster(PowerBlockDestroyOpenActive);
    }

    public void PowerBlockDestroyOpenActive(GoogleMobileAds.Api.Reward reward) {
        blockDestroyButton.transform.GetChild(0).gameObject.SetActive(true);
        blockDestroyButton.GetComponent<Button>().interactable = false;
        blockSwitchButton.GetComponent<Button>().interactable = false;

        throwButtonsGrid.SetActive(false);
        blockDestroyButtonsGrid.SetActive(true);
    }

    public void PowerBlockDestroy () {
        var selectedCell = EventSystem.current.currentSelectedGameObject;

        Cell selectedCellReal = allCells.Where(obj => obj.name == selectedCell.name).SingleOrDefault();

        if (selectedCellReal.fill == null)
            return;

        throwButtonsGrid.SetActive(true);
        blockDestroyButtonsGrid.SetActive(false);

        blockDestroyButton.transform.GetChild(0).gameObject.SetActive(false);

        selectedCellReal.fill = null;

        powersCooldown = 0;
        fillCooldown = 1;
        blockSwitchButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 1;
        blockDestroyButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 1;
        blockDestroyButton.transform.GetChild(0).gameObject.SetActive(false);

        SlideUp();
    }

    public void PowerBlockSwitchOpen() {
        ReklamScript.RewardedReklamGoster(PowerBlockSwitchOpenActive);
    }

    public void PowerBlockSwitchOpenActive(GoogleMobileAds.Api.Reward reward) {
        blockSwitchButton.transform.GetChild(0).gameObject.SetActive(true);
        blockDestroyButton.GetComponent<Button>().interactable = false;
        blockSwitchButton.GetComponent<Button>().interactable = false;

        throwButtonsGrid.SetActive(false);
        blockSwitchButtonsGrid.SetActive(true);
    }

    public void PowerBlockSwitch() {
        GameObject selectedCell = EventSystem.current.currentSelectedGameObject;
        selectedCell.GetComponent<Image>().color = new Color(selectedCell.GetComponent<Image>().color.r, selectedCell.GetComponent<Image>().color.g, selectedCell.GetComponent<Image>().color.b, .6f);

        Cell selectedCellReal = allCells.Where(obj => obj.name == selectedCell.name).SingleOrDefault();

        if (selectedCellReal.fill == null || selectedCellReal.fill == firstCell)
            return;

        if (firstCell == null) {
            firstCell = selectedCellReal;
            firstCellValue = firstCell.fill.value;
            firstImage = selectedCell.GetComponent<Image>();
        } else {
            secondCell = selectedCellReal;
            secondCellValue = secondCell.fill.value;
            secondImage = selectedCell.GetComponent<Image>();
        }

        if (firstCell != null && secondCell != null) {
            throwButtonsGrid.SetActive(true);
            blockSwitchButtonsGrid.SetActive(false);

            blockSwitchButton.transform.GetChild(0).gameObject.SetActive(false);

            GameObject tempFill = Instantiate(fillPrefab, secondCell.transform);
            Fill tempFillComp = tempFill.GetComponent<Fill>();

            secondCell.fill = tempFillComp;
            tempFillComp.fillValueUpdate(firstCellValue);

            GameObject tempFill2 = Instantiate(fillPrefab, firstCell.transform);
            Fill tempFillComp2 = tempFill2.GetComponent<Fill>();

            firstCell.fill = tempFillComp2;
            tempFillComp2.fillValueUpdate(secondCellValue);

            firstCell = null;
            secondCell = null;

            firstImage.color = new Color(firstImage.color.r, firstImage.color.g, firstImage.color.b, 0f);
            secondImage.color = new Color(secondImage.color.r, secondImage.color.g, secondImage.color.b, 0f);

            powersCooldown = 0;
            fillCooldown = 1;
            blockSwitchButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 1;
            blockDestroyButton.transform.GetChild(1).GetComponent<Image>().fillAmount = 1;
            blockSwitchButton.transform.GetChild(0).gameObject.SetActive(false);

            StartCoroutine(CheckCombinationsDelay());
        }
    }
}
