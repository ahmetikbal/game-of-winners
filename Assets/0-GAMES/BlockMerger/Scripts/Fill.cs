﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fill : MonoBehaviour
{
    public int value;
    [SerializeField] Text valueDisplay;
    [SerializeField] float speed;

    bool hasCombined;

    Image cellImage;

    public void fillValueUpdate(int valueIn)
    {
        value = valueIn;
        valueDisplay.text = value.ToString();

        int colorIndex = GetColorIndex(value);
        cellImage = GetComponent<Image>();
        cellImage.color = GameController_BlockMerger.instance.fillColors[colorIndex];
    }

    int GetColorIndex(int valueIn)
    {
        int index = 0;
        while (valueIn != 1)
        {
            index += 1;
            valueIn /= 2;
        }

        index -= 1;
        return index;
    }

    private void Update()
    {
        if (transform.localPosition != Vector3.zero)
        {
            hasCombined = false;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero, speed * Time.deltaTime);
        }
        else if(hasCombined == false)
        {
            if (transform.parent.GetChild(0) != this.transform)
            {
                Destroy(transform.parent.GetChild(0).gameObject);
            }

            hasCombined = true;
        }
    }

    public void Double()
    {
        value = value * 2;
        GameController_BlockMerger.instance.ScoreUpdate(value);
        valueDisplay.text = value.ToString();

        int colorIndex = GetColorIndex(value);
        cellImage.color = GameController_BlockMerger.instance.fillColors[colorIndex];

        GameController_BlockMerger.instance.SlideUp();
        GameController_BlockMerger.instance.WinningCheck(value);    
    }

    public void Triple() {
        value = (value * 2) * 2;
        GameController_BlockMerger.instance.ScoreUpdate(value);
        valueDisplay.text = value.ToString();

        int colorIndex = GetColorIndex(value);
        cellImage.color = GameController_BlockMerger.instance.fillColors[colorIndex];

        GameController_BlockMerger.instance.SlideUp();
        GameController_BlockMerger.instance.WinningCheck(value);
    }

    public void Quadruple() {
        value = (value * 4) * 2;
        GameController_BlockMerger.instance.ScoreUpdate(value);
        valueDisplay.text = value.ToString();

        int colorIndex = GetColorIndex(value);
        cellImage.color = GameController_BlockMerger.instance.fillColors[colorIndex];

        GameController_BlockMerger.instance.SlideUp();
        GameController_BlockMerger.instance.WinningCheck(value);
    }
}
