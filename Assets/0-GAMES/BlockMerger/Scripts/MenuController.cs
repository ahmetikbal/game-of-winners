﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuController : MonoBehaviour
{
    [SerializeField] List<Button> rewardButtons;
    [SerializeField] List<GameObject> rewardImages;

    [SerializeField] List<Button> premiumRewardButtons;
    [SerializeField] List<GameObject> premiumRewardImages;

    public GameObject noPremiumPopup;

    public void StartTheGame() {
        SceneManager.LoadScene("Main_BlockMerger");
    }

    public void StartThePremiumGame() {
        if (SubscriptionSystem.Instance.subscriptionStatus != SubscriptionSystem.SubscriptionStatus.None) {
            SceneManager.LoadScene("Main_BlockMerger");
        }
        else {
            noPremiumPopup.SetActive(true);
        }
    }

    public void OpenRewardPopup() {
        var selectedRewardButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

        int rewardButtonIndex = rewardButtons.IndexOf(selectedRewardButton);

        rewardImages[rewardButtonIndex].SetActive(true);
    }

    public void OpenPremiumRewardPopup() {
        var selectedPremiumRewardButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

        int premiumRewardButtonIndex = premiumRewardButtons.IndexOf(selectedPremiumRewardButton);

        premiumRewardImages[premiumRewardButtonIndex].SetActive(true);
    }

    public void reklamGoster(float chanceToWatch) {
        if (Random.value > chanceToWatch) {
            ReklamScript.InsterstitialGoster();
        }
    }

    public void OpenRanking()
    {
        //FireStoreManager.Instance.GetCurrentGameRanking();
    }

    public void OpenInstagram()
    {
        Application.OpenURL("https://www.instagram.com/gameofwinners_/");
    }

    public void GoToMainMenuMarket() {
        PlayerPrefs.SetInt("ShowMarket", 1);
        SceneManager.LoadScene("MainMenu_Main");
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu_Main");
    }
}
