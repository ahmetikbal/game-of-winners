﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public static bool isMerged = false;

    public Cell right;
    public Cell left;
    public Cell down;
    public Cell up;

    public Fill fill;

    private void OnEnable()
    {
        GameController_BlockMerger.slide += OnSlide;
    }

    private void OnDisable()
    {
        GameController_BlockMerger.slide -= OnSlide;
    }

    private void OnSlide(string whatWasSent)
    {
        Cell currentCell = this;

        if (currentCell.fill == null)
            return;

        if (whatWasSent == "w")
        {
            if (up != null)
                return;

            SlideUp(currentCell);
        }

        if (whatWasSent == "CheckTriple") {
            print("CheckTriple");
            StartCoroutine(TripleMergeDelay(currentCell));
        }

        if (whatWasSent == "CheckDouble") {
            print("CheckDouble");
            StartCoroutine(DoubleMergeDelay(currentCell));
        }
    }

    IEnumerator TripleMergeDelay(Cell currentCell) {
        yield return new WaitForSeconds(.3f);
        TripleMerge(currentCell);
    }

    IEnumerator DoubleMergeDelay(Cell currentCell) {
        yield return new WaitForSeconds(.3f);
        DoubleMerge(currentCell);
    }

    void FixedUpdate() {
        Cell currentCell = this;

        if (currentCell.transform.childCount > 0) {
            if (currentCell.fill == null) {
                foreach (Transform item in currentCell.transform) {
                    Destroy(item.gameObject);
                }
            }
        }

        if (currentCell.down != null && currentCell.fill == null) {
            SlideUp(currentCell);
        }
    }

    void TripleMerge(Cell currentCell) {
        if (currentCell.fill == null)
            return;

        /* Triple and Quadra Situation
         * y  x  y
         * x  () x
         * 
         * */
        if (currentCell.up != null && currentCell.left != null && currentCell.right != null) {
            if (currentCell.up.fill != null && currentCell.right.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.left.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    Debug.Log("QUADRA BABY!!!! - ");
                    currentCell.fill.Quadruple();

                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.left.fill = null;

                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.right.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;

                    isMerged = true;
                    return;
                }
            }
            /* Triple variation 2
             * x  y
             * () x
             * 
             * */
            if (currentCell.up.fill != null && currentCell.right.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    Debug.Log("TRIPLE BABY! - ");
                    currentCell.fill.Triple();

                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.right.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    print("Triple and Quadra Situation - Done - ");

                    isMerged = true;
                    return;
                }
            }
            /* Triple variation 3
             * y  x
             * x  ()
             * 
             * */
            if (currentCell.up.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.left.fill.value) {
                    Debug.Log("TRIPLE BABY! - ");
                    currentCell.fill.Triple();

                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.left.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    print("Triple and Quadra Situation - Done - ");

                    isMerged = true;
                    return;
                }
            }
            /* Triple variation 4
             * x  ()  x
             * */
            if (currentCell.left != null && currentCell.right != null) {
                if (currentCell.right.fill != null && currentCell.left.fill != null) {
                    if (currentCell.fill.value == currentCell.left.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                        currentCell.left.fill.transform.SetParent(currentCell.transform);
                        currentCell.fill = currentCell.left.fill;
                        currentCell.left.fill = null;


                        currentCell.right.fill.transform.SetParent(currentCell.transform);
                        currentCell.fill = currentCell.right.fill;
                        currentCell.right.fill = null;

                        Debug.Log("TRIPLE BABY! - ");
                        currentCell.fill.Triple();

                        isMerged = true;
                        return;
                    }
                }

                print("Triple variation 4 - ");
            }

            print("Triple and Quadra Situation - ");

        }
        /* Triple variation 2
         * x  y
         * () x
         * 
         * */
        else if (currentCell.up != null && currentCell.right != null) {
            if (currentCell.up.fill != null && currentCell.right.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    Debug.Log("TRIPLE BABY! - ");
                    currentCell.fill.Triple();

                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.right.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;
                    print("Triple and Quadra Situation - Done - ");

                    isMerged = true;
                    return;
                }
            }

            print("Triple variation 2 - ");
        }
        /* Triple variation 3
         * y  x
         * x  ()
         * 
         * */
        else if (currentCell.up != null && currentCell.left != null) {
            if (currentCell.up.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value && currentCell.fill.value == currentCell.left.fill.value) {
                    Debug.Log("TRIPLE BABY! - ");
                    currentCell.fill.Triple();

                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.left.fill = null;

                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;

                    isMerged = true;
                    return;
                }
            }

            print("Triple variation 3 - ");
        }
        /* Triple variation 4
         * x  ()  x
         * */
        else if (currentCell.left != null && currentCell.right != null) {
            if (currentCell.right.fill != null && currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.left.fill.value && currentCell.fill.value == currentCell.right.fill.value) {
                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.left.fill;
                    currentCell.left.fill = null;


                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.right.fill;
                    currentCell.right.fill = null;

                    Debug.Log("TRIPLE BABY! - ");
                    currentCell.fill.Triple();

                    isMerged = true;
                    return;
                }
            }

            print("Triple variation 4 - ");
        }
    }

    void DoubleMerge(Cell currentCell) {
        if (currentCell.fill == null)
            return;

        if (isMerged)
            return;

        // Double Variation 1
        if (currentCell.up != null) {
            if (currentCell.up.fill != null) {
                if (currentCell.fill.value == currentCell.up.fill.value) {
                    currentCell.fill.Double();
                    Debug.Log("Doubled up - ");
                    currentCell.fill.transform.SetParent(currentCell.up.transform);
                    currentCell.up.fill = currentCell.fill;
                    currentCell.fill = null;

                    isMerged = true;
                    return;
                }
                print("Double Variation 1 - ");
            }
        }
        // Double Variation 2
        if (currentCell.left != null) {
            if (currentCell.left.fill != null) {
                if (currentCell.fill.value == currentCell.left.fill.value) {
                    currentCell.left.fill.Double();
                    Debug.Log("Doubled right - ");
                    currentCell.left.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.left.fill;
                    currentCell.left.fill = null;

                    isMerged = true;
                    return;
                }
                print("Double Variation 2 - ");
            }
        }
        // Double Variation 3
        if (currentCell.right != null) {
            if (currentCell.right.fill != null) {
                if (currentCell.fill.value == currentCell.right.fill.value) {
                    currentCell.right.fill.Double();
                    Debug.Log("Doubled left - ");
                    currentCell.right.fill.transform.SetParent(currentCell.transform);
                    currentCell.fill = currentCell.right.fill;
                    currentCell.right.fill = null;

                    isMerged = true;
                    return;
                }
                print("Double Variation 3 - ");
            }
        }

    }

    public void SlideUp(Cell currentCell)
    {
        if (currentCell.down == null)
            return;

        Cell nextCell = currentCell.down;
        while (nextCell.down != null && nextCell.fill == null) {
            nextCell = nextCell.down;
        }

        if (nextCell.fill != null) {
            if (currentCell.fill == null) {
                nextCell.fill.transform.SetParent(currentCell.transform);
                currentCell.fill = nextCell.fill;
                nextCell.fill = null;
                // Debug.Log("Slide to Empty");
            }
        }

        if (currentCell.down == null)
            return;

        SlideUp(currentCell.down);
    }
}
