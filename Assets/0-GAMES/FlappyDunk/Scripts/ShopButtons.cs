﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShopButtons : MonoBehaviour
{
	public GameObject player;
	public GameObject wing;


	public void ChangeSkin(int i)
	{
		player.GetComponent<SpriteRenderer>().sprite = player.GetComponent<BallMove>().skins[i];
		PlayerPrefs.SetInt("Skin",i);
	}
	public void ChangeWing(int i)
	{
		wing.GetComponent<SpriteRenderer>().sprite = player.GetComponent<BallMove>().wings[i];
		PlayerPrefs.SetInt("Wing",i);
	}
	public void ChangeTheme(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}
}
