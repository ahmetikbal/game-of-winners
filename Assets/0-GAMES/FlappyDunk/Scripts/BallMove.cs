﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;
using MoreMountains.NiceVibrations;
using TMPro;

public class BallMove : MonoBehaviour
{
    public static BallMove Instance;

	public float jumpForce = 1600;
	public float speed = 0.7f;
	public ObscuredInt score;
	bool bonus;
	public bool dead = false;
	bool gemBonus;
	public bool started = false;
	public float bonusTimer=0;
	public float gemBonusTimer=0;
	public GameObject adScreen;
	public GameObject prefabSpawner;
	public GameObject scoreText;
	public GameObject wing;
	public GameObject wing2;
	//public GameObject accesory;
	public GameObject bonusTimerUI;
	public GameObject gemBonusTimerUI;
	public GameObject effect;
	public GameObject shield;
	public GameObject startMenu;

	public AudioSource[] sources;
	public AudioClip[] wingSounds;
	public AudioClip[] powerupSounds;
	public AudioClip[] hitSounds;

	public Sprite[] skins;
	public Sprite[] wings;
	public GameObject[] accesories;
	public Button pauseButton;
    public float gameTimer;
	bool pauseBool;
	float pauseTimer;

	[Header("Unity Stuffs")]
	public GameObject rewardPopup;
	public GameObject showRewardPopup;
	public TextMeshProUGUI rewardText;
	public GameObject treasureButton;
	public Sprite treasureSprite;

	bool isInvincible = false;

	private void Awake()
	{
        if (Instance == null) Instance = this;

		if (PlayerPrefs.HasKey("Skin")) { this.gameObject.GetComponent<SpriteRenderer>().sprite = skins[PlayerPrefs.GetInt("Skin")]; }
		else { this.gameObject.GetComponent<SpriteRenderer>().sprite = skins[0]; }
		if (PlayerPrefs.HasKey("Wing"))
		{
			wing.GetComponentInChildren<SpriteRenderer>().sprite = wings[PlayerPrefs.GetInt("Wing")];
			wing2.GetComponent<SpriteRenderer>().sprite = wings[PlayerPrefs.GetInt("Wing")];
		}
		else
		{
			wing.GetComponentInChildren<SpriteRenderer>().sprite = wings[0];
			wing2.GetComponent<SpriteRenderer>().sprite = wings[0];
		}
		if (PlayerPrefs.HasKey("Accesories")){accesories[PlayerPrefs.GetInt("Accesories")-1].SetActive(true);}
		else{accesories[0].SetActive(true);}

		StartCoroutine(advanceSpeed());
	}

	IEnumerator advanceSpeed()
	{
		while (true) {
			yield return new WaitForSeconds(60f);
			speed += 1;
		}
	}

	public void randomReward() {
		ReklamScript.RewardedReklamGoster(RewardedVideoReward);
		// RewardedVideoReward(null);
	}

	public void RewardedVideoReward(GoogleMobileAds.Api.Reward odul)
    {
		int rndm = Random.Range(0, 102);

        if (gameTimer <= 120) {
			reward(rndm);
		} else {
			reward2(rndm);
		}
		
	}

	public void InvincibleBonus() {
		shield.SetActive(true);
		bonus = true;
		bonusTimerUI.SetActive(true);
		bonusTimer = 3;
		bonusTimerUI.GetComponent<Slider>().maxValue = bonusTimer;
		bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
		// rewardText.text = "3 SANIYELIK KALKAN KAZANDINIZ";
	}

	public void reward(int rewardId) {
		if (rewardId >= 0 && rewardId <= 30) {
			// +100 Point - 30%
			score += 100;
			rewardText.text = "+100 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 31 && rewardId <= 50) {
			// +200 Point - 20%
			score += 200;
			rewardText.text = "+200 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 51 && rewardId <= 65) {
			// +300 Point - 15%
			score += 300;
			rewardText.text = "+300 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 66 && rewardId <= 75) {
			// +400 Point - 10%
			score += 400;
			rewardText.text = "+400 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 76 && rewardId <= 90) {
			// 5 Saniye Kalkan - 15%
			shield.SetActive(true);
			bonus = true;
			bonusTimerUI.SetActive(true);
			bonusTimer = 5;
			bonusTimerUI.GetComponent<Slider>().maxValue = bonusTimer;
			bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
			rewardText.text = "5 SANIYELIK KALKAN KAZANDINIZ";
		}
		else if (rewardId >= 91 && rewardId <= 100) {
			// 10 Saniye Kalkan - 10%
			shield.SetActive(true);
			bonus = true;
			bonusTimerUI.SetActive(true);
			bonusTimer = 10;
			bonusTimerUI.GetComponent<Slider>().maxValue = bonusTimer;
			bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
			rewardText.text = "10 SANIYELIK KALKAN KAZANDINIZ";
		}
		else if (rewardId == 101) {
			// 20 Saniye Kalkan - %1
			Debug.Log("zink");
			shield.SetActive(true);
			bonus = true;
			bonusTimerUI.SetActive(true);
			bonusTimer = 20;
			bonusTimerUI.GetComponent<Slider>().maxValue = bonusTimer;
			bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
			rewardText.text = "20 SANIYELIK KALKAN KAZANDINIZ, TEBRIKLER!";
		}
		rewardPopup.SetActive(false);

		StartCoroutine(showRewardsWithDelay());
	}

	public void reward2(int rewardId) {
		if (rewardId >= 0 && rewardId <= 30) {
			// +50 Point - 30%
			score += 50;
			rewardText.text = "+50 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 31 && rewardId <= 50) {
			// +75 Point - 20%
			score += 75;
			rewardText.text = "+75 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 51 && rewardId <= 65) {
			// +100 Point - 15%
			score += 100;
			rewardText.text = "+100 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 66 && rewardId <= 75) {
			// +150 Point - 10%
			score += 150;
			rewardText.text = "+150 PUAN KAZANDINIZ";
		}
		else if (rewardId >= 76 && rewardId <= 90) {
			// 5 Saniye Kalkan - 15%
			shield.SetActive(true);
			bonus = true;
			bonusTimerUI.SetActive(true);
			bonusTimer = 5;
			bonusTimerUI.GetComponent<Slider>().maxValue = bonusTimer;
			bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
			rewardText.text = "5 SANIYELIK KALKAN KAZANDINIZ";
		}
		else if (rewardId >= 91 && rewardId <= 100) {
			// 10 Saniye Kalkan - 10%
			shield.SetActive(true);
			bonus = true;
			bonusTimerUI.SetActive(true);
			bonusTimer = 10;
			bonusTimerUI.GetComponent<Slider>().maxValue = bonusTimer;
			bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
			rewardText.text = "10 SANIYELIK KALKAN KAZANDINIZ";
		}
		else if (rewardId == 101) {
			// 20 Saniye Kalkan - %1
			Debug.Log("zink");
			shield.SetActive(true);
			bonus = true;
			bonusTimerUI.SetActive(true);
			bonusTimer = 20;
			bonusTimerUI.GetComponent<Slider>().maxValue = bonusTimer;
			bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
			rewardText.text = "20 SANIYELIK KALKAN KAZANDINIZ, TEBRIKLER!";
		}

		// For not getting crash
		StartCoroutine(showRewardsWithDelay());
	}

	IEnumerator showRewardsWithDelay()
    {
		yield return new WaitForSecondsRealtime(0.1f); // For escaping from Time.timeScale 0
		showRewardPopup.SetActive(true);

		treasureButton.GetComponent<Image>().sprite = treasureSprite;
		treasureButton.GetComponent<Button>().interactable = true;
	}

	void Update()
    {
		if (Input.GetKeyDown(KeyCode.E)) {
			rewardPopup.SetActive(true);
		}

        if (!started) {
			if (!dead) {
				if (rewardPopup.activeSelf || showRewardPopup.activeSelf)
					return;

				Time.timeScale = 0;
				startMenu.SetActive(true);
			}


			if (Input.GetMouseButtonDown(0) && !dead) {
				if (rewardPopup.activeSelf || showRewardPopup.activeSelf)
					return;
				started = true;
				startMenu.SetActive(false);
				Time.timeScale = 1;
			}
		}

		if(started)
		{
			if (rewardPopup.activeSelf || showRewardPopup.activeSelf) {
				Time.timeScale = 0;
				started = false;
			}
				

			if (speed >= 7) {
				speed = 7;
				StopCoroutine("advanceSpeed");
			}
				

			this.gameObject.transform.position += transform.right * Time.deltaTime * speed;

			if (Input.GetMouseButtonDown(0))
			{
				
				if (Time.timeScale != 1) {
					Time.timeScale = 1;
					startMenu.SetActive(false);
					if (pauseButton.interactable == false) {
						pauseBool = true;
					}
				}

				this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(this.gameObject.GetComponent<Rigidbody2D>().velocity.x, 0);
				this.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce);
				wing.GetComponent<Animator>().Play("flapping");
				if (gemBonus || bonus)
				{
					if (gemBonus && bonus)
					{
						sources[0].clip = wingSounds[2];
						sources[0].Play();
					}
					else
					{
						sources[0].clip = wingSounds[1];
						sources[0].Play();
					}
				}
				else
				{
					sources[0].clip = wingSounds[0];
					sources[0].Play();
				}
			}

			if (pauseBool == true)
			{
				if(pauseTimer<1)
				{
					pauseTimer += Time.deltaTime;
				}
				else
				{
					pauseBool = false;
					pauseTimer = 0;
					pauseButton.interactable = true;
				}
			}
			if (gemBonus)
			{
				gemBonusTimer -= Time.deltaTime;
				gemBonusTimerUI.GetComponent<Slider>().value = gemBonusTimer;
				this.gameObject.transform.position += transform.right * Time.deltaTime * 6;
				prefabSpawner.GetComponent<PrefabSpawner>().timer = .1f;
				if (gemBonusTimer <= 0) { gemBonus = false; }
			}
			else
			{
				gemBonusTimerUI.SetActive(false);
			}

			if (bonus)
			{
				bonusTimer -= Time.deltaTime;
				bonusTimerUI.GetComponent<Slider>().value = bonusTimer;
				this.gameObject.transform.position += transform.right * Time.deltaTime*6;
				prefabSpawner.GetComponent<PrefabSpawner>().timer = .1f;
				if (bonusTimer <= 0) { bonus = false; }
			}
			else
			{
				shield.SetActive(false);
				bonusTimerUI.SetActive(false);
			}

			if (!bonus && !gemBonus)
			{
				this.gameObject.transform.position += transform.right * Time.deltaTime * 5;
				prefabSpawner.GetComponent<PrefabSpawner>().timer = .1f;
			}


            if (!dead)
                gameTimer += Time.deltaTime;
			
		}
		scoreText.GetComponent<Text>().text = score.ToString();
		
    }

	public void MakeInvincible() {
		if (isInvincible)
			isInvincible = false;
		else
			isInvincible = true;
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
        if (isInvincible) 
			return;

		switch (collision.gameObject.tag)
		{
			case "Obstacle":
				if (bonus)
				{
					sources[2].clip = hitSounds[1];
					sources[2].Play();
					Destroy(collision.gameObject);
					Instantiate(effect, collision.gameObject.transform.position, 
					Quaternion.identity);
				}
				else
				{
					sources[2].clip = hitSounds[0];
					sources[2].Play();
					adScreen.SetActive(true);
					started = false;
					dead = true;
                    MMVibrationManager.Haptic(HapticTypes.Failure);

                    ReklamScript.BannerGoster();
                }
				break;
			case "MovingObstacle":
				if (bonus) {
					sources[2].clip = hitSounds[1];
					sources[2].Play();
					Destroy(collision.gameObject);
					Instantiate(effect, collision.gameObject.transform.position,
					Quaternion.identity);
				}
				else {
					sources[2].clip = hitSounds[0];
					sources[2].Play();
					adScreen.SetActive(true);
					started = false;
					dead = true;
					MMVibrationManager.Haptic(HapticTypes.Failure);

					ReklamScript.BannerGoster();
				}
				break;
			case "Ground":
				sources[2].clip = hitSounds[0];
				sources[2].Play();

                started = false;
                dead = true;
                adScreen.SetActive(true);

                MMVibrationManager.Haptic(HapticTypes.Failure);

                ReklamScript.BannerGoster();
                break;
		}
	}
	private void OnTriggerEnter2D(Collider2D collision)
	{
		switch (collision.gameObject.tag)
		{
			case "Coin":
				sources[1].clip = powerupSounds[0];
				sources[1].Play();

                if (gameTimer <= 60) {
					score += 15;
				}
				else if (gameTimer <= 120) {
					score += 5;
				} 
				else {
					score += 1;
				}
				
				Destroy(collision.gameObject);
				break;
			case "Bonus":
				sources[1].clip = powerupSounds[1];
				sources[1].Play();
				shield.SetActive(true);
				bonus = true;
				bonusTimerUI.SetActive(true);
				bonusTimer = 5;
				prefabSpawner.GetComponent<PrefabSpawner>().timer = .5f;
				Destroy(collision.gameObject);
				break;
			case "GemBonus":
				sources[1].clip = powerupSounds[2];
				sources[1].Play();
				gemBonus = true;
				// score += 20;
				gemBonusTimerUI.SetActive(true);
				gemBonusTimer = 5;
				prefabSpawner.GetComponent<PrefabSpawner>().timer = .5f;
				Destroy(collision.gameObject);
				break;
			case "BonusBox":
				sources[1].clip = powerupSounds[2];
				sources[1].Play();
				rewardPopup.SetActive(true);
				Destroy(collision.gameObject);
				break;
		}
	}
}
