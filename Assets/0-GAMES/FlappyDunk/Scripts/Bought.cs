﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bought : MonoBehaviour
{
	public bool isBought;
	public int itemNumber;

	private void Awake()
	{
		if (PlayerPrefs.GetInt(itemNumber.ToString()) == 1) { isBought=true; this.gameObject.GetComponent<Button>().interactable = true; }
	}
}
