﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flappy_Destroyer : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision) {
        Destroy(collision.gameObject);
    }
    void OnCollisionEnter2D(Collision2D collision) {
        Destroy(collision.gameObject);
    }
}
