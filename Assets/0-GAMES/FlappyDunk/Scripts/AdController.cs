﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.Storage;

public class AdController : MonoBehaviour
{
	public int remainingRevives=1;
	public GameObject player;
	public GameObject resetColider;
    public GameObject startMenu;

	public void PlayAd()
	{
        if (remainingRevives > 0)
        {
            ReklamScript.RewardedReklamGoster(RewardAndPlay);
            EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
        }
        else {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
	}

    public void RewardAndPlay(GoogleMobileAds.Api.Reward reward)
    {
        remainingRevives -= 1;

        player.transform.position = new Vector3(player.transform.position.x, -0.35f, player.transform.position.z);
        resetColider.transform.position = new Vector3(player.transform.position.x, -0.35f, player.transform.position.z);
        player.GetComponent<BallMove>().dead = false;

        ReklamScript.BannerGizle();

        this.gameObject.SetActive(false);
        Time.timeScale = 1;

        BallMove.Instance.InvincibleBonus();
    }

	public void Multiply()
	{
        //Play ad
        ReklamScript.RewardedReklamGoster(MultiplyReward);
	}

    public void MultiplyReward(GoogleMobileAds.Api.Reward odul)
    {
        player.GetComponent<BallMove>().score *= 3;
        if (ObscuredPrefs.GetInt(UIController.instance.highScoreName) < player.GetComponent<BallMove>().score) {
            ObscuredPrefs.SetInt(UIController.instance.highScoreName, player.GetComponent<BallMove>().score);
            //FireStoreManager.Instance.CreateFlappyDunkScore();
            FireStoreManager.Instance.CreateCurrentGameScore(BallMove.Instance.gameTimer);
        }
        SceneManager.LoadScene("Start");
    }


	public void Restart()
	{
        Time.timeScale = 1;
        ReklamScript.InsterstitialGoster();


		if (ObscuredPrefs.GetInt(UIController.instance.highScoreName) < player.GetComponent<BallMove>().score) {
            ObscuredPrefs.SetInt(UIController.instance.highScoreName, player.GetComponent<BallMove>().score);
            //FireStoreManager.Instance.CreateFlappyDunkScore();
            FireStoreManager.Instance.CreateCurrentGameScore(BallMove.Instance.gameTimer);
        }

        //	SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene("Start");
	}

    public void Retry() {

        if (Random.value > 0.5f)
            ReklamScript.InsterstitialGoster();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Pause()
    {
        startMenu.SetActive(true);
        Time.timeScale = 0;
    }


    public void OpenInstagram()
    {
        Application.OpenURL("https://www.instagram.com/gameofwinners_/");
    }
}
