﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delete : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
	{
		print(collision.gameObject.name);
		if (collision.gameObject.tag == "Obstacle") {
			Destroy(collision.gameObject);
		}
	}
    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Obstacle") {
			Destroy(collision.gameObject);
		}
	}
}
