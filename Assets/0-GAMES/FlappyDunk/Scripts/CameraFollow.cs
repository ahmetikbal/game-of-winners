﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

	public GameObject followGameObject;
    public int length = 2;

    void Update()
    {
		this.gameObject.transform.SetPositionAndRotation(new Vector3(followGameObject.transform.position.x + length, this.gameObject.transform.position.y,this.gameObject.transform.position.z),Quaternion.identity);
    }
}
