﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Watched : MonoBehaviour
{
	public int adNumber;

	private void Awake()
	{
		if (PlayerPrefs.GetInt(adNumber.ToString()) == 1) {this.gameObject.GetComponent<Button>().interactable = false; }
	}
}
