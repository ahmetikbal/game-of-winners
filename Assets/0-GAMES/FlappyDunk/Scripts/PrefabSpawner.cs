﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine.UI;

public class PrefabSpawner : MonoBehaviour
{
	[Header("Obstacles")]
	public GameObject[] upObstacles;
	public GameObject[] downObstacles;
	public GameObject[] midObstacle;
	public GameObject[] movingObstacle;

	[Header("Points/Bonuses")]
	public GameObject pointPrefab;

	public GameObject bonusBox;
	private int bonusBoxSpawnTimer = 230;

	[Header("Settings")]
	public int spawnRange = 10;
	public int spawnRangeBonus = 10;
	public float timer = 2;
	float timerSave;
	public int xAxis, xAxisPoint;
	GameObject player;
	public ObscuredFloat elapsedTime;
	private bool isDensityAdvanced = false;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		timerSave = timer;
		// StartCoroutine(SpawnBonus());
	}

	void FixedUpdate()
	{
		elapsedTime += Time.deltaTime;


		if (elapsedTime > 300 && isDensityAdvanced == false) {
			StartCoroutine(advanceDensity());
			isDensityAdvanced = true;
		}

		if (timerSave > 0) { 
			timerSave -= Time.deltaTime; 
		}
		else
		{
			if (xAxisPoint - player.transform.position.x < 10) {
				SpawnPoint();
			}

			if (xAxis - player.transform.position.x < 10) {
				SpawnObstacle();
			}
		}
	}

	/*
	void SpawnObstacleBackup() {
		float randomNumber = Random.value;

		if (randomNumber > obstacleToPointRatio) {
			float randomNumberPoint = Random.value;
			if (randomNumberPoint > 0.1f) {
				switch (Random.Range(1, 3)) {
					case 1:
						int randomItem = Random.Range(0, downObstacles.Length);
						int randomItem2 = Random.Range(0, midPoints.Length);

						if (randomItem == downObstacles.Length - 1) {
							Instantiate(downObstacles[randomItem], downObstacles[randomItem].transform.position + new Vector3(xAxis + 8, 0, 0), downObstacles[randomItem].transform.rotation, this.gameObject.transform);
							InstantiatePrefab();
						}
						else {
							Instantiate(downObstacles[randomItem], downObstacles[randomItem].transform.position + new Vector3(xAxis + 5, 0, 0), downObstacles[randomItem].transform.rotation, this.gameObject.transform);
							if (Random.value < .95f) {
								GameObject point = Instantiate(midPoints[randomItem2], midPoints[randomItem2].transform.position + new Vector3(xAxis + 5, 0, 0), midPoints[randomItem2].transform.rotation, this.gameObject.transform);
								point.transform.position = new Vector3(point.transform.position.x, Random.Range(-4, 6), point.transform.position.z);
							}
							InstantiatePrefab();
						}
						break;
					case 2:
						int randomItem3 = Random.Range(0, upObstacles.Length);
						int randomItem4 = Random.Range(0, midPoints.Length);
						Instantiate(upObstacles[randomItem3], upObstacles[randomItem3].transform.position + new Vector3(xAxis + 5, 0, 0), upObstacles[randomItem3].transform.rotation, this.gameObject.transform);
						if (Random.value < .95f) {
							GameObject point = Instantiate(midPoints[randomItem4], midPoints[randomItem4].transform.position + new Vector3(xAxis + 5, 0, 0), midPoints[randomItem4].transform.rotation, this.gameObject.transform);
							point.transform.position = new Vector3(point.transform.position.x, Random.Range(-4, 6), point.transform.position.z);
						}
						InstantiatePrefab();
						break;
				}
			}
			else {
				switch (Random.Range(1, 4)) {
					case 1:
						int randomItem = Random.Range(0, downObstacles.Length);
						Instantiate(downObstacles[randomItem], downObstacles[randomItem].transform.position + new Vector3(xAxis + 5, 0, 0), downObstacles[randomItem].transform.rotation, this.gameObject.transform);
						InstantiatePrefab();
						break;
					case 2:
						int randomItem2 = Random.Range(0, upObstacles.Length);
						Instantiate(upObstacles[randomItem2], upObstacles[randomItem2].transform.position + new Vector3(xAxis + 5, 0, 0), upObstacles[randomItem2].transform.rotation, this.gameObject.transform);
						InstantiatePrefab();
						break;
					case 3:
						int test = Random.Range(0, midObstacle.Length);
						Instantiate(midObstacle[test], new Vector3(0, 0, 0) + new Vector3(xAxis + 5, 0, 0), midObstacle[test].transform.rotation, this.gameObject.transform);
						InstantiatePrefab();
						break;
				}
			}
		}
	}
	*/

	IEnumerator advanceDensity() {
		while (true) {
			yield return new WaitForSeconds(30f);
			spawnRange -= 1;
			spawnRangeBonus += 10;
		}
	}

	void SpawnObstacle()
	{
		switch (Random.Range(1, 5))
		{
			case 1:
				int randomItem = Random.Range(0, downObstacles.Length);
				Instantiate(downObstacles[randomItem], downObstacles[randomItem].transform.position + new Vector3(xAxis + 5, 0, 0), downObstacles[randomItem].transform.rotation, this.gameObject.transform);
				RangeArrange();
				break;
			case 2:
				int randomItem2 = Random.Range(0, upObstacles.Length);
				Instantiate(upObstacles[randomItem2], upObstacles[randomItem2].transform.position + new Vector3(xAxis + 5, 0, 0), upObstacles[randomItem2].transform.rotation, this.gameObject.transform);
				RangeArrange();
				break;
			case 3:
				int randomItem3 = Random.Range(0, midObstacle.Length);
				// Instantiate(midObstacle[randomItem3], midObstacle[randomItem3].transform.position + new Vector3(xAxis + 5, 0, 0), midObstacle[randomItem3].transform.rotation, this.gameObject.transform);
				GameObject midGo = Instantiate(midObstacle[randomItem3], new Vector3(xAxis + 5, Random.Range(-2f, 2f), 0), midObstacle[randomItem3].transform.rotation, this.gameObject.transform);
				midGo.transform.position = new Vector3(midGo.transform.position.x, Random.Range(-2, 2), midGo.transform.position.z);
				RangeArrange();
				break;
			case 4:
                if (elapsedTime > 120 && movingObstacle.Length > 0) {
					int randomItem4 = Random.Range(0, movingObstacle.Length);
					Instantiate(movingObstacle[randomItem4], movingObstacle[randomItem4].transform.position + new Vector3(xAxis + 5, 0, 0), movingObstacle[randomItem4].transform.rotation, this.gameObject.transform);
					RangeArrange();
				} else {
					int randomItem5 = Random.Range(0, downObstacles.Length);
					Instantiate(downObstacles[randomItem5], downObstacles[randomItem5].transform.position + new Vector3(xAxis + 5, 0, 0), downObstacles[randomItem5].transform.rotation, this.gameObject.transform);
					RangeArrange();
				}
				break;
		}
	}

	void SpawnPoint() {
		GameObject point = Instantiate(pointPrefab, pointPrefab.transform.position + new Vector3(xAxis + 5, 0, 0), pointPrefab.transform.rotation, this.gameObject.transform);
		point.transform.position = new Vector3(point.transform.position.x + Random.Range(-3f, 3f), Random.Range(-4, 6), point.transform.position.z);

		xAxisPoint += spawnRangeBonus;
	}

	void RangeArrange()
	{
		xAxis += spawnRange;
		timerSave = timer;
	}

	IEnumerator SpawnBonus() {
		while (true) {
			yield return new WaitForSeconds(10f);
			GameObject spawnedBonus = Instantiate(bonusBox, new Vector3(Random.Range(bonusBoxSpawnTimer, bonusBoxSpawnTimer + 30), 0, 0), this.transform.rotation, this.gameObject.transform);
			spawnedBonus.tag = "BonusBox";
			bonusBoxSpawnTimer += 230;
		}
	}
}
