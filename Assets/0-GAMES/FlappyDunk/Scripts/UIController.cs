﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using CodeStage.AntiCheat.Storage;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    private string mapName = "Underwater";

    public GameObject mainMenu, rankingMenu, popUpMenu, zatenGirisYaptinPanel;
    public GameObject ForceUpdatePanel, KaydolmadinPanel;

    public GameObject[] canvases;
	public Sprite[] nameplates;
	public GameObject shopName;
	public GameObject highscoreText;
	public GameObject shop;
	public GameObject buttons;
	public GameObject[] characterButtons;
	public GameObject[] mapButtons;
	public GameObject[] accesoriesButtons;
	public GameObject[] wingButtons;
	int canvasLayer=0;
	int activeScene;

    public Text nicknameText;

    public GameObject rewardsPanel;

    private int adCounter = 0;

    public int rewardedJ;

    public string highScoreName = "Highscore_FlappyXmas";

	private void Awake()
	{
        if (instance == null) instance = this;

        if (!PlayerPrefs.HasKey("firstOpen"))
        {
            PlayerPrefs.SetInt("WatchedAdMap" + 1, 1);
            PlayerPrefs.SetInt("Map", 2);

            PlayerPrefs.SetInt("WatchedAdWing" + 0, 1);
            PlayerPrefs.SetInt("Wing", 0);

            PlayerPrefs.SetInt("WatchedAdAccesory" + 1, 1);
            PlayerPrefs.SetInt("Accesories", 1);

            PlayerPrefs.SetInt("WatchedAdCharacter" + 0, 1);
            PlayerPrefs.SetInt("WatchedAdCharacter" + 1, 1);
            PlayerPrefs.SetInt("WatchedAdCharacter" + 2, 1);
            PlayerPrefs.SetInt("Skin", 0);

            PlayerPrefs.SetInt("firstOpen", 1);
        }

        nicknameText.text = PlayerPrefs.GetString("username", "PLAYER");

        adCounter = PlayerPrefs.GetInt("adCounter", 0);
		
		if (PlayerPrefs.HasKey("Map"))
		{
			activeScene = PlayerPrefs.GetInt("Map");
		}
		else
		{
			activeScene = 1;
		}

        /*
        if (!PlayerPrefs.HasKey("popup_flappydunk"))
        {
            popUpMenu.SetActive(true);
        }
        */

      //  AntiCheatToolKitFix(); //OBSCUREDPREFSE GEÇİNCE VERİ KAYBI OLMAMASI İÇİN KULLANILDI

    }

    private static bool oneAd;
    private void Start()
    {
        ReklamScript.BannerGoster();

        if (!oneAd)
        {
            if (Random.value > 0.5f)
                ReklamScript.InsterstitialGoster();

            oneAd = true;
        }

        //TimeManager.Instance.RemainTime();

           var secondWord = GameManager.Instance.goWSettings.currentGameSettingsAsset.gameName.Split(' ')[1];
           mapName = secondWord;

        highScoreName = GameManager.Instance.currentHighScorePlayerPrefsName;

        highscoreText.GetComponent<Text>().text = ObscuredPrefs.GetInt(highScoreName).ToString();
    }
    /*
    public void AntiCheatToolKitFix()
    {
        if (!PlayerPrefs.HasKey("AntiCheat"))
        {
            if(PlayerPrefs.HasKey("Highscore")){

                ObscuredPrefs.SetInt("Highscore", PlayerPrefs.GetInt("Highscore"));
            }
            PlayerPrefs.SetInt("AntiCheat", 1);
        }
    }
    */

    public Text newGameTimerText;
    private void LateUpdate()
    {
        newGameTimerText.text = TimeManager.Instance.GetRemainingTimeText();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
            SelectCharacterReward(null);

        if (Input.GetKeyDown(KeyCode.Y))
            BuyMapsReward(null);

        if (Input.GetKeyDown(KeyCode.U))
            SelectAccesoriesReward(null);

        if (Input.GetKeyDown(KeyCode.I))
            SelectWingReward(null);
    }

    public void RestoreBoughts()
    {
        for (int i = 0; i < characterButtons.Length; i++)
        {
            if (PlayerPrefs.GetInt("WatchedAdCharacter" + i) == 1)
            {
                characterButtons[i].GetComponent<Text>().text = "SEC";
            }
        }
        for (int i = 0; i < wingButtons.Length; i++)
        {
            if (PlayerPrefs.GetInt("WatchedAdWing" + i) == 1)
            {
                wingButtons[i].GetComponent<Text>().text = "SEC";
            }
        }
        for (int i = 0; i < accesoriesButtons.Length + 1; i++)
        {
            if (PlayerPrefs.GetInt("WatchedAdAccesory" + i) == 1)
            {
                accesoriesButtons[i-1].GetComponent<Text>().text = "SEC";
            }
        }
        for (int i = 0; i < mapButtons.Length + 1; i++)
        {
            if (PlayerPrefs.GetInt("WatchedAdMap" + i) == 1)
            {
                mapButtons[i-1].GetComponent<Text>().text = "SEC";
            }
        }

        if(PlayerPrefs.GetInt("Accesories") != 0) accesoriesButtons[PlayerPrefs.GetInt("Accesories")-1].GetComponent<Button>().interactable = false; else accesoriesButtons[0].GetComponent<Button>().interactable = false;
        characterButtons[PlayerPrefs.GetInt("Skin")].GetComponent<Button>().interactable = false;
        wingButtons[PlayerPrefs.GetInt("Wing")].GetComponent<Button>().interactable = false;
        if (PlayerPrefs.GetInt("Map") != 0) mapButtons[PlayerPrefs.GetInt("Map") - 1].GetComponent<Button>().interactable = false; else mapButtons[0].GetComponent<Button>().interactable = false;

    }


    public void CharacterButton()
	{
		canvases[0].SetActive(true);
		canvasLayer = 1;
	}
	public void MapButton()
	{
		canvases[1].SetActive(true);
		canvasLayer = 1;
	}
	public void AccesoriesButton()
	{
		canvases[2].SetActive(true);
		canvasLayer = 1;
	}
	public void BackButton()
	{
		if (canvasLayer == 0)
		{
			shop.SetActive(false);
		}
		else
		{
			canvases[0].SetActive(false);
			canvases[1].SetActive(false);
			canvases[2].SetActive(false);
			canvasLayer = 0;
			buttons.SetActive(true);
		}
	}
	public void BuyMaps(int j)
	{
     //   print(EventSystem.current.currentSelectedGameObject.transform.name);
        j = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex()+2;

		/*Temp

		PlayerPrefs.SetInt("Map", j);
		activeScene = j;

		for (int i = 0; i < mapButtons.Length; i++)
		{
			mapButtons[i].GetComponent<Button>().interactable = true;
		}
		EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
        */

		if (PlayerPrefs.GetInt("WatchedAdMap" + j) == 1)
		{
			PlayerPrefs.SetInt("Map", j);
			activeScene = j;

			for (int i = 0; i < mapButtons.Length; i++)
			{
				mapButtons[i].GetComponent<Button>().interactable = true;
			}
			EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
		}
		else
		{
            rewardedJ = j;

            //Show ad here
            ReklamScript.RewardedReklamGoster(BuyMapsReward);

		}
	}

    public void BuyMapsReward(GoogleMobileAds.Api.Reward reward)
    {
        PlayerPrefs.SetInt("WatchedAdMap" + rewardedJ, 1);
        EventSystem.current.currentSelectedGameObject.GetComponent<Text>().text = "SEC";

        PlayerPrefs.SetInt("Map", rewardedJ);
        activeScene = rewardedJ;

        for (int i = 0; i < mapButtons.Length; i++)
        {
            mapButtons[i].GetComponent<Button>().interactable = true;
        }
        EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
    }
	public void SelectCharacter(int j)
	{
        j = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();
		/* Temp  

		PlayerPrefs.SetInt("Skin", j);

		for (int i = 0; i < characterButtons.Length; i++)
		{
			characterButtons[i].GetComponent<Button>().interactable = true;
		}
		EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
        */
		
		if (PlayerPrefs.GetInt("WatchedAdCharacter" + j) == 1)
		{
			PlayerPrefs.SetInt("Skin", j);

			for (int i = 0; i < characterButtons.Length; i++)
			{
				characterButtons[i].GetComponent<Button>().interactable = true;
			}
			EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
		}
		else
		{
            rewardedJ = j;

            //Show ad here
            ReklamScript.RewardedReklamGoster(SelectCharacterReward);
        }
	}
    public void SelectCharacterReward(GoogleMobileAds.Api.Reward reward)
    {
        PlayerPrefs.SetInt("WatchedAdCharacter" + rewardedJ, 1);
        EventSystem.current.currentSelectedGameObject.GetComponent<Text>().text = "SEC";

        PlayerPrefs.SetInt("Skin", rewardedJ);

        for (int i = 0; i < characterButtons.Length; i++)
        {
            characterButtons[i].GetComponent<Button>().interactable = true;
        }
        EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
    }
	public void SelectWing(int j)
	{
        j = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex()-12;

        if (PlayerPrefs.GetInt("WatchedAdWing" + j) == 1)
		{
			PlayerPrefs.SetInt("Wing", j);

			for (int i = 0; i < wingButtons.Length; i++)
			{
				wingButtons[i].GetComponent<Button>().interactable = true;
			}
			EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
		}else
		{
            rewardedJ = j;

            ReklamScript.RewardedReklamGoster(SelectWingReward);
		}
	}

    public void SelectWingReward(GoogleMobileAds.Api.Reward reward)
    {
        PlayerPrefs.SetInt("WatchedAdWing" + rewardedJ, 1);
        EventSystem.current.currentSelectedGameObject.GetComponent<Text>().text = "SEC";

        PlayerPrefs.SetInt("Wing", rewardedJ);

        for (int i = 0; i < wingButtons.Length; i++)
        {
            wingButtons[i].GetComponent<Button>().interactable = true;
        }
        EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
    }

    public void SelectAccesories(int j)
	{
        j = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex()+1;

		/*Temp

		PlayerPrefs.SetInt("Accesories", j);

		for (int i = 0; i < accesoriesButtons.Length; i++)
		{
			accesoriesButtons[i].GetComponent<Button>().interactable = true;
		}
		EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
        */


		if (PlayerPrefs.GetInt("WatchedAdAccesory" + j) == 1)
		{
			PlayerPrefs.SetInt("Accesories", j);

			for (int i = 0; i < accesoriesButtons.Length; i++)
			{
				accesoriesButtons[i].GetComponent<Button>().interactable = true;
			}
			EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
		}
		else
		{
            rewardedJ = j;

            ReklamScript.RewardedReklamGoster(SelectAccesoriesReward);
		}
	}

    public void SelectAccesoriesReward(GoogleMobileAds.Api.Reward reward)
    {
        PlayerPrefs.SetInt("WatchedAdAccesory" + rewardedJ, 1);
        EventSystem.current.currentSelectedGameObject.GetComponent<Text>().text = "SEC";

        PlayerPrefs.SetInt("Accesories", rewardedJ);

        for (int i = 0; i < accesoriesButtons.Length; i++)
        {
            accesoriesButtons[i].GetComponent<Button>().interactable = true;
        }
        EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
    }

    public void loadScene(string map) {
        SceneManager.LoadScene(map);
    }

    public void StartGame()
	{
    //    InterstitialCounter();

        /*
        if (GameManager.Instance.newVersion > GameManager.Instance.currentVersion)
        {
            ForceUpdatePanel.SetActive(true);
        }
        else
        {
            if (!PlayerPrefs.HasKey("LoggedIn"))
            {
                KaydolmadinPanel.SetActive(true);
            }
            else
            {
                SceneManager.LoadScene(mapName);
            }
        }
        */

        SceneManager.LoadScene(mapName);

    }
    public void Shop()
    {
        //    InterstitialCounter();
        RestoreBoughts();
    }

    public void RegisterButton()
    {

        if (!PlayerPrefs.HasKey("LoggedIn"))
        {
            RegisterScene();
        }
        else
        {
            zatenGirisYaptinPanel.SetActive(true);

        }

    }

    public void RegisterScene()
    {
        SceneManager.LoadScene("Login");
    }

    public void OpenRewardsPanel()
    {
        rewardsPanel.SetActive(true);
    }
    public void CloseRewardsPanel()
    {
        rewardsPanel.SetActive(false);
    }

    public void InterstitialCounter()
    {
        adCounter++;
        PlayerPrefs.SetInt("adCounter", adCounter);

        if (adCounter == 2)
        {
            ReklamScript.InsterstitialGoster();
            adCounter = 0;
            PlayerPrefs.SetInt("adCounter", adCounter);
        }
    }

    public void MuteSound()
	{
		AudioListener.pause = true;
	}
	public void OpenSound()
	{
		AudioListener.pause = false;
	}
	public void ResetPerfs()
	{
		PlayerPrefs.DeleteAll();
	}
    public void OpenInstagram()
    {
        Application.OpenURL("https://www.instagram.com/gameofwinners_/");
    }

    public void OpenRanking()
    {
        rankingMenu.SetActive(true);
        mainMenu.SetActive(false);

        //FireStoreManager.Instance.GetCurrentGameRanking();
    }

    public void CloseRanking()
    {
        ReklamScript.InsterstitialGoster();

        rankingMenu.SetActive(false);
        mainMenu.SetActive(true);

    }

    public void PopUpClose()
    {
        PlayerPrefs.SetInt("popup_flappydunk", 1);

        popUpMenu.SetActive(false);
    }

    public void Guncelle()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.MedusaGames.GameofWinners");
    }

    public void LoadGameForKaydolmadinExitButton()
    {
        SceneManager.LoadScene(mapName);
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu_Main");
    }
}
