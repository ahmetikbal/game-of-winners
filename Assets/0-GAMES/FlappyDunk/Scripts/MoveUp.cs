﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveUp : MonoBehaviour
{
	GameObject player;
	public bool bounce;
	private void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		DOTween.SetTweensCapacity(1, 1);
	}

    void Start() {
		if (this.gameObject.transform.position.y < 0) {
			bounce = true;
		}
        else {
			bounce = false;
        }
	}

    void FixedUpdate()
    {
		if (this.gameObject.transform.position.x - player.transform.position.x < 6) {
			if (bounce) {
                if (this.gameObject.transform.position.y < 8) {
					this.gameObject.transform.position += new Vector3(0, Random.Range(0.1f, 0.2f), 0);
				}
			}
            else {
				if (this.gameObject.transform.position.y > -3) {
					this.gameObject.transform.position -= new Vector3(0, Random.Range(0.1f, 0.2f), 0);
				}
				
			}
		}

	}   
}
