﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneRotation : MonoBehaviour
{
    void Update()
    {
		this.gameObject.transform.Rotate(new Vector3(0,0,-1));

		if (this.gameObject.transform.rotation.z >= 250)
		{
			Destroy(this.gameObject);
		}
    }
}
