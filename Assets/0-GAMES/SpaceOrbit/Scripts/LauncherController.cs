﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherController : MonoBehaviour {

	/// <summary>
	/// Launcher controller simply create the missiles in the scene. It uses the parameters from other controllers
	/// (such as GameController or PlayerController) to increase the speed of missile instantiation and thus increasing 
	/// the game's difficulty.
	/// Notice that missiles that are being instantiated have their own controllers (AI) and we do not set their
	/// target, movement or anything here.
	/// </summary>

	public GameObject missile;							//Missile object we need to instantiate
	public AudioClip shootSfx;							//instantiation sfx
	private bool canCreateMissile;						//flag
	//private float missileCreationBaseDelay = 1f;		//default delay
	private float missileCreationDelay;					//actual delay 
	private float rotationZ;                            //the amount of rotation for this launcher pad

	void Start () {
		missileCreationDelay = 0;
		canCreateMissile = false;
		rotationZ = 0;
		StartCoroutine(activeMissileCreation ());
	}

	/*
	void Update() {

		//create a new missile whenever possible.
		if (canCreateMissile && !GameController.isGameFinished && GameController.isGameStarted)
			createMissile();

		//rotate the laucher pad's body and also sync the direction with the player.
		rotationZ += (Time.deltaTime + PlayerController.dir) * -0.5f * (1 + (float)GameController.level / 5);
		transform.rotation = Quaternion.Euler(0, 0, rotationZ);
	}
	*/

	void FixedUpdate () {
		//create a new missile whenever possible.
		if(canCreateMissile && !GameController.isGameFinished && GameController.isGameStarted)
			createMissile ();

		//rotate the laucher pad's body and also sync the direction with the player.
		rotationZ += (Time.deltaTime + PlayerController.dir) * -0.5f * 10;
		transform.rotation = Quaternion.Euler (0, 0, rotationZ );
	}

	void createMissile() {
		canCreateMissile = false;
		GameObject m = Instantiate (missile, transform.position, Quaternion.Euler (0, 180, 0)) as GameObject;
		m.name = "Missile";
		playSfx (shootSfx);
		StartCoroutine(activeMissileCreation());
	}

	IEnumerator activeMissileCreation() {

		//we use this to create more missiles when player advance to higher levels
		// missileCreationDelay = missileCreationBaseDelay - (1 / 30);

        if (GameController.level >= 1 && GameController.level < 2) {
			missileCreationDelay = 0.5f;
		} else if (GameController.level >= 2 && GameController.level < 4) {
			missileCreationDelay = 0.5f;
		} else if (GameController.level >= 4 && GameController.level < 6) {
			missileCreationDelay = 0.45f;
		} else if (GameController.level >= 6 && GameController.level < 10) {
			missileCreationDelay = 0.4f;
		} else if(GameController.level >= 10) {
			missileCreationDelay = 0.35f;
		}

		if (GameController.level % 5 == 0) {
			// Bonus
			missileCreationDelay = 0.3f;
		}

		yield return new WaitForSeconds (missileCreationDelay);
		canCreateMissile = true;
	}

	void playSfx ( AudioClip _clip  ){
		GetComponent<AudioSource>().PlayOneShot(_clip);
	}

}
