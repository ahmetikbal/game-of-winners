﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using CodeStage.AntiCheat.Storage;
using CodeStage.AntiCheat.ObscuredTypes;

public class GameController : MonoBehaviour {

	/// <summary>
	/// Main game controller handles game states, updating scores, showing information on UI, creating new coins at the start of each round
	/// and managing gameover sequence. All the static variables used inside gamecontroller are available to use in other classes.
	/// </summary>

	public static ObscuredInt level;					//Current game level - Starts with 1
	public static ObscuredInt totalCoins = 30;			//Total coins available in each level.
	public static int remainingCoins;			//remaining coins player need to collect to finish the level
	public static ObscuredInt totalGatheredCoin;		//total number of collected coins before gameover
	public static bool isGameStarted;			//flag
	public static bool isGameFinished;			//flag
	public static bool isGameOver;              //flag
	// public static int isExtraLifeTaken;
	public static ObscuredInt extraLifeCount;
	public static int rewardTakenLevel;

	//reference to UI objects
	public GameObject uiLevelNumber;
	public GameObject uiScore;
	public GameObject uiTapToStart;
	public GameObject uiRestartBtn;
	public GameObject uiFinishPlane;
	public GameObject uiYourScore;
	public GameObject uiBestScore;
	public GameObject uiNewBestScore;
	public GameObject uiExtraLife;
	public GameObject uiBonus;
	public GameObject coin;                     //coin prefab
	public GameObject bonusCoin;
	public TextMeshProUGUI lifeText;

	[Header("Reward Popup")]
	public GameObject rewardPopup;
	public GameObject[] rewards;
	public GameObject showRewardPopup;
	public TextMeshProUGUI rewardText;
	
	//private variables
	private Vector3 position;
	private float positionPrecision;
	private bool gameoverRunFlag;
	private bool isAdvancing;
	private int bestSavedScore;

	public bool IsSoundOn = true;
	public GameObject soundButton;
	public Sprite[] soundSprites;

	private ObscuredInt rewardId;
	/*
	 * RewardTakenLevel is for not showing reward popup again and again.
	 * IsGameFailed is a bool for failed game
	 * 
	 * */

	void Awake () {
		bestSavedScore = ObscuredPrefs.GetInt("BestSavedScore3", 0);
		level = ObscuredPrefs.GetInt("GameLevel", 1);
		// isExtraLifeTaken = PlayerPrefs.GetInt("ExtraLifeTaken", 0);
		extraLifeCount = ObscuredPrefs.GetInt("ExtraLife", 2);
		totalGatheredCoin = ObscuredPrefs.GetInt ("TotalGatheredCoin", 0);
		rewardTakenLevel = ObscuredPrefs.GetInt("RewardTakenLevel", 0);
		ObscuredPrefs.SetInt("FailedGameLevel", ObscuredPrefs.GetInt("FailedGameLevel", 0));

		if (PlayerPrefs.GetInt("SpaceOrbit_IsSoundOn", 1) == 0) 
			IsSoundOn = true;
		else 
			IsSoundOn = false;

		toggleSound();

		uiRestartBtn.SetActive (false);
		uiFinishPlane.SetActive (false);
		positionPrecision = totalCoins / 19.0f;
		remainingCoins = totalCoins;
		isGameStarted = false;
		isGameFinished = false;
		isGameOver = false;
		isAdvancing = false;
		gameoverRunFlag = false;

		lifeText.text = extraLifeCount.ToString();

		if (level % 4 == 0 && rewardTakenLevel != level) {
			// Reward Popup
			ObscuredPrefs.SetInt("RewardTakenLevel", level);
			rewardTakenLevel = level;
			rewardPopup.SetActive(true);
			PlayerController.Instance.rewardPopupIsOn = true;
		}


		if (ObscuredPrefs.GetInt("IsGameFailed", 0) == 1) {
			ObscuredPrefs.SetInt("IsGameFailed", 0);

			if (level % 5 == 0) {
				// Bonus
				uiBonus.SetActive(true);
				remainingCoins = ObscuredPrefs.GetInt("RemainingCoins", 28);
				StartCoroutine(createCoinsInOrbit(bonusCoin, remainingCoins));
			} else {
				remainingCoins = ObscuredPrefs.GetInt("RemainingCoins", 28);
				StartCoroutine(createCoinsInOrbit(coin, remainingCoins));
			}
		} else {
			if (level % 5 == 0) {
				// Bonus
				uiBonus.SetActive(true);
				StartCoroutine(createCoinsInOrbit(bonusCoin, remainingCoins));
			}
			else {
				StartCoroutine(createCoinsInOrbit(coin, remainingCoins));
			}
		}
	}

	void Start () {
		//show current level on UI
		uiLevelNumber.GetComponent<TextMeshProUGUI>().text = level.ToString ();

	//	Application.targetFrameRate = 60;
	}

	void Update () {
		//check for level finish state
		if (remainingCoins <= 0) {
			advanceLevel ();
		}

		lifeText.text = ObscuredPrefs.GetInt("ExtraLife", 2).ToString();

		//hide "tapToStart" when game is started
		if (isGameStarted && uiTapToStart && !isGameOver) {
			uiTapToStart.SetActive(false);
			uiBonus.SetActive(false);
		}

		//Monitor score and update on UI
		uiScore.GetComponent<TextMeshProUGUI>().text = totalGatheredCoin.ToString ();

		//check for game finish event
		if (isGameOver) {
			StartCoroutine (runGameover());
		}

        if (Input.GetKeyDown(KeyCode.E)) {
			// rewardPopup.SetActive(true);
			ObscuredPrefs.SetInt("ExtraLife", ObscuredPrefs.GetInt("ExtraLife", 2) + 1);
		}
	}

	public void extraLife() {
		gameoverRunFlag = false;
		ObscuredPrefs.SetInt("GameLevel", level);
		ObscuredPrefs.SetInt("TotalGatheredCoin", totalGatheredCoin);
		ObscuredPrefs.SetInt("ExtraLifeTaken", ObscuredPrefs.GetInt("ExtraLifeTaken", 1) + 1);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void toggleSound() {
        if (!IsSoundOn) {
			soundButton.GetComponent<Image>().sprite = soundSprites[0];
			AudioListener.volume = 1f;
			PlayerPrefs.SetInt("SpaceOrbit_IsSoundOn", 1);
			IsSoundOn = true;

		}
        else {
			soundButton.GetComponent<Image>().sprite = soundSprites[1];
			AudioListener.volume = 0f;
			PlayerPrefs.SetInt("SpaceOrbit_IsSoundOn", 0);
			IsSoundOn = false;
		}
    }

	IEnumerator runGameover() {
		if (gameoverRunFlag)
			yield break;
		gameoverRunFlag = true;

        if (ObscuredPrefs.GetInt("ExtraLife", 2) > 0) {
			gameoverRunFlag = false;
			ObscuredPrefs.SetInt("ExtraLife", ObscuredPrefs.GetInt("ExtraLife", 2) - 1);
			// print("Remaining Coins: " + remainingCoins);
			ObscuredPrefs.SetInt("RemainingCoins", remainingCoins);
			ObscuredPrefs.SetInt("GameLevel", level);
			ObscuredPrefs.SetInt("TotalGatheredCoin", totalGatheredCoin);
			ObscuredPrefs.SetInt("IsGameFailed", 1);
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			yield break;
		}

		//show current score on UI
		uiYourScore.GetComponent<TextMeshProUGUI>().text = totalGatheredCoin.ToString ();
		if (bestSavedScore < totalGatheredCoin) {
			//save new score as best score
			bestSavedScore = totalGatheredCoin;
			ObscuredPrefs.SetInt("BestSavedScore3", bestSavedScore);
			//FireStoreManager.Instance.CreateSpaceOrbitScore(level);
			FireStoreManager.Instance.CreateCurrentGameScore(level);
			uiNewBestScore.SetActive(true);
		}
		//show best score on UI
		uiBestScore.GetComponent<TextMeshProUGUI>().text = bestSavedScore.ToString ();

		/*
		//show a full screen ad every now and then (default: once in each 4 gameover)
		if (Random.value > 0.75f) {
			ReklamScript.InsterstitialGoster();
		}
		*/

		yield return new WaitForSeconds (0.75f);
		uiRestartBtn.SetActive (true);
		uiFinishPlane.SetActive (true);

		ObscuredPrefs.SetInt("TotalGatheredCoin", 0);
		ObscuredPrefs.SetInt("GameLevel", 1);
		ObscuredPrefs.SetInt("ExtraLife", 2);
		ObscuredPrefs.SetInt("RewardTakenLevel", 0);
		// Extra Life Popup
		/*
		if (isExtraLifeTaken == 0)
			uiExtraLife.SetActive(true);
		*/
	}

	public void rewardPopUpClose()
    {
		PlayerController.Instance.rewardPopupIsOn = false;
	}

	public void reklamGoster(float chanceToWatch) {
		if (Random.value > chanceToWatch) {
			ReklamScript.InsterstitialGoster();
		}
	}

	void advanceLevel() {

		if (isAdvancing)
			return;
		isAdvancing = true;

		isGameFinished = true;
		level++;
		ObscuredPrefs.SetInt("GameLevel", level);
		ObscuredPrefs.SetInt("TotalGatheredCoin", totalGatheredCoin);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);

	}

	IEnumerator createCoinsInOrbit(GameObject _coin, int size) {
		for (int i = 1; i <= size; i++) {
			position = new Vector3 (Mathf.Sin( (float)i / (Mathf.PI * positionPrecision) ) * PlayerController.orbitRadius, Mathf.Cos( (float)i / (Mathf.PI * positionPrecision) ) * PlayerController.orbitRadius, 0.1f);
			GameObject c = Instantiate(_coin, position, Quaternion.Euler(0, 180, 0)) as GameObject;
			c.name = "Coin-" + i.ToString();
			yield return new WaitForSeconds (0.01f);
		}
	}

	public void randomReward() {
		int rndm = Random.Range(0, 102);
		rewardId = rndm;

        foreach (var item in rewards) {
			item.GetComponent<Button>().interactable = false;
        }

		// ReklamScript.RewardedReklamGoster(reward);
		StartCoroutine(delayedReward(rndm));
	}

	IEnumerator delayedReward(int rndm) {
		yield return new WaitForSeconds(1f);
		ReklamScript.RewardedReklamGoster(reward);
		// reward(rndm);
	}

	public void reward(GoogleMobileAds.Api.Reward odul) {
        if (rewardId >= 0 && rewardId <= 30) {
			// +25 Point - 30%
			totalGatheredCoin += 25;
			StartCoroutine(delayedText("+25 PUAN KAZANDINIZ"));
        } else if (rewardId >= 31 && rewardId <= 50) {
			// +50 Point - 20%
			totalGatheredCoin += 50;
			StartCoroutine(delayedText("+50 PUAN KAZANDINIZ"));
		} else if (rewardId >= 51 && rewardId <= 65) {
			// +75 Point - 15%
			totalGatheredCoin += 75;
			StartCoroutine(delayedText("+75 PUAN KAZANDINIZ"));
		} else if (rewardId >= 66 && rewardId <= 75) {
			// +100 Point - 10%
			totalGatheredCoin += 100;
			StartCoroutine(delayedText("+100 PUAN KAZANDINIZ"));
		} else if (rewardId >= 76 && rewardId <= 90) {
			// +1 Life - 15%
			ObscuredPrefs.SetInt("ExtraLife", ObscuredPrefs.GetInt("ExtraLife", 2) + 1);
			StartCoroutine(delayedText("+1 CAN KAZANDINIZ"));
		} else if (rewardId >= 91 && rewardId <= 100) {
			// +2 Life - 10%
			ObscuredPrefs.SetInt("ExtraLife", ObscuredPrefs.GetInt("ExtraLife", 2) + 2);
			StartCoroutine(delayedText("+2 CAN KAZANDINIZ"));
		} else if (rewardId == 101) {
			// +500 Point - %1
			totalGatheredCoin += 250;
			Debug.Log("zink");
			StartCoroutine(delayedText("+250 PUAN KAZANDINIZ, TEBRIKLER!"));
		}
	}

	IEnumerator delayedText(string newText)
	{
		yield return new WaitForSeconds(0.5f);
		rewardPopup.SetActive(false);
		showRewardPopup.SetActive(true);
		rewardText.text = newText;
	}

}
