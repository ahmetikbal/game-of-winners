﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.Storage;

public class UiController_SpaceOrbit : MonoBehaviour
{
    public static UiController_SpaceOrbit Instance;

    public TextMeshProUGUI nicknameText;
    public TextMeshProUGUI highscoreText;

    public GameObject zatenGirisYaptinPanel;
    public Text newGameTimerText;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    private static bool oneAd;
    void Start()
    {
        ReklamScript.BannerGoster();

        nicknameText.text = PlayerPrefs.GetString("username", "PLAYER");
        highscoreText.text = ObscuredPrefs.GetInt("BestSavedScore3", 0).ToString();

        if (!oneAd)
        {
            ReklamScript.InsterstitialGoster();
            oneAd = true;
        }

        //TimeManager.Instance.RemainTime();
    }
    private void LateUpdate()
    {
        newGameTimerText.text = TimeManager.Instance.GetRemainingTimeText();
    }

    public void reklamGoster(float chanceToWatch) {
        if (Random.value > chanceToWatch) {
            ReklamScript.InsterstitialGoster();
        }
    }

    public void RegisterButton()
    {
        if (!PlayerPrefs.HasKey("LoggedIn"))
        {
            OpenRegisterScene();
        }
        else
        {
            zatenGirisYaptinPanel.SetActive(true);
        }
    }

    public void OpenRegisterScene()
    {
        SceneManager.LoadScene("Login_SpaceOrbit");
    }

    public void OpenInstagram() {
        Application.OpenURL("https://www.instagram.com/gameofwinners_/");
    }

    public void Guncelle()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.MedusaGames.GameofWinners");
    }

    public void LoadGameForKaydolmadinExitButton()
    {
        SceneManager.LoadScene("Spaceorbit_Game");
    }
    public void OpenRanking()
    {
     //   rankingMenu.SetActive(true);
     //   mainMenu.SetActive(false);

        //FireStoreManager.Instance.GetCurrentGameRanking();
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu_Main");
    }
}
