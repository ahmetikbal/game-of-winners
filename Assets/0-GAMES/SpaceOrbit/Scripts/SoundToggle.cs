﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundToggle : MonoBehaviour
{
    public bool IsSoundOn = true;
    public GameObject soundButton;
    public Sprite[] soundSprites;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("SpaceOrbit_IsSoundOn", 1) == 0)
            IsSoundOn = true;
        else
            IsSoundOn = false;

        toggleSound();
    }

    public void toggleSound() {
        if (!IsSoundOn) {
            soundButton.GetComponent<Image>().sprite = soundSprites[0];
            AudioListener.volume = 1f;
            PlayerPrefs.SetInt("SpaceOrbit_IsSoundOn", 1);
            IsSoundOn = true;

        }
        else {
            soundButton.GetComponent<Image>().sprite = soundSprites[1];
            AudioListener.volume = 0f;
            PlayerPrefs.SetInt("SpaceOrbit_IsSoundOn", 0);
            IsSoundOn = false;
        }
    }
}
