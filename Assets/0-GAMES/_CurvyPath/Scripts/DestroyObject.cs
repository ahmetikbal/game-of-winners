﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CurvyPath
{
    public class DestroyObject : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        private void OnBecameInvisible()
        {
            if (GameManager_CurvyPath.Instance != null && GameManager_CurvyPath.Instance.playerController != null)
            {
                if (GameManager_CurvyPath.Instance.playerController.isPlay && gameObject.transform.position.z < GameManager_CurvyPath.Instance.playerController.transform.position.z)
                    Destroy(gameObject);
            }
        }


        // Update is called once per frame
        void Update()
        {

        }
    }
}
