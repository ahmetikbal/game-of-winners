﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using MoreMountains.NiceVibrations;

namespace CurvyPath
{
    public enum GameState
    {
        Prepare,
        Playing,
        Paused,
        PreGameOver,
        GameOver
    }

    public class GameManager_CurvyPath : MonoBehaviour
    {
        public static GameManager_CurvyPath Instance { get; private set; }

        public static event System.Action<GameState, GameState> GameStateChanged;

        private static bool isRestart;

        public GameState GameState
        {
            get
            {
                return _gameState;
            }
            private set
            {
                if (value != _gameState)
                {
                    GameState oldState = _gameState;
                    _gameState = value;

                    if (GameStateChanged != null)
                        GameStateChanged(_gameState, oldState);
                }
            }
        }

        public static int GameCount
        {
            get { return _gameCount; }
            private set { _gameCount = value; }
        }

        private static int _gameCount = 0;

        public float elapsedTime;

        [Header("Set the target frame rate for this game")]
        [Tooltip("Use 60 for games requiring smooth quick motion, set -1 to use platform default frame rate")]
        public int targetFrameRate = -1;

        [Header("Current game state")]
        [SerializeField]
        private GameState _gameState = GameState.Prepare;

        // List of public variable for gameplay tweaking
        [Header("Gameplay Config")]

        [Range(0f, 1f)]
        public float coinFrequency = 0.1f;

        public float width = 50;

        public float spacing = 12;

        public float speed = 200;

        public float lenght = 25;

        public float timeCreateBall = 0.5f;

        public int limitPlane = 60;

        public float swipeForce = 2000;

        public float increaseSpeedRatio = 0.05f;

        public float limitSpeed = 300;

        public Color planeFirstColor = Color.white;

        public Color planeSecondColor = Color.gray;

        public Color color1 = Color.yellow;

        public Color color2 = Color.green;

        public Color color3 = Color.red;

        // List of public variables referencing other objects
        [Header("Object References")]
        public PlayerController playerController;

        [SerializeField]
        private Material materialObjectColor1;

        [SerializeField]
        private Material materialObjectColor2;

        [SerializeField]
        private Material materialObjectColor3;


        [SerializeField]
        private Material materialChangeColor1;

        [SerializeField]
        private Material materialChangeColor2;

        [SerializeField]
        private Material materialChangeColor3;

        void OnEnable()
        {
            PlayerController.PlayerDied += PlayerController_PlayerDied;
        }

        void OnDisable()
        {
            PlayerController.PlayerDied -= PlayerController_PlayerDied;
        }

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                DestroyImmediate(Instance.gameObject);
                Instance = this;
            }
            materialObjectColor1.SetColor("_Color", color1);
            materialObjectColor2.SetColor("_Color", color2);
            materialObjectColor3.SetColor("_Color", color3);

            materialChangeColor1.SetColor("_Color", color1);
            materialChangeColor2.SetColor("_Color", color2);
            materialChangeColor3.SetColor("_Color", color3);
        }

        void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        // Use this for initialization
        void Start()
        {
            // Initial setup
            Application.targetFrameRate = targetFrameRate;
            ScoreManager.Instance.Reset();

            PrepareGame();
        }

        private void Update()
        {
            if (_gameState == GameState.Playing)
                elapsedTime += Time.deltaTime;

            if (timeCreateBall > 0.6f)
                timeCreateBall = 0.75f - (0.01f * (int)(elapsedTime / 20));
        }


        // Listens to the event when player dies and call GameOver
        void PlayerController_PlayerDied()
        {
            GameOver();
        }

        // Make initial setup and preparations before the game can be played
        public void PrepareGame()
        {
            GameState = GameState.Prepare;

            // Automatically start the game if this is a restart.
            if (isRestart)
            {
                isRestart = false;
                StartGame();
            }
        }

        // A new game official starts
        public void StartGame()
        {
            GameState = GameState.Playing;
            if (SoundManager.Instance.background != null)
            {
                SoundManager.Instance.PlayMusic(SoundManager.Instance.background);
            }
        }

        // Called when the player died
        public void GameOver()
        {
            if (SoundManager.Instance.background != null)
            {
                SoundManager.Instance.StopMusic();
            }

            SoundManager.Instance.PlaySound(SoundManager.Instance.gameOver);
            GameState = GameState.GameOver;
            GameCount++;

            // Add other game over actions here if necessary
            if (ScoreManager.Instance.HasNewHighScore)
            {
                FireStoreManager.Instance.CreateCurrentGameScore(elapsedTime);
            }

            ReklamScript.InsterstitialGoster();

            MMVibrationManager.Haptic(HapticTypes.Failure);
        }

        // Start a new game
        public void RestartGame(float delay = 0)
        {
            isRestart = true;
            StartCoroutine(CRRestartGame(delay));
        }

        IEnumerator CRRestartGame(float delay = 0)
        {
            yield return new WaitForSeconds(delay);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void HidePlayer()
        {
            if (playerController != null)
                playerController.gameObject.SetActive(false);
        }

        public void ShowPlayer()
        {
            if (playerController != null)
                playerController.gameObject.SetActive(true);
        }

        public GameObject rewardedAdPopUpPanel;
        public Text rewardText;
        [HideInInspector] public bool rewardedAdPopUpPanelIsOn = false;
        public void WatchAdButtonForReward()
        {
            ReklamScript.RewardedReklamGoster(RewardFunctionForAdCompleted);
         //   RewardFunctionForAdCompleted(null); //TEST
        }
        public void RewardFunctionForAdCompleted(GoogleMobileAds.Api.Reward odul)
        {
            rewardedAdPopUpPanel.SetActive(false);
            rewardedAdPopUpPanelIsOn = false;

            int rewardId = Random.Range(0, 102);
            if (rewardId >= 0 && rewardId <= 30)
            {
                // +35 Point - 30%
                ScoreManager.Instance.AddScore(35);
                StartCoroutine(delayedText("+35 PUAN KAZANDINIZ"));
            }
            else if (rewardId >= 31 && rewardId <= 50)
            {
                // +40 Point - 20%
                ScoreManager.Instance.AddScore(40);
                StartCoroutine(delayedText("+40 PUAN KAZANDINIZ"));
            }
            else if (rewardId >= 51 && rewardId <= 65)
            {
                // +50 Point - 15%
                ScoreManager.Instance.AddScore(50);
                StartCoroutine(delayedText("+50 PUAN KAZANDINIZ"));
            }
            else if (rewardId >= 66 && rewardId <= 75)
            {
                // +60 Point - 10%
                ScoreManager.Instance.AddScore(60);
                StartCoroutine(delayedText("+60 PUAN KAZANDINIZ"));
            }
            else if (rewardId >= 76 && rewardId <= 90)
            {
                // +70 Point - 15%
                ScoreManager.Instance.AddScore(70);
                StartCoroutine(delayedText("+70 PUAN KAZANDINIZ"));
            }
            else if (rewardId >= 91 && rewardId <= 100)
            {
                // +80 Point - 10%
                ScoreManager.Instance.AddScore(80);
                StartCoroutine(delayedText("+80 PUAN KAZANDINIZ"));
            }
            else if (rewardId == 101)
            {
                // +250 Point - %1
                ScoreManager.Instance.AddScore(250);
                Debug.Log("zink");
                StartCoroutine(delayedText("+250 PUAN KAZANDINIZ"));
            }
        }
        IEnumerator delayedText(string newText)
        {
            rewardText.text = newText;
            yield return new WaitForSeconds(2.0f);
            rewardText.text = "";
        }
        public void ExitButtonOnPopUpPanel()
        {
            rewardedAdPopUpPanel.SetActive(false);
            rewardedAdPopUpPanelIsOn = false;
        }

    }
}