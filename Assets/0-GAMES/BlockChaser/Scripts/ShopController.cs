﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using com.adjust.sdk;

public class ShopController : MonoBehaviour
{
    // public GameObject[] characterButtons;

    /*
     * Equipped skin syntax : {gameName}_Skin_{category} == 1 / 0
     * Watched skin syntax : {gameName}_IsAdWatched_{category}_{"Index of the item"} == 1 / 0
     */


    [Header("Main")]
    public string gameName = "ChangeMe";
    public string category = "ChangeMe";

    [Header("Unity Stuffs")]
    public GameObject content;
    public List <GameObject> characterButtons;
    public GameObject popupArea;

    private int characterIndex;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < content.transform.childCount; i++) {
            characterButtons.Add(content.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < characterButtons.Count; i++) {
            if (PlayerPrefs.GetInt(gameName + "_IsAdWatched_" + category + "_" + i) == 1) {
                characterButtons[i].transform.GetChild(1).gameObject.SetActive(false);
            }
        }

        // Debug.Log("Child count: " + content.transform.childCount);

        // Debug.Log(gameName + "_Skin_" + category + ": " + PlayerPrefs.GetInt(gameName + "_Skin_" + category));
        PlayerPrefs.SetInt(gameName + "_IsAdWatched_" + category + "_" + 0, 1); // Default
        characterButtons[PlayerPrefs.GetInt(gameName + "_Skin_" + category, 0)].transform.GetChild(2).gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        // Reset
        if (Input.GetKeyDown(KeyCode.R)) {
            for (int i = 0; i < characterButtons.Count; i++) {
                PlayerPrefs.SetInt(gameName + "_IsAdWatched_" + category + "_" + i, 0);
            }
        }

        // Debug
        if (Input.GetKeyDown(KeyCode.D)) {
            Debug.Log("Current Item: " + PlayerPrefs.GetInt(gameName + "_Skin_" + category));
            for (int i = 0; i < characterButtons.Count; i++) {
                Debug.Log("AD Watch Check: " + PlayerPrefs.GetInt(gameName + "_IsAdWatched_" + category + "_" + i));
            }
        }
    }

    public void SelectCharacter() {
        characterIndex = EventSystem.current.currentSelectedGameObject.transform.GetSiblingIndex();

        if (PlayerPrefs.GetInt(gameName + "_IsAdWatched_" + category + "_" + characterIndex) == 1) {
            PlayerPrefs.SetInt(gameName + "_Skin_" + category, characterIndex);

            // Border for selected item
            foreach (var item in characterButtons)
                item.transform.GetChild(2).gameObject.SetActive(false);

            characterButtons[characterIndex].transform.GetChild(2).gameObject.SetActive(true);

        }
        else {
            PopupMessage(gameName + "_IsAdWatched_" + category + "_" + characterIndex, characterIndex);
            //WatchAdGetReward(gameName + "_IsAdWatched_" + category + "_" + characterIndex, characterIndex);
        }
    }

    public void WatchAdGetReward(string prefs, int characterIndex) {

        // REKLAM

        ReklamScript.RewardedReklamGoster((GoogleMobileAds.Api.Reward reward)=> {
            
            PlayerPrefs.SetInt(gameName + "_IsAdWatched_" + category + "_" + characterIndex, 1);
            PlayerPrefs.SetInt(gameName + "_Skin_" + category, characterIndex);

            AdjustEvent adjustEvent = new AdjustEvent("ovef90");
            Adjust.trackEvent(adjustEvent);

            foreach (var item in characterButtons)
                item.transform.GetChild(2).gameObject.SetActive(false);

            characterButtons[PlayerPrefs.GetInt(gameName + "_Skin_" + category, 0)].transform.GetChild(2).gameObject.SetActive(true);
            characterButtons[characterIndex].transform.GetChild(1).gameObject.SetActive(false);
            popupArea.SetActive(false);

        });

    }


    public void PopupMessage(string prefs, int characterIndex) {
        popupArea.SetActive(true); 
        popupArea.GetComponentsInChildren<Button>()[1].onClick.AddListener(delegate () { WatchAdGetReward(prefs, characterIndex); });
        //popupArea.GetComponentsInChildren<Button>()[1].onClick.AddListener(() => WatchAdGetReward(prefs, characterIndex)); 
    }
}
