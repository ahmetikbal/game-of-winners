﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Obstacle Spawning")]
    public Transform[] spawnPoints;
    public GameObject[] bigBlockPrefabs;
    public GameObject[] miniBlockPrefabs;
    GameObject spawnBlock;

    [Header("Bonus Spawning")]
    public GameObject[] bonusPrefabs;
    public Transform[] bonusSpawnPoints;

    [Header("Extra Life Spawning")]
    public GameObject[] randomBonusPrefabs;

    [Header("Main")]
    public float delay = 2f;
    public float bonusDelay;
    public float speed = 3f;
    public Color blockColor = new Color32(42, 45, 51, 200);
    public int variation = 0;

    int spawnMiniBlockIndex, spawnBigBlockIndex;
    int tempVariation = 0;

    // Start is called before the first frame update
    void Start() {
        blockColor = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
        tempVariation = variation;

        StartCoroutine("SpawnBlocks");
        StartCoroutine("SpawnBonuses"); 
        StartCoroutine("SpawnRandomBonus");
    }
    public void stopSpawning() {
        StopCoroutine("SpawnBlocks");
        StopCoroutine("SpawnBonuses");
        StopCoroutine("SpawnRandomBonus");
    }

    public void startSpawning() {
        StartCoroutine("SpawnBlocks");
        StartCoroutine("SpawnBonuses");
        StartCoroutine("SpawnRandomBonus");
    }

    // With Random Colors
    IEnumerator SpawnBlocks() {
        while (true) {
            yield return new WaitForSeconds(delay);

            int spawnPointIndex = Random.Range(0, spawnPoints.Length);
            Transform spawnPoint = spawnPoints[spawnPointIndex];

            if (spawnPointIndex == 2) {
                // Mini Block Spawning
                spawnBlock = miniBlockPrefabs[0];
            }
            else {
                // Big Block Spawning
                spawnBlock = bigBlockPrefabs[0];
            }

            GameObject spawnedObstacle = Instantiate(spawnBlock, spawnPoint.position, spawnPoint.rotation);
            spawnedObstacle.GetComponent<Obstacle>().speed = speed;

            if (tempVariation != variation) {
                blockColor = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
                tempVariation = variation;
            }
            spawnedObstacle.GetComponentInChildren<SpriteRenderer>().color = blockColor;

            Destroy(spawnedObstacle, 10f);
        }
    }


    /* With Pictures
    IEnumerator SpawnBlocks() {
        while (true) {
            yield return new WaitForSeconds(delay);

            int spawnPointIndex = Random.Range(0, spawnPoints.Length);
            Transform spawnPoint = spawnPoints[spawnPointIndex];

            if (spawnPointIndex == 2) {
                // Mini Block Spawning
                if (variation == 0) {
                    spawnMiniBlockIndex = Random.Range(0, 3);
                } else if(variation == 1) {
                    spawnMiniBlockIndex = Random.Range(3, 4);
                } else if (variation == 2) {
                    spawnMiniBlockIndex = Random.Range(4, 5);
                }
                spawnBlock = miniBlockPrefabs[spawnMiniBlockIndex];
            } else {
                // Big Block Spawning
                if (variation == 0) {
                    spawnBigBlockIndex = Random.Range(0, 6);
                }
                else if (variation == 1) {
                    spawnBigBlockIndex = Random.Range(6, 7);
                }
                else if (variation == 2) {
                    spawnBigBlockIndex = Random.Range(7, 6);
                }
                spawnBlock = bigBlockPrefabs[spawnBigBlockIndex];
            }

            GameObject spawnedObstacle = Instantiate(spawnBlock, spawnPoint.position, spawnPoint.rotation);
            spawnedObstacle.GetComponent<Obstacle>().speed = speed;
            // spawnedObstacle.GetComponentInChildren<SpriteRenderer>().color = blockColor;

            Destroy(spawnedObstacle, 10f);
        }
    }
    */

    IEnumerator SpawnBonuses() {
        while (true) {
            bonusDelay = delay + (delay / 2);
            yield return new WaitForSeconds(bonusDelay);

            int spawnPointIndex = Random.Range(0, bonusSpawnPoints.Length);
            Transform spawnPoint = bonusSpawnPoints[spawnPointIndex];

            int spawnBlockIndex = Random.Range(0, bonusPrefabs.Length);
            GameObject spawnBonus = bonusPrefabs[spawnBlockIndex];

            GameObject spawnedBonus = Instantiate(spawnBonus, spawnPoint.position + new Vector3(Random.Range(-1.2f, 1.2f), 0, 0), spawnPoint.rotation);
            spawnedBonus.GetComponent<Bonus>().speed = speed;

            Destroy(spawnedBonus, 10f);
        }
    }

    IEnumerator SpawnRandomBonus() {
        while (true) {
            float delay = Random.Range(40f, 60f);
            yield return new WaitForSeconds(delay);

            int spawnPointIndex = Random.Range(0, bonusSpawnPoints.Length);
            Transform spawnPoint = bonusSpawnPoints[spawnPointIndex];

            int spawnItemIndex = Random.Range(0, randomBonusPrefabs.Length);
            GameObject spawnItem = randomBonusPrefabs[spawnItemIndex];

            GameObject spawnedRandomBonus = Instantiate(spawnItem, spawnPoint.position + new Vector3(Random.Range(-1.2f, 1.2f), 0, 0), spawnPoint.rotation);
            spawnedRandomBonus.GetComponent<Bonus>().speed = speed;

            Destroy(spawnedRandomBonus, 10f);
        }
    }
}
