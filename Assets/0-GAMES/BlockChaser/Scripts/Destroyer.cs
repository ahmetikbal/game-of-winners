﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    GameManager_BlockChaser GM;

    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager_BlockChaser>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Obstacle") {
            Destroy(collision.gameObject);
            GM.score += 1;
        }

        if (collision.tag == "Bonus1") {
            Destroy(collision.gameObject);
        }
    }
}
