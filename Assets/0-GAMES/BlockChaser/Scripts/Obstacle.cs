﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float speed = 3f;

    Transform[] transforms;
    // Start is called before the first frame update
    void Start() {
        gameObject.transform.GetChild(0).GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, Random.Range(0, 180));
        /*
        transforms = GetComponentsInChildren<Transform>();
        
        int randomRotation = Random.Range(-50, 50);
        foreach (Transform item in transforms) {
            if (item.tag == "Obstacle") {
                item.rotation = Quaternion.Euler(0, 0, randomRotation);
            }
        }
        */


    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.up * -speed * Time.deltaTime;
    }
}
