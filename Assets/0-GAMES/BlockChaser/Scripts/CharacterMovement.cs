﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [Header("Main")]
    public bool isRotatingLeft = false;
    public bool isRotatingRight = false;

    [Header("Speed")]
    public float speedRotate = 250f;



    // Update is called once per frame
    void Update() {
        // Holding down the GUI button
        if (isRotatingLeft)
            transform.Rotate(Vector3.forward * speedRotate * Time.deltaTime);
        if (isRotatingRight)
            transform.Rotate(Vector3.back * speedRotate * Time.deltaTime);

        // For PC testing
        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.forward * speedRotate * Time.deltaTime);
        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.back * speedRotate * Time.deltaTime);
    }

    public void leftButtonPointerDown() {
        isRotatingLeft = true;
    }

    public void leftButtonPointerUp() {
        isRotatingLeft = false;
    }

    public void rightButtonPointerDown() {
        isRotatingRight = true;
    }

    public void rightButtonPointerUp() {
        isRotatingRight = false;
    }

}
