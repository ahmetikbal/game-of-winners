﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.Storage;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine.UI;

public class GameManager_BlockChaser : MonoBehaviour
{
    public static GameManager_BlockChaser Instance;

    [Header("Main")]
    public ObscuredInt life = 1;
    public bool isPlaying = true;
    public ObscuredInt score = 0;
    public float elapsedTime = 0;
    public float spawnerDelay = 0;
    public float obstacleSpeed = 0;
    public int stage = 0;
    public int bonusTaken = 0;
    public int lifeTaken = 0;

    [Header("Unity Stuffs")]
    public GameoverScreen gameOverScreen;
    public TextMeshProUGUI scoreText;
    public Spawner spawner;
    public GameObject character;
    public GameObject[] heart;
    public TextMeshProUGUI elapsedTimeText;
    public TextMeshProUGUI gameoverScoreText;
    public TextMeshProUGUI highscoreText;
    public Camera mainCamera;
    public TextMeshProUGUI lifeText;
    public TextMeshProUGUI lastLifeText;
    public GameObject background;
    public GameObject randomBonusPopup;
    public TextMeshProUGUI rewardText;

    [Header("Market")]
    public Sprite[] characterSkins;
    public Sprite[] backgroundSkins;

    Coroutine currentCoroutine;
    Coroutine currentCoroutine2;

    //Color changedColor;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;

        if (PlayerPrefs.GetInt("PlayerExtraLife") > 0) {
            life += PlayerPrefs.GetInt("PlayerExtraLife");

            PlayerPrefs.SetInt("PlayerExtraLife", PlayerPrefs.GetInt("PlayerExtraLife") - 1);
        }

        highscoreText.text = ObscuredPrefs.GetInt("BlockChaser_Highscore2", 0).ToString();


        character.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponentInParent<SpriteRenderer>().sprite = characterSkins[PlayerPrefs.GetInt("BlockChaser_Skin_Character", 0)];
        character.transform.GetChild(1).gameObject.transform.GetChild(0).GetComponentInParent<SpriteRenderer>().sprite = characterSkins[PlayerPrefs.GetInt("BlockChaser_Skin_Character", 0)];
        background.GetComponent<Image>().sprite = backgroundSkins[PlayerPrefs.GetInt("BlockChaser_Skin_Background", 0)];


        // Maybe Switch-Case?
        if (PlayerPrefs.GetInt("BlockChaser_Skin_Background") == 0) {
            mainCamera.backgroundColor = Color.gray;
        }
        else if (PlayerPrefs.GetInt("BlockChaser_Skin_Background") == 1) {
            mainCamera.backgroundColor = new Color(0.9433962f, 0.9105336f, 0.6808473f, 1);
        }
        else if (PlayerPrefs.GetInt("BlockChaser_Skin_Background") == 2) {
            mainCamera.backgroundColor = new Color(0.8662413f, 0.990566f, 0.630785f, 1);
        }

        /*
        if (PlayerPrefs.GetInt("BlockChaser_Skin_Character") == 0) {
            changedColor = Color.white;
        }
        else if (PlayerPrefs.GetInt("BlockChaser_Skin_Character") == 1) {
            changedColor = Color.red;
        }
        else if (PlayerPrefs.GetInt("BlockChaser_Skin_Character") == 2) {
            changedColor = Color.green;
        }
        else if (PlayerPrefs.GetInt("BlockChaser_Skin_Character") == 3) {
            changedColor = Color.yellow;
        }
        else if (PlayerPrefs.GetInt("BlockChaser_Skin_Character") == 4) {
            changedColor = Color.blue;
        }
        */
        

        //character.GetComponentsInChildren<SpriteRenderer>()[0].color = changedColor;
        //character.GetComponentsInChildren<SpriteRenderer>()[1].color = changedColor;

        //character.GetComponentsInChildren<TrailRenderer>()[0].startColor = changedColor;
        //character.GetComponentsInChildren<TrailRenderer>()[1].startColor = changedColor;
    }

    public void startGame() {
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (life >= 2) {
            heart[1].SetActive(true);
        } else {
            heart[1].SetActive(false);
        }
        lifeText.text = life.ToString();

        if (isPlaying == false || life <= 0) {
            isPlaying = false;

            // Simple Cheat Protection and Highscore (CLOSED FOR NOW)
            if (elapsedTime < 60 && bonusTaken == 0) {
                if (ObscuredPrefs.GetInt("BlockChaser_Highscore2", 0) < score) {
                    ObscuredPrefs.SetInt("BlockChaser_Highscore2", score);
                    //FireStoreManager.Instance.CreateBlockChaserScore();
                    FireStoreManager.Instance.CreateCurrentGameScore(elapsedTime);
                }

            } else {
                if (ObscuredPrefs.GetInt("BlockChaser_Highscore2", 0) < score) {
                    ObscuredPrefs.SetInt("BlockChaser_Highscore2", score);
                    //FireStoreManager.Instance.CreateBlockChaserScore();
                    FireStoreManager.Instance.CreateCurrentGameScore(elapsedTime);
                }

            }

            ReklamScript.InsterstitialGoster();

            gameOverScreen.Toggle();
            gameoverScoreText.text = score.ToString();

            int minutes = Mathf.FloorToInt(elapsedTime / 60F);
            int seconds = Mathf.FloorToInt(elapsedTime - minutes * 60);
            string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

            elapsedTimeText.text = niceTime;
        } else {
            elapsedTime += Time.fixedDeltaTime;
        }

        highscoreText.text = ObscuredPrefs.GetInt("BlockChaser_Highscore2", 0).ToString();
        scoreText.text = score.ToString();


        // Difficultness and Stages
        if (elapsedTime >= 0 && stage < 1) {
            // Stage 1 - Lowered delay obstacles

            stage = 1;
            currentCoroutine = StartCoroutine(lowerSpawnerDelay(5, 0.2f));
            currentCoroutine2 = StartCoroutine(speedyBlocks(5, 0.2f));
            spawner.variation = 0;
            // spawner.blockColor = new Color32((byte)Random.Range(0, 100), (byte)Random.Range(0, 100), (byte)Random.Range(0, 100), 255);
        } else if (elapsedTime >= 30 && stage < 2) {
            // Stage 2 - Obstacles start to falling faster

            stage = 2;
            StopCoroutine(currentCoroutine);
            StopCoroutine(currentCoroutine2);

            currentCoroutine = StartCoroutine(lowerSpawnerDelay(5, 0.1f));
            currentCoroutine2 = StartCoroutine(speedyBlocks(5, 0.1f));
            spawner.variation = 1;
            //spawner.blockColor = Color.black;
        } else if (elapsedTime >= 60 && stage < 3) {
            // Stage 3 - Obstacles start to falling faster with lowered delay obstacles

            stage = 3;
            StopCoroutine(currentCoroutine);
            StopCoroutine(currentCoroutine2);

            currentCoroutine = StartCoroutine(lowerSpawnerDelay(5, 0.1f));
            currentCoroutine2 = StartCoroutine(speedyBlocks(5, 0.2f));
            spawner.variation = 2;
            //spawner.blockColor = Color.red;
        } else if (elapsedTime >= 100 && stage < 4) {
            // Stage 4 - Max default Speed

            stage = 4;
            StopCoroutine(currentCoroutine);
            StopCoroutine(currentCoroutine2);

            spawner.delay = 0.7f;
            spawner.speed = 5f;

            spawner.variation = 1;
        } else if (elapsedTime >= 150 && stage < 5) {
            // Random variation - Endless - Hardcore

            stage = 5;

            spawner.delay = 0.5f;
            spawner.speed = 6f;

            spawner.variation = 0;
            StartCoroutine(changeVariation(30f));
        }

        // Limit
        if (spawner.delay <= 0.7f) {
            StopCoroutine(currentCoroutine);
        }
        if (spawner.speed >= 5f) {
            StopCoroutine(currentCoroutine2);
        }
    }

    IEnumerator changeVariation(float waitTimeInput) {
        while (true) {
            yield return new WaitForSeconds(waitTimeInput);

            spawner.variation = spawner.variation + 1;
        }
    }

    IEnumerator speedyBlocks(float waitTimeInput, float speedInput) {
        while (true) {
            yield return new WaitForSeconds(waitTimeInput);

            spawner.speed += speedInput;
            obstacleSpeed = spawner.speed;
        }
    }

    IEnumerator lowerSpawnerDelay(float waitTimeInput, float speedInput) {
        while (true) {
            yield return new WaitForSeconds(waitTimeInput);

            spawner.delay -= speedInput;
            spawnerDelay = spawner.delay;
        }
    }

    public void respawn() {
        character.transform.rotation = Quaternion.Euler(0, 0, 90);

        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject obstacle in obstacles)
            Destroy(obstacle);

        GameObject[] bonuses = GameObject.FindGameObjectsWithTag("Bonus1");
        foreach (GameObject bonus in bonuses)
            Destroy(bonus);

        if (life == 1) {
            lastLifeText.color = new Color(lastLifeText.color.r, lastLifeText.color.g, lastLifeText.color.b, 1);
            StartCoroutine(TextFade(lastLifeText, 0, 2f));
        }

        spawner.stopSpawning();
        StartCoroutine("startSpawning");
    }

    IEnumerator startSpawning() {
        while (true) {
            yield return new WaitForSeconds(1.5f);

            spawner.startSpawning();
            StopCoroutine("startSpawning");
        }
    }

    public void openRandomBonusPopup() {
        Time.timeScale = 0f;
        randomBonusPopup.SetActive(true);
    }

    public void randomBonus() {
        int rndm = Random.Range(0, 5);
        if (rndm == 0) {
            if (lifeTaken == 0) 
                ReklamScript.RewardedReklamGoster(extraLifeReward);
            else
                ReklamScript.RewardedReklamGoster(extraPointReward4);
        } 
        else if (rndm == 1) {
            ReklamScript.RewardedReklamGoster(extraPointReward);
            //extraPointReward(null);
        }
        else if (rndm == 2) {
            ReklamScript.RewardedReklamGoster(extraPointReward2);
            //extraPointReward2(null);
        }
        else if (rndm == 3) {
            ReklamScript.RewardedReklamGoster(extraPointReward3);
            //pointMultiplierReward(null);
        }
        else if (rndm == 4) {
            ReklamScript.RewardedReklamGoster(extraPointReward4);
            //pointMultiplierReward2(null);
        }
    }

    public void extraLife() {
        ReklamScript.RewardedReklamGoster(extraLifeReward);
    }

    public void extraLifeReward(GoogleMobileAds.Api.Reward reward)
    {
        life += 1;
        lifeTaken += 1;
        rewardText.text = "+1 Can Kazandın!";
        respawn();
        rewardText.color = new Color(rewardText.color.r, rewardText.color.g, rewardText.color.b, 1);
        StartCoroutine(TextFade(rewardText, 0, 5f));

        randomBonusPopup.SetActive(false);
        startGame();
    }

    public void extraPointReward(GoogleMobileAds.Api.Reward reward) {
        score += 250;
        bonusTaken++;
        rewardText.text = "Puanına 250 Eklendi!";
        respawn();
        rewardText.color = new Color(rewardText.color.r, rewardText.color.g, rewardText.color.b, 1);
        StartCoroutine(TextFade(rewardText, 0, 5f));

        randomBonusPopup.SetActive(false);
        startGame();
    }

    public void extraPointReward2(GoogleMobileAds.Api.Reward reward) {
        score += 500;
        bonusTaken++;
        rewardText.text = "Puanına 500 Eklendi!";
        respawn();
        rewardText.color = new Color(rewardText.color.r, rewardText.color.g, rewardText.color.b, 1);
        StartCoroutine(TextFade(rewardText, 0, 5f));

        randomBonusPopup.SetActive(false);
        startGame();
    }

    public void extraPointReward3(GoogleMobileAds.Api.Reward reward) {
        score += 750;
        bonusTaken++;
        rewardText.text = "Puanına 750 Eklendi!";
        respawn();
        rewardText.color = new Color(rewardText.color.r, rewardText.color.g, rewardText.color.b, 1);
        StartCoroutine(TextFade(rewardText, 0, 5f));

        randomBonusPopup.SetActive(false);
        startGame();
    }

    public void extraPointReward4(GoogleMobileAds.Api.Reward reward) {
        score += 1000;
        bonusTaken++;
        rewardText.text = "Puanına 1.000 Eklendi!";
        respawn();
        rewardText.color = new Color(rewardText.color.r, rewardText.color.g, rewardText.color.b, 1);
        StartCoroutine(TextFade(rewardText, 0, 5f));

        randomBonusPopup.SetActive(false);
        startGame();
    }

    IEnumerator TextFade(TextMeshProUGUI sr, float endValue, float duration) {
        float elapsedTime = 0;
        float startValue = sr.color.a;
        while (elapsedTime < duration) {
            elapsedTime += Time.deltaTime;
            float newAlpha = Mathf.Lerp(startValue, endValue, elapsedTime / duration);
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, newAlpha);
            yield return null;
        }
    }

    public void doVibration() {
        StartCoroutine(Vibrate());
    }

    private IEnumerator Vibrate() {
        float interval = 0.05f;
        WaitForSeconds wait = new WaitForSeconds(interval);
        float t;

        for (t = 0; t < 0.7f; t += interval) // Change the end condition (t < 1) if you want
        {
            Handheld.Vibrate();
            yield return wait;
        }

        yield return new WaitForSeconds(0.4f);
    }
}
