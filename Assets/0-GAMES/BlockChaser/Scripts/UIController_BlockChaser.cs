﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.Storage;

public class UIController_BlockChaser : MonoBehaviour
{
    public static UIController_BlockChaser Instance;

    public TextMeshProUGUI highscoreText, nicknameText, newGameTimerText;

    /* //IPTAL EDILDI ONESIGNAL POP UPI ILE DEVAM EDILECEK
    public GameObject popUpMenu;
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("popup_blockchaser"))
        {
            popUpMenu.SetActive(true);
            PlayerPrefs.SetInt("popup_blockchaser", 1);
        }
    }
    */

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    private static bool oneAd;
    void Start()
    {
        ReklamScript.BannerGoster();
        if (!oneAd)
        {
            ReklamScript.InsterstitialGoster();
            oneAd = true;
        }

        highscoreText.text = ObscuredPrefs.GetInt("BlockChaser_Highscore2", 0).ToString();
        nicknameText.text = PlayerPrefs.GetString("username", "PLAYER");

        //TimeManager.Instance.RemainTime();
    }

    private void LateUpdate()
    {
        newGameTimerText.text = TimeManager.Instance.GetRemainingTimeText();
    }

    /*
    public GameObject mainMenuArea, yarismaArea;
    public void YarismaButton()
    {
        mainMenuArea.SetActive(false);
        yarismaArea.SetActive(true);
    }
    */

    public void Guncelle()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.MedusaGames.GameofWinners");
    }

    public GameObject zatenGirisYaptinPanel;
    public void KaydolButton()
    {
        if (!PlayerPrefs.HasKey("LoggedIn"))
        {
            ReklamScript.InsterstitialGoster();
            SceneManager.LoadScene("Login 3");
        }
        else
        {
            zatenGirisYaptinPanel.SetActive(true);
        }
    }

    public void KayitSceneAc()
    {
        SceneManager.LoadScene("Login 3");
    }

    public void OdullerButton()
    {
        ReklamScript.InsterstitialGoster();
    }

    public void SiralamaButton()
    {

        //if (FireStoreManager.Instance != null) FireStoreManager.Instance.GetCurrentGameRanking();
        reklamGoster(0.5f);
    }

    public void OpenInstagram()
    {
        Application.OpenURL("https://www.instagram.com/gameofwinners_/");
    }

    public void LoadGameForKaydolmadinExitButton()
    {
        SceneManager.LoadScene("Main");
    }
    public void reklamGoster(float chanceToWatch)
    {
        if (Random.value > chanceToWatch)
        {
            ReklamScript.InsterstitialGoster();
        }
    }
}
