﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameoverScreen : MonoBehaviour
{
    public GameObject ui;
    public GameObject pauseButton;

    // Update is called once per frame
    void Update() {

        /*
        if (Input.GetMouseButtonDown(0) && ui.activeSelf) {
            Toggle();
        }
        */
    }

    public void Toggle() {
        ui.SetActive(!ui.activeSelf);

        if (ui.activeSelf) {
            Time.timeScale = 0f;
            pauseButton.SetActive(false);
        }
        else {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
