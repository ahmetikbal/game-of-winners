﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Header("Main")]
    GameManager_BlockChaser GM;
    TextMeshProUGUI floatingBonusText;

    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager_BlockChaser>();
        floatingBonusText = GameObject.Find("FloatingBonusText").GetComponent<TextMeshProUGUI>();
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Obstacle") {
            GM.life -= 1;

            GM.respawn();
            GM.doVibration();
        }

        if (collision.tag == "Bonus1") {
            Destroy(collision.gameObject);
            GM.score += 5;

            floatingBonusText.transform.position = collision.gameObject.transform.position;
            floatingBonusText.color = new Color(floatingBonusText.color.r, floatingBonusText.color.g, floatingBonusText.color.b, 1);
            StartCoroutine(SpriteFade(floatingBonusText, 0, 1.5f));
        }

        if (collision.tag == "RandomBonus") {
            Destroy(collision.gameObject);
            GM.openRandomBonusPopup();
        }
    }

    IEnumerator SpriteFade(TextMeshProUGUI sr, float endValue, float duration) {
        float elapsedTime = 0;
        float startValue = sr.color.a;
        while (elapsedTime < duration) {
            elapsedTime += Time.deltaTime;
            float newAlpha = Mathf.Lerp(startValue, endValue, elapsedTime / duration);
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, newAlpha);
            yield return null;
        }
    }
}
