﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string sceneName = "MainMenu";
    public bool whenAwaking = false; 

    // Start is called before the first frame update
    private void Awake() {
        if (whenAwaking)
            SceneManager.LoadScene(sceneName);
    }

    public GameObject ForceUpdatePanel, KaydolmadinPanel;
    public void goToScene(string sceneName) {

        /*
        if (sceneName == "Main" || sceneName == "Spaceorbit_Game")
        {
            if (GameManager.Instance.newVersion > GameManager.Instance.currentVersion)
            {
                ForceUpdatePanel.SetActive(true);
            }
            else
            {
                if (!PlayerPrefs.HasKey("LoggedIn") && KaydolmadinPanel != null)
                {
                    KaydolmadinPanel.SetActive(true);
                }
                else
                {
                    SceneManager.LoadScene(sceneName);
                }
            }
        }
        else
        {
            SceneManager.LoadScene(sceneName);
        }
        */

        SceneManager.LoadScene(sceneName);

    }

    public void extraLife(string sceneName) {
        PlayerPrefs.SetInt("PlayerExtraLife", 1);
        SceneManager.LoadScene(sceneName);
    }
}
