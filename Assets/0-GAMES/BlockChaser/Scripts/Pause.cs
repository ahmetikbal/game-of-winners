﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject ui;
    public GameObject pauseButton;

    // Update is called once per frame
    void Update() {
        /*
        if (Input.GetMouseButtonDown(0) && ui.activeSelf) {
            Toggle();
        }
        */
    }

    public void Toggle() {
        ui.SetActive(!ui.activeSelf);

        if (ui.activeSelf) {
            Time.timeScale = 0f;
            pauseButton.SetActive(false);
        }
        else {
            Time.timeScale = 1f;
            pauseButton.SetActive(true);
        }
    }
}
