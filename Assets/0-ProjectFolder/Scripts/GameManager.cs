﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.Storage;
using CodeStage.AntiCheat.Detectors;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [HideInInspector] public bool cheatDetected = false;

    [Header("Force Update")]
    public float currentVersion;
    public float newVersion;

    public GoWSettings goWSettings;
    [HideInInspector] public CurrentGameSettings currentGameSettingsAsset;

    //[HideInInspector] public int currentGame; // 0 or 1 //ŞİMDİLİK KULLANILMIYOR ÇOĞU KISMI BOZUK OLMALI
    [HideInInspector] public string currentHighScorePlayerPrefsName; //ObscuredPrefs *********************
    [HideInInspector] public string currentGameSceneName;
    [HideInInspector] public string currentGameFirestoreCollectionNameForScoreboard;

    //   public string firstGameSceneName, secondGameSceneName;
    //   public string firstGameHighScorePlayerPrefs, secondGameHighScorePlayerPrefs;
    //Highscore , 

    [HideInInspector] public bool firebaseSetupCompleted;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        currentVersion = float.Parse(Application.version);

        //currentGame = PlayerPrefs.GetInt("currentGame", 0);

        /*
       if (currentGame == 0)
           currentHighScorePlayerPrefsName = firstGameHighScorePlayerPrefs;
       else if (currentGame == 1)
           currentHighScorePlayerPrefsName = secondGameHighScorePlayerPrefs;
        */


      //  SetGameSettings();

        Invoke("LoadMainMenu", 1.25f);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu_Main");
    }

    public void SetGameSettings()
    {
        currentGameSettingsAsset = goWSettings.currentGameSettingsAsset;

        currentHighScorePlayerPrefsName = currentGameSettingsAsset.highScorePlayerPrefsName;
        currentGameSceneName = currentGameSettingsAsset.sceneNameForThisGame;
        currentGameFirestoreCollectionNameForScoreboard = currentGameSettingsAsset.collectionNameForSaveScore;

        if(currentGameFirestoreCollectionNameForScoreboard != "") 
            FireStoreManager.Instance.SetOpenedGameCollection(currentGameFirestoreCollectionNameForScoreboard);

    }

    public void LoadCurrentGame()
    {
        /*
        if (PlayerPrefs.GetInt("currentGame") == 0)
            SceneManager.LoadScene("WaterShooter");
        else if (PlayerPrefs.GetInt("currentGame") == 1)
            SceneManager.LoadScene("WaterShooter");
            */

           SceneManager.LoadScene(currentGameSceneName);
    }

    //ANTI CHEAT
    private void Start()
    {
        ObscuredPrefs.OnAlterationDetected += OnCheatingDetected;
    }
    #region AntiCheat
    public void OnCheatingDetected()
    {
        cheatDetected = true;
    }
    private void OnGUI()
    {
        if (cheatDetected) GUILayout.Label("Hile tespit edildi! Yapmaya devam ederseniz banlanacaksınız.");
    }
    #endregion
}
