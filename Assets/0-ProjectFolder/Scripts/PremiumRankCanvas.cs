﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using CodeStage.AntiCheat.Storage;
using UnityEngine.UI;

public class PremiumRankCanvas : MonoBehaviour
{
    public static PremiumRankCanvas Instance;

    public TextMeshProUGUI[] rankingTexts;

    [Header("Main")]
    public GameObject content;
    public GameObject playerPrefab;
    public GameObject scrollRect;

    GameObject player;
    public List<GameObject> playerList;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void CreatePlayer(int rank, string nickname, string score) {
        player = Instantiate(playerPrefab, content.transform);
        playerList.Add(player);

        if (PlayerPrefs.GetString("username", "PLAYER") == nickname) 
            player.GetComponent<Image>().color = Color.yellow;

        player.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = rank.ToString();
        player.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = nickname;
        player.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = score;
    }

    public void VerticalScrollbar() {
        if (scrollRect.GetComponent<ScrollRect>().verticalNormalizedPosition <= 0.0f) {
            FireStoreManager.Instance.FetchNext();
        }
    }

    public void FetchNext() {
        FireStoreManager.Instance.FetchNext();
    }

    public void AddPlayerToDatabase() {
        FireStoreManager.Instance.Test();
    }

    private void OnEnable()
    {
        FireStoreManager.Instance.GetCurrentPremiumGameRanking();
    }
}
