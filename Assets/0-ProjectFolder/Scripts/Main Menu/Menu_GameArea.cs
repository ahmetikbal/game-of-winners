﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Firebase;

public class Menu_GameArea : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI gameNameText;
    [SerializeField] private Image gameImage;

    [Header("Countdown")]
    [SerializeField] private TextMeshProUGUI statusText;
    [SerializeField] private TextMeshProUGUI dayText;
    [SerializeField] private TextMeshProUGUI hourText;
    [SerializeField] private TextMeshProUGUI minuteText;
    [SerializeField] private GameObject countdownPanel;
    [SerializeField] private GameObject passiveGamePanel;

    [ReadOnly] public CurrentGameSettings currentGameSettings;
    
    public void SetGameArea()
    {
        gameObject.name = currentGameSettings.gameName;

        gameNameText.text = currentGameSettings.gameName;
        gameImage.sprite = currentGameSettings.gamePhoto;

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (GameManager.Instance.firebaseSetupCompleted) GetGameSettings();
            else Invoke("GetGameSettings", 2f);
        }
        else
        {
            Invoke("NetWorkError", .5f); //DİL SİSTEMİ NEDENİYLE DELAY OLMADAN YAZI DEĞİŞMİYORDU İKİSİ DE AWAKE İÇİNDE OLDUĞUNDAN
        }
    }

    public void GetGameSettings()
    {
        FireStoreManager.Instance.GetGameSettings(currentGameSettings, this);
    }

    public void SetGameTimes() //VERİTABANINDAN GELEN AÇILIŞ KAPANIŞ BİLGİLERİ İLE
    {
        var remainingTime = TimeManager.Instance.GetRemainingTimeForGame(currentGameSettings);

        if ((currentGameSettings._gameStatus != CurrentGameSettings.gameStatus.Passive))
        {
            countdownPanel.SetActive(true);
            passiveGamePanel.SetActive(false);

            if ((currentGameSettings._gameStatus == CurrentGameSettings.gameStatus.Active))
                statusText.text = LanguageManager.Instance.GetValue("toEnd");
            else if ((currentGameSettings._gameStatus == CurrentGameSettings.gameStatus.WaitToOpen))
                statusText.text = LanguageManager.Instance.GetValue("toStart");

            dayText.text = remainingTime.Days.ToString("00");
            hourText.text = remainingTime.Hours.ToString("00");
            minuteText.text = remainingTime.Minutes.ToString("00");
        }
        else
        {
            passiveGamePanel.SetActive(true);
            countdownPanel.SetActive(false);
        }
    }

    public void OnClick()
    {
        UIController_MainMenu.Instance.GameButtonOnClick(currentGameSettings);
    }

    public void NetWorkError()
    {
        statusText.text = LanguageManager.Instance.GetValue("networkError");
    }

}
