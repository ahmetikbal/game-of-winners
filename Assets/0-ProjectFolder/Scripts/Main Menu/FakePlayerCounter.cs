﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class FakePlayerCounter : MonoBehaviour
{
    public Text playerCounterText;

    [SerializeField] private float changingTime = 20f;

    private int hour;
    private static int playerCount;

    private int minPlayerCount, maxPlayerCount;

    private void Awake()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {

            hour = System.DateTime.Now.Hour;

            GetFakePlayerRange();

            if (playerCount == 0) playerCount = Random.Range(minPlayerCount, maxPlayerCount + 1);
            playerCounterText.DOText(playerCount.ToString(), 2f, true, ScrambleMode.Numerals);

            InvokeRepeating("RandomChanging", 3.5f, changingTime);
        }
    }

    /*
    private void LateUpdate()
    {
        playerCount = Mathf.Clamp(playerCount, minPlayerCount, maxPlayerCount);
    }
    */

    public void GetFakePlayerRange()
    {
        if ((0 <= hour) && (hour <= 2))
        {
            minPlayerCount = 3500;
            maxPlayerCount = 7000;
        }
        else if ((2 < hour) && (hour <= 4))
        {
            minPlayerCount = 1500;
            maxPlayerCount = 3500;
        }
        else if ((4 < hour) && (hour <= 6))
        {
            minPlayerCount = 200;
            maxPlayerCount = 500;
        }
        else if ((6 < hour) && (hour <= 8))
        {
            minPlayerCount = 200;
            maxPlayerCount = 500;
        }
        else if ((8 < hour) && (hour <= 10))
        {
            minPlayerCount = 400;
            maxPlayerCount = 850;
        }
        else if ((10 < hour) && (hour <= 12))
        {
            minPlayerCount = 1000;
            maxPlayerCount = 3500;
        }
        else if ((12 < hour) && (hour <= 14))
        {
            minPlayerCount = 1500;
            maxPlayerCount = 4000;
        }
        else if ((14 < hour) && (hour <= 16))
        {
            minPlayerCount = 2000;
            maxPlayerCount = 5500;
        }
        else if ((16 < hour) && (hour <= 18))
        {
            minPlayerCount = 8000;
            maxPlayerCount = 10000;
        }
        else if ((18 < hour) && (hour <= 20))
        {
            minPlayerCount = 14000;
            maxPlayerCount = 18000;
        }
        else if ((20 < hour) && (hour <= 22))
        {
            minPlayerCount = 15000;
            maxPlayerCount = 25000;
        }
        else if (22 < hour)
        {
            minPlayerCount = 13000;
            maxPlayerCount = 21000;
        }
    }

    public void RandomChanging()
    {
        var minDeflection = Mathf.RoundToInt((maxPlayerCount - minPlayerCount) / 100);
        var maxDeflection = Mathf.RoundToInt((maxPlayerCount - minPlayerCount) / 10);
        var deflection = Random.Range(minDeflection, maxDeflection + 1);

        var addOrSub = Random.Range(0, 3); // 0 and 1 : Addition; 2: Subtraction

        if (addOrSub == 2)
            playerCount -= deflection;
        else
            playerCount += deflection;

        //playerCount = Mathf.Clamp(playerCount, minPlayerCount, maxPlayerCount);

        playerCounterText.DOText(playerCount.ToString(), 2f, true, ScrambleMode.Numerals);
    }
}
