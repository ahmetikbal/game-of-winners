﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;

public class UIController_MainMenu : MonoBehaviour
{
    public static UIController_MainMenu Instance;

    private GoWSettings goWSettings;

    [Header("Game Area Spawner")]
    [SerializeField] private GameObject gameAreaPrefab;
    [SerializeField] private GameObject premiumGameAreaPrefab;
    [SerializeField] private Transform gameAreasParentTransform;
    [SerializeField] private Transform premiumGameAreasParentTransform;

    [Header("Header")]
    public TextMeshProUGUI currentMenuText;

    [Header("Active Games Area")]
    public GameObject activeGamesArea;
    public GameObject activeGamesButtonArrow;

    [Header("Future Games Area")]
    public GameObject futureGamesArea;
    public GameObject futureGamesButtonArrow;

    [Header("Premium Games Area")]
    public GameObject premiumGamesArea;
    public GameObject premiumGamesButtonArrow;

    [Header("Places to Change With Premium")]
    public Transform topButtonsParent;
    public Sprite topButtonsNormalTexture;
    public Sprite topButtonsPremiumTexture;

    public Image background;
    public Sprite normalBackground;
    public Sprite premiumBackground;

    public GameObject backgroundEffect;
    //DEVAMI EKLENECEK (PROFİL, BG VS.)

    [Header("Register and Profile Area")]
    public GameObject registerandProfileArea;
    public GameObject registerandProfileButtonArrow;
    public GameObject registerArea;
    public GameObject loginArea;
    public GameObject profileArea;
    public TextMeshProUGUI nicknameText;
    public TextMeshProUGUI emailText;

    [Header("Shop Area")]
    public GameObject shopArea;

    [Header("Pop Ups")]
    public GameObject alreadyRegisterPanel;
    public GameObject dontRegisterPanel;
    public GameObject forceUpdatePanel;
    public GameObject notPremiumUserPanel;

    [Header("Others")]
    public GameObject loadingPanel;

    private void Awake()
    {
        if (Instance == null) Instance = this;

        goWSettings = GameManager.Instance.goWSettings;
        SpawnGameAreas();
        ActiveGamesButton();
    }

    private void Start()
    {
        ReklamScript.BannerGoster();

        if (PlayerPrefs.GetInt("ShowMarket", 0) == 1) {
            PlayerPrefs.SetInt("ShowMarket", 0);
            ShopButton();
        }
    }

    private void SpawnGameAreas()
    {
        var gameCount = goWSettings.GameList.Count;

        for (int i = 0; i < gameCount; i++)
        {
            Transform parent = gameAreasParentTransform;
            GameObject prefab = gameAreaPrefab;

            if (goWSettings.GameList[i]._gameType == CurrentGameSettings.gameType.Normal)
            {
                parent = gameAreasParentTransform;
                prefab = gameAreaPrefab;
            }
            else if (goWSettings.GameList[i]._gameType == CurrentGameSettings.gameType.Premium)
            {
                parent = premiumGameAreasParentTransform;
                prefab = premiumGameAreaPrefab;
            }

            var gamePanel = Instantiate(prefab, parent);

            gamePanel.GetComponent<Menu_GameArea>().currentGameSettings = goWSettings.GameList[i];
            gamePanel.GetComponent<Menu_GameArea>().SetGameArea();
        }
    }

    public void GameButtonOnClick(CurrentGameSettings game)
    {
        goWSettings.currentGameSettingsAsset = game;
        GameManager.Instance.SetGameSettings();

        if (GameManager.Instance.newVersion > GameManager.Instance.currentVersion)
        {
            forceUpdatePanel.SetActive(true);
        }
        else
        {
            if (game._gameType == CurrentGameSettings.gameType.Normal)
            {
                if (!PlayerPrefs.HasKey("LoggedIn"))
                {
                    dontRegisterPanel.SetActive(true);
                }
                else
                {
                    LoadSelectedGame();
                }
            }
            else //PREMIUM OYUNSA
            {
                LoadSelectedGame();

                /*
                if (SubscriptionSystem.Instance.subscriptionStatus != SubscriptionSystem.SubscriptionStatus.None)
                    LoadSelectedGame();
                else
                    notPremiumUserPanel.SetActive(true);
                */
            }
        }
    }

    public void LoadSelectedGame()
    {
        loadingPanel.GetComponent<CanvasGroup>().alpha = 1f;
        loadingPanel.SetActive(true);
        loadingPanel.GetComponent<CanvasGroup>().DOFade(0f, .6f).From();
        DOVirtual.DelayedCall(.6f, () => GameManager.Instance.LoadCurrentGame());
    }

    public void ResetMenu()
    {
        activeGamesArea.SetActive(false);
        activeGamesButtonArrow.SetActive(false);

        futureGamesArea.SetActive(false);
        futureGamesButtonArrow.SetActive(false);

        premiumGamesArea.SetActive(false);
        premiumGamesButtonArrow.SetActive(false);

        registerandProfileArea.SetActive(false);
        registerandProfileButtonArrow.SetActive(false);
        registerArea.SetActive(false);
        loginArea.SetActive(false);
        profileArea.SetActive(false);
        shopArea.SetActive(false);

        alreadyRegisterPanel.SetActive(false);
        dontRegisterPanel.SetActive(false);
        forceUpdatePanel.SetActive(false);
        notPremiumUserPanel.SetActive(false);

        loadingPanel.SetActive(false);

        backgroundEffect.SetActive(false);
        background.sprite = normalBackground;

        ChangeTopButtonTextures(topButtonsNormalTexture);
    }

    public void ActiveGamesButton()
    {
        ResetMenu();

        activeGamesArea.SetActive(true);
        activeGamesButtonArrow.SetActive(true);

        currentMenuText.text = LanguageManager.Instance.GetValue("activegames");
    }
    public void FutureGamesButton()
    {
        ResetMenu();
        futureGamesArea.SetActive(true);
        futureGamesButtonArrow.SetActive(true);

        currentMenuText.text = LanguageManager.Instance.GetValue("futuregames");
    }
    public void PremiumGamesButton()
    {
        ResetMenu();
        premiumGamesArea.SetActive(true);
        premiumGamesButtonArrow.SetActive(true);

        currentMenuText.text = LanguageManager.Instance.GetValue("premiumgames");

        ChangeTopButtonTextures(topButtonsPremiumTexture);

        backgroundEffect.SetActive(true);
        background.sprite = premiumBackground;
    }
    public void RegisterAndProfileButton()
    {
        ResetMenu();
        registerandProfileArea.SetActive(true);
        registerandProfileButtonArrow.SetActive(true);

        if (!PlayerPrefs.HasKey("LoggedIn"))
        {
            registerArea.SetActive(true);
            currentMenuText.text = LanguageManager.Instance.GetValue("register");
        }
        else
        {
            profileArea.SetActive(true);
            currentMenuText.text = LanguageManager.Instance.GetValue("profile");

            nicknameText.text = PlayerPrefs.GetString("username");
            emailText.text = PlayerPrefs.GetString("email");
        }
    }

    public void TurnBackToRegisterAreaButton() 
    {
        alreadyRegisterPanel.SetActive(true);
    }
    public void YesForTurnBackToRegisterArea()
    {
        alreadyRegisterPanel.SetActive(false);

        profileArea.SetActive(false);
        registerArea.SetActive(true);

        currentMenuText.text = LanguageManager.Instance.GetValue("register");
    }

    public void ShopButton()
    {
        ResetMenu();
        shopArea.SetActive(true);
    }

    public void DetailedFeaturesForPremiumPackagesButton(GameObject detailedFeaturesArea)
    {
        detailedFeaturesArea.SetActive(true);
    }

    public void ChangeTopButtonTextures(Sprite sprite) //NORMAL AND PREMIUM MODES
    {
        for (int i = 0; i < topButtonsParent.childCount; i++)
        {
            topButtonsParent.GetChild(i).GetComponent<Image>().sprite = sprite;
        }
    }

    public void UpdateTheGame()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.MedusaGames.GameofWinners");
    }
    public void OpenInstagram()
    {
        Application.OpenURL("https://www.instagram.com/gameofwinners_/");
    }
}
