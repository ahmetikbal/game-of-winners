﻿using UnityEngine;

[CreateAssetMenu(menuName = "MEDUSA GAMES/Add New Game Settings")]
public class CurrentGameSettings : ScriptableObject
{
    //[TextArea] [ReadOnly] public string Medusa = "Deneme Yazı Alanı";

    [Header("Menüde Gözükecek Bilgiler")]
    public string gameName;
    [Tooltip("420x300 fotoğraf")] public Sprite gamePhoto;

    [Header("Firestore Veritabanı")]
    public string documentNameInGameSettings;

    public enum gameType
    {
        Normal,
        Premium
    }

    [Header("Diğer")]
    public gameType _gameType;
    public string sceneNameForThisGame;
    public string highScorePlayerPrefsName;

    [Header("Veritabanından Çekilen Bilgiler")]
    [ReadOnly] public string startTime;
    [ReadOnly] public string endTime;
    [ReadOnly] public string collectionNameForSaveScore;

    public System.TimeSpan remainingTime;

    public enum gameStatus
    {
        Passive,
        WaitToOpen,
        Active
    }
    [Header("Game Status")]
    [ReadOnly, SerializeField] public gameStatus _gameStatus;
}