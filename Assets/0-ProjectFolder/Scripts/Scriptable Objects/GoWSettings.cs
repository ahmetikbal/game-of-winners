﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MEDUSA GAMES/GoW Settings")]
public class GoWSettings : ScriptableObject
{
    [Header("Listelenecek Oyunlar")]
    public List<CurrentGameSettings> GameList = new List<CurrentGameSettings>();

    [ReadOnly] public CurrentGameSettings currentGameSettingsAsset;
}
