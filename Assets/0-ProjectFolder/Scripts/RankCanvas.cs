﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using CodeStage.AntiCheat.Storage;

public class RankCanvas : MonoBehaviour
{
    public static RankCanvas Instance;

    // RANKING
    public TextMeshProUGUI[] rankingTexts;
    public TextMeshProUGUI scoreTexts;

    public TextMeshProUGUI playerText, scoreText;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        //    DontDestroyOnLoad(this);
        }
        else
        {
       //     GameObject.Destroy(this);
        }

    //    gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        rankingTexts[0].text = "";
        rankingTexts[1].text = "";
        rankingTexts[2].text = "";
        rankingTexts[3].text = "";
        scoreTexts.text = "";

        playerText.text = PlayerPrefs.GetString("username", "PLAYER");
        scoreText.text = ObscuredPrefs.GetInt(GameManager.Instance.currentHighScorePlayerPrefsName,0).ToString();

        FireStoreManager.Instance.GetCurrentGameRanking();
    }

}
