﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;

    //    System.DateTime newFirstGame = new System.DateTime(2020, 11, 20, 00, 00, 00); //OLD SYSTEM
    //    System.DateTime newSecondGame = new System.DateTime(2020, 11, 20, 00, 00, 00); //OLD SYSTEM

    //System.DateTime newGameDate/* = new System.DateTime(2020, 11, 20, 00, 00, 00)*/; //OLD SYSTEM

    //public System.TimeSpan remainTime; //OLD SYSTEN
    //public string remainTimeText; //OLD SYSTEM

    public System.TimeSpan remainingTime;

    public GameManager gameManager;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        /*
        print(System.DateTime.Now);
        print(System.DateTime.Now.Hour);
        print(System.DateTime.Now.ToLongTimeString()); //PM OLARAK VERDİ
        print(System.DateTime.UtcNow); // 3 SAAT GERİ (PM DEĞİL DİREKT)
        print(System.DateTime.UtcNow.Hour);
        print(System.DateTime.UtcNow.ToString()); //PM OLARAK VERDİ
        print(System.DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss"));  //PM DEĞİLLLLLLLLLLLLL
        print(System.DateTime.UtcNow.ToString("M/d/yy   hh:mm tt")); // PM
        print(System.DateTime.UtcNow.ToLongTimeString()); // saniyeli pm
        print(System.DateTime.UtcNow.ToShortTimeString()); //saniyesiz pm
        print(System.DateTime.UtcNow.ToLocalTime()); // İLK PRİNTLE AYNI DEĞERİ VERDİ
        print(System.DateTime.UtcNow.ToUniversalTime());
        */

        /*
        if (PlayerPrefs.GetString("email") == "test@medusa.games")
        {
            print("test maili");
            gameManager.currentGame = 1;
            PlayerPrefs.SetInt("currentGame", 1);
        }
        else
        {
            print("test maili değil");
            if (PlayerPrefs.GetInt("currentGame") == 0)
            {
                print("ilk oyunda");

                if (remainTime.Days < 0 || remainTime.Hours < 0 || remainTime.Minutes < 0 || remainTime.Seconds < 0)
                {
                    gameManager.currentGame = 1;
                    PlayerPrefs.SetInt("currentGame", 1);

                }
                else
                {
                    print("niye?");
                    gameManager.currentGame = 0;
                    PlayerPrefs.SetInt("currentGame", 0);
                }
            }
        }
        */
    }

    #region OldSystem
    /*
    private void OnEnable()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            remainTimeText = "İnternet Bağlantısı Yok";
        }

      //      RemainTime();

    }


    public void RemainTime()
    {
        if (receivedFirestoreDate) {
            var dateNow = System.DateTime.UtcNow.ToLocalTime();

            
        //   if (gameManager.currentGame == 0)
        //       remainTime = newFirstGame - dateNow;
        //   else if (gameManager.currentGame == 1)
        //       remainTime = newSecondGame - dateNow;
                

            remainTime = newGameDate - dateNow;

            if (GameManager.Instance.newVersion > GameManager.Instance.currentVersion)
            {
                remainTimeText = "Yeni Yarışma İçin Güncelle";
            }
            else
            {
                if (remainTime.CompareTo(System.TimeSpan.Zero) > 0)
                    remainTimeText = "Kalan Zaman: " + remainTime.Days + " Gün " + remainTime.Hours + " Saat " + remainTime.Minutes + " Dk";
                else
                    remainTimeText = "Yeni Yarışma Çok Yakında";
            }

            print(remainTimeText);
            print(newGameDate);
        }
    }

    public bool receivedFirestoreDate = false;
    public void CreateDatabaseDate(string firestoreDate)
    {
        receivedFirestoreDate = true;

        newGameDate = System.DateTime.Parse(firestoreDate); // FORMAT = 2020-11-25
        RemainTime();
    }
    */
    #endregion

    //NEW SYSTEM

    public System.TimeSpan GetRemainingTimeForGame(CurrentGameSettings gameSettingsAsset)
    {
        var startDate = System.DateTime.Parse(gameSettingsAsset.startTime); // FORMAT = 2020-11-25
        var endDate = System.DateTime.Parse(gameSettingsAsset.endTime); // FORMAT = 2020-11-25

        var dateNow = System.DateTime.UtcNow.ToLocalTime();

        if ((endDate - dateNow).CompareTo(System.TimeSpan.Zero) > 0) //BİTİŞ TARİHİ İLERİ BİR TARİH İSE
        {

            if ((startDate - dateNow).CompareTo(System.TimeSpan.Zero) > 0) //BAŞLANGIÇ TARİHİ İLERİ BİR TARİH İSE
            {
                gameSettingsAsset._gameStatus = CurrentGameSettings.gameStatus.WaitToOpen;

                var remainTime = startDate - dateNow; //AÇILMASINA KALAN ZAMAN

                gameSettingsAsset.remainingTime = remainTime;
                return remainTime;
            }
            else //BAŞLANGIÇ TARİHİ GEÇMİŞSE BİTİŞ TARİHİ İLERİDE İSE (YARIŞMA AKTİF)
            {
                gameSettingsAsset._gameStatus = CurrentGameSettings.gameStatus.Active;

                var remainTime = endDate - dateNow; //BİTMESİNE KALAN ZAMAN

                gameSettingsAsset.remainingTime = remainTime;
                return remainTime;
            }
        }
        else
        {
            gameSettingsAsset._gameStatus = CurrentGameSettings.gameStatus.Passive;

            gameSettingsAsset.remainingTime = System.TimeSpan.Zero;
            return System.TimeSpan.Zero;
        }
    }
    
    public string GetRemainingTimeText()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            var currentGame = GameManager.Instance.goWSettings.currentGameSettingsAsset;

            if (currentGame._gameStatus == CurrentGameSettings.gameStatus.Active)
            {
                var remainTime = currentGame.remainingTime;
                return "Kalan Zaman:\n" + remainTime.Days + " Gün " + remainTime.Hours + " Saat " + remainTime.Minutes + " Dk";
            }
            else
            {
                return "Yarışma Şu Anda Aktif Değil!";
            }
        }
        else
        {
            return "İnternet Bağlantısı Yok";
        }
    }
    
}