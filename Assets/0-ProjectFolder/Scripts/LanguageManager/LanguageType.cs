public enum LanguageType
{
    English = 0,
    Turkish = 1,
    Russian = 2,
    German = 3,
    French = 4,
    Spanish = 5,
    Portuguese = 6,
    Arabic = 7,
    Hindi = 8
}