using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SwitchTextWithLanguage : MonoBehaviour
{
    public string id;
    public string[] @params;

    private Text text;
    private TextMeshPro textTextMeshPro;
    private TextMeshProUGUI textTextMeshProUGUI;

    private LanguageManager languageManager;


    private void Start()
    {
        LanguageManager.OnChangedLanguage += OnChangedLanguage;
        languageManager = LanguageManager.Instance;

        if(GetComponent<Text>() != null)
            text = GetComponent<Text>();
        else if(GetComponent<TextMeshPro>() != null)
            textTextMeshPro = GetComponent<TextMeshPro>();
        else if(GetComponent<TextMeshProUGUI>() != null)
            textTextMeshProUGUI = GetComponent<TextMeshProUGUI>();

        UpdateText();
    }

    private void OnDestroy()
    {
        LanguageManager.OnChangedLanguage -= OnChangedLanguage;
    }

    private void OnChangedLanguage()
    {
        UpdateText();
    }

    public void UpdateText()
    {
        if (id.Length < 1)
        {
            Debug.Log("Empty ID! Object name: " + gameObject.name);
            return;
        }

        if (text != null)
            text.text = languageManager.GetValue(id, @params);
        else if (textTextMeshPro != null)
            textTextMeshPro.text = languageManager.GetValue(id, @params);
        else if (textTextMeshProUGUI != null)
            textTextMeshProUGUI.text = languageManager.GetValue(id, @params);

    //    text.text = languageManager.GetValue(id, @params);
    }

}