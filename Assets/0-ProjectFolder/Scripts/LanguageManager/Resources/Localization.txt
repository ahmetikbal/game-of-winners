﻿Language Count = 2


()
English=>
Turkish=>
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(language)
English=>LANGUAGE
Turkish=>DIL
Russian=>ЯЗЫК
German=>SPRACHE
French=> LANGUE
Spanish=>IDIOMA
Portuguese=>LÍNGUA
Arabic=>اللغة 
Hindi=>भाषा

(login)
English=>LOGIN
Turkish=>GİRİŞ YAP
Russian=>ВХОД
German=>ANMELDEN
French=> S’IDENTIFIER
Spanish=>INGRESAR
Portuguese=>INICIAR O USO
Arabic=>تسجيل الدخول 
Hindi=>लॉग इन

(donthaveanaccount)
English=>I don't have an account
Turkish=>Hesabınız yok mu?
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(emailWarning)
English=>If you win a prize, you will be sent an e-mail. Make sure you enter your e-mail address correctly.
Turkish=>Eğer ödül kazanırsanız, size e-mail gönderilecek. E-mail adresinizi doğru girdiğinizden emin olunuz.
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(register)
English=>REGISTER
Turkish=>KAYDOL
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(usernameInputField)
English=>Nickname*
Turkish=>Kullanıcı Adı*
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(alreadyaccount)
English=>I've already an account
Turkish=>Zaten hesabınız var mı?
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(profile)
English=>PROFILE
Turkish=>PROFİL
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(activegames)
English=>ACTIVE GAMES
Turkish=>AÇIK YARIŞMALAR
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(futuregames)
English=>FUTURE GAMES
Turkish=>GELECEK OYUNLAR
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(premiumgames)
English=>PREMIUM COMPETITIONS
Turkish=>PREMIUM YARIŞMALAR
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(onlinePlayer)
English=>Online Player
Turkish=>Çevrimiçi Oyuncu
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(toEnd)
English=>TO END:
Turkish=>BİTMESİNE:
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(toStart)
English=>TO START:
Turkish=>BAŞLAMASINA:
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(networkError)
English=>NETWORK ERROR
Turkish=>İNTERNET YOK
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(passiveGame)
English=>THE COMPETITION IS END. IT WILL START AGAIN IN A SHORT TIME!
Turkish=>YARIŞMA SONA ERDİ. KISA SÜREDE TEKRAR BAŞLAYACAK!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(day)
English=>Day
Turkish=>Gün
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(hour)
English=>Hour
Turkish=>Saat
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(minute)
English=>Mnt
Turkish=>Dk
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(loading)
English=>LOADING...
Turkish=>YÜKLENİYOR...
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(loggedIn)
English=>You have already logged in. Would you like to login again?
Turkish=>Zaten üye girişi yaptın. Yeniden giriş yapmak ister misin?
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(notLoggedIn)
English=>You did not register. Don't forget to register to win prizes!
Turkish=>Yarışmaya kaydolmadın. Ödülleri kazanabilmek için kaydolmayı unutma!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(yes)
English=>YES
Turkish=>EVET
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(forceupdate)
English=>An update is required to continue playing and racing!
Turkish=>Oyunlara ve yarışmaya devam edebilmek için güncelleme yapman gerekiyor!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(update)
English=>UPDATE
Turkish=>GÜNCELLE
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(registrationWindow)
English=>REGISTRATION WINDOW
Turkish=>KAYIT EKRANI
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(soon)
English=>Soon...
Turkish=>Yakında...
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(loginFailed)
English=>Login Failed!
Turkish=>Giriş Başarısız!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(missingEmail)
English=>Missing Email
Turkish=>Email Giriniz
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(missingPassword)
English=>Missing Password
Turkish=>Şifre Giriniz
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(wrongPassword)
English=>Wrong password
Turkish=>Yanlış şifre
Russian=>Неверный  пароль
German=>Falsches Passwort
French=>Mauvais mot de passe
Spanish=>Contraseña incorrecta
Portuguese=>Senha inválida
Arabic=>كلمة المرور غير صحيحة
Hindi=>गलत पासवर्ड

(invalidEmail)
English=>Invalid Email
Turkish=>Geçersiz Email
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(userNotFound)
English=>User not found
Turkish=>Hesap mevcut değil
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(succesfullyLoggedIn)
English=>Logged In!
Turkish=>Giriş Başarılı!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(emptyNickname)
English=>Empty Nickname
Turkish=>Kullanıcı Adı Giriniz
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(nicknameWarning)
English=>Nickname must be 4-16 characters long
Turkish=>Kullanıcı Adı 4-16 Karakter Uzunluğunda Olmalıdır
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(registrationFailed)
English=>Registration Failed!
Turkish=>Kayıt Başarısız!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(emailAlreadyInUse)
English=>Email Already In Use
Turkish=>Email Zaten Kullanılıyor
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(nicknameIsAlreadyTaken)
English=>Nickname is already taken
Turkish=>Bu kullanıcı adı alındı
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(serverError)
English=>Server error. Try again!
Turkish=>Sunucu hatası. Tekrar deneyin!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(registrationSuccessful)
English=>Registration Successful!
Turkish=>Kayıt Başarılı!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(notPremiumUser)
English=>You Should Be a Premium User For the Premium Games Which Have Big Awards!
Turkish=>Büyük Ödüllü Premium Oyunlarda Yarışabilmek İçin Premium Üye Olmalısın!
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(bePremium)
English=>BE PREMIUM
Turkish=>PREMIUM OL
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(shop)
English=>SHOP
Turkish=>MARKET
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(mainmenu)
English=>Main Menu
Turkish=>Ana Sayfa
Russian=>
German=>
French=>
Spanish=>
Portuguese=>
Arabic=>
Hindi=>

(select)
English=>SELECT
Turkish=>SEÇ
Russian=>ВЫБРАТЬ
German=>AUSWÄHLEN
French=>CHOSIS
Spanish=>ELEGIR
Portuguese=>ESCOLHER
Arabic=>اختر 
Hindi=>चुनना

(selected)
English=>SELECTED
Turkish=>SEÇİLİ
Russian=>ВЫБРАНО
German=>AUSGEWÄHLT
French=>CHOISI
Spanish=>ELEGIDO
Portuguese=> ELEITO
Arabic=>المحدد
Hindi=>चुन लिया

(buy)
English=>BUY
Turkish=>SATIN AL
Russian=>КУПИТЬ
German=>KAUF
French=> ACHÈTES
Spanish=>COMPRAR
Portuguese=>COMPRAR
Arabic=>اشتري
Hindi=>खरीदना

(watchvideo)
English=>WATCH VIDEO
Turkish=>VIDEO IZLE
Russian=>СМОТРЕТЬ ВИДЕО 
German=> VIDEO ANSCHAUEN
French=> REGARDES VIDEO
Spanish=>VER VIDEO
Portuguese=>VER VÍDEO
Arabic=> شاهد الفيديو
Hindi=>वीडियो देखें

(follow)
English=>FOLL
Turkish=>TAKIP ET
Russian=>ОТСЛЕЖИВАТЬ 
German=>FOLGEN
French=> SUIVRE
Spanish=>SEGUIR
Portuguese=>SEGUIR
Arabic=>تابع
Hindi=>फॉलो

(ok)
English=>OK
Turkish=>TAMAM
Russian=>ОК
German=>OK
French=>OK (D’ACCORD)
Spanish=>OK
Portuguese=>OK
Arabic=>حسناً
Hindi=>ठीक है

(menu)
English=>MENU
Turkish=>MENÜ
Russian=>МЕНЮ
German=>MENÜ
French=>MENU
Spanish=>MENÚ
Portuguese=>CARDÁPİO
Arabic=>القائمة
Hindi=>मेन्यू

(restorePurchases)
English=>RESTORE PURCHASES
Turkish=>SATIN ALIMLARI GERİ YÜKLE
Russian=>ОБНОВИТЬ ПОКУПКИ
German=>EINKÄUFE WIEDERHERSTELLEN
French=>RESTAURER LES ACHATS
Spanish=>RESTAURAR LAS COMPRAS
Portuguese=>RESTAURAR AS COMPRAS
Arabic=>استعادة المشتريات
Hindi=>खरीद बहाल करें

(rateUsHeader)
English=>DO YOU LIKE OUR GAME? RATE US!
Turkish=>OYUNUMUZU BEĞENİYOR MUSUNUZ? BİZİ OYLAYIN!
Russian=> ВАМ НРАВИТЬСЯ НАША ИГРА? ОЦЕНИТЕ!
German=>GEFÄLLT IHNEN UNSER SPIEL? STIMMEN SIE UNS AB! 
French=>AIMEZ-VOUS NOTRE JEU? ÉVALUEZ NOUS!
Spanish=>¿TE GUSTA NUESTRO JUEGO? ¡PUNTUANOS!
Portuguese=>VOCÊ GOSTA DO NOSSO JOGO? PUNTUENOS!
Arabic=>هل تحب لعبتنا؟ قيمنا!
Hindi=>क्या आपको हमारा खेल पसंद है? हमें रेटिंग दें!

(rate)
English=>RATE!
Turkish=>OYLA!
Russian=>ОЦЕНИТЬ!
German=>ABSTIMMUNG!
French=>VOTER!
Spanish=>PUNTUA!
Portuguese=>PONTUA!
Arabic=>!قيم 
Hindi=>रेट

(later)
English=>LATER
Turkish=>DAHA SONRA
Russian=>ПОЗЖЕ
German=>SPÄTER 
French=>PLUS TARD
Spanish=>LUEGO
Portuguese=>LOGO
Arabic=>فيما بعد
Hindi=>बाद में, 

(never)
English=>NEVER
Turkish=>ASLA
Russian=>НИКОГДА
German=>NIEMALS 
French=>JAMAIS
Spanish=>NUNCA
Portuguese=>NUNCA
Arabic=>أبداً
Hindi=>कभी नहीँ

