using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Events;
//using Sirenix.OdinInspector;

public class LanguageManager : MonoBehaviour
{
    public static LanguageManager Instance;

    [System.Serializable]
    public class LanguageValueClass
    {
        public LanguageType language;
        public string value;
    }

    [System.Serializable]
    public class LanguageDataClass
    {
        public string id;
        public List<LanguageValueClass> localizations = new List<LanguageValueClass>();
    }

    private TextAsset languageTextAsset;

//    [DisableInEditorMode, DisableInPlayMode]
    public List<LanguageDataClass> localizations = new List<LanguageDataClass>();
//    [DisableInEditorMode, DisableInPlayMode]
    public List<LanguageType> availableLanguages = new List<LanguageType>();
//    [DisableInEditorMode, DisableInPlayMode]
    public LanguageType selectedLanguage;

    public const LanguageType DefaultLanguage = LanguageType.English;

    public static UnityAction OnChangedLanguage;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);

            LoadSystem();
        }
        else
        {
            Destroy(gameObject);
        }


        string selectedLanguage = PlayerPrefs.GetString("SelectedLanguage", "");
        if (!HasLanguage(selectedLanguage))
        {
            string localLanguage = Application.systemLanguage.ToString();
            if (HasLanguage(localLanguage))
            {
                this.selectedLanguage = SystemLanguage_To_LanguageType(Application.systemLanguage);
            }
            else
            {
                this.selectedLanguage = DefaultLanguage;
            }
            PlayerPrefs.SetString("SelectedLanguage", this.selectedLanguage.ToString());
            PlayerPrefs.Save();
        }
        else
        {
            this.selectedLanguage = Extensions.ParseEnum<LanguageType>(selectedLanguage);
        }
    }

    private static LanguageType SystemLanguage_To_LanguageType(SystemLanguage lang)
    {
        try
        {
            return Extensions.ParseEnum<LanguageType>(lang.ToString());
        }
        catch (Exception)
        {
            return LanguageType.English;
        }
        
    }

    private bool HasLanguage(string language)
    {
        for (int i = 0; i < availableLanguages.Count; i++)
            if (availableLanguages[i].ToString() == language)
                return true;

        return false;
    }

    public void ChangeLanguage(LanguageType language)
    {
        ChangeLanguage(language.ToString());
    }

    public string GetLocalizedLanguageString(LanguageType language)
    {
        switch (language)
        {
            case LanguageType.English:
                return "English";
            case LanguageType.Turkish:
                return "Türkçe";
            case LanguageType.Russian:
                return "русский";
            case LanguageType.German:
                return "Deutsche";
            case LanguageType.French:
                return "Français";
            case LanguageType.Spanish:
                return "Español";
            case LanguageType.Portuguese:
                return "Português";
            case LanguageType.Arabic:
                return ArabicSupport.ArabicFixer.Fix("عربى");
            case LanguageType.Hindi:
                return "हिन्दी";
            default:
                return GetLocalizedLanguageString(DefaultLanguage);
        }
    }

    public void ChangeLanguage(string language)
    {
        for (int i = 0; i < availableLanguages.Count; i++)
        {
            if (availableLanguages[i].ToString() == language)
            {
                selectedLanguage = availableLanguages[i];
                PlayerPrefs.SetString("SelectedLanguage", availableLanguages[i].ToString());
                PlayerPrefs.Save();
                // LoadSystem();
                if (OnChangedLanguage != null)
                    OnChangedLanguage();
                return;
            }
        }
        Debug.LogError("The language is not found! {" + language + "}");
    }


    private void LoadSystem()
    {
        print("SİSTEM BAŞLARRRRRRRRRRRRRRRRRRRRRRR");
        localizations.Clear();
        availableLanguages.Clear();

        languageTextAsset = Resources.Load<TextAsset>("Localization");
        string[] allLines = languageTextAsset.text.Split('\n');

        int languageCount = int.Parse(allLines[0].Substring(allLines[0].IndexOf("=") + 1,
            allLines[0].Length - allLines[0].IndexOf("=") - 1).Replace(" ", ""));

        int errorIndex = 0;
        int continueCount = 0;

        for (int i = 0; i < allLines.Length; i++)
        {
            if (allLines[i].Replace(" ", "").Length < 3)
                continue;

            if (continueCount > 0)
            {
                continueCount -= 1;
                continue;
            }

            if (allLines[i].Contains("(") && allLines[i].Contains(")"))
            {
                try
                {
                    LanguageDataClass localization = new LanguageDataClass();
                    localization.id = allLines[i].Replace("(", "").Replace(")", "").Replace(" ", "").Replace(System.Environment.NewLine, "");

                    for (int ii = 1; ii <= languageCount; ii++)
                    {
                        LanguageValueClass languageValue = new LanguageValueClass();
                        int indexFlag = i + ii;
                        errorIndex = indexFlag;
                        //Debug.Log(allLines[indexFlag] + " __ " + errorIndex);
                        languageValue.language = Extensions.ParseEnum<LanguageType>
                            (allLines[indexFlag].Substring(0, allLines[indexFlag].IndexOf("=>")));
                        languageValue.value = allLines[indexFlag].Substring(allLines[indexFlag].IndexOf("=>") + 2).Replace(System.Environment.NewLine, "");
                        localization.localizations.Add(languageValue);

                        if (availableLanguages.Count != languageCount)
                        {
                            availableLanguages.Add(languageValue.language);
                        }
                    }

                    localizations.Add(localization);
                    continueCount = languageCount;
                }
                catch (Exception e)
                {
                    Debug.LogError("Language Manager is not initialized! :\n" + allLines[errorIndex] + "\n" + errorIndex + "\n" + e.ToString());
                    localizations.Clear();
                    availableLanguages.Clear();
                }

            }

        }

    }

    public string GetValue(string id, params string[] @params)
    {
        return GetValue(id, selectedLanguage, @params);
    }

    public string GetValue(string id, LanguageType language, params string[] @params)
    {
        bool hasLanguage = false;
        for (int i = 0; i < availableLanguages.Count; i++)
        {
            if (availableLanguages[i] == language)
            {//istenilen dil var
                hasLanguage = true;
                break;
            }
        }

        for (int i = 0; i < localizations.Count; i++)
        {
            if (localizations[i].id.Trim() == id.Trim())
            {
                for (int ii = 0; ii < localizations[i].localizations.Count; ii++)
                {
                    if (hasLanguage && localizations[i].localizations[ii].language == language)
                    {
                        string result = "";
                        if (@params == null || @params.Length < 1)
                            result = localizations[i].localizations[ii].value;
                        else
                            result = string.Format(localizations[i].localizations[ii].value, @params);

                        if (selectedLanguage == LanguageType.Arabic)
                            return ArabicSupport.ArabicFixer.Fix(result);
                        else
                            return result;

                    }
                    else if (!hasLanguage && localizations[i].localizations[ii].language == DefaultLanguage)
                    {
                        string result = "";
                        if (@params == null || @params.Length < 1)
                            result = localizations[i].localizations[ii].value;
                        else
                            result = string.Format(localizations[i].localizations[ii].value, @params);

                        if (selectedLanguage == LanguageType.Arabic)
                            return ArabicSupport.ArabicFixer.Fix(result);
                        else
                            return result;
                    }
                }
                break;
            }
        }
        Debug.Log("Language value isn't found! {" + id + "} {" + language + "}");
        return "NAN";
    }

}