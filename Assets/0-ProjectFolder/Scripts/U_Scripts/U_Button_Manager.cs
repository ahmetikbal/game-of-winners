using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class U_Button_Manager : MonoBehaviour
{
    public GameObject U_Background_Market;
    public GameObject U_Odul;
    public GameObject U_Info;
    public GameObject Kazi_Kazan_Video;
    public GameObject Kazi_Kazan_Coin;
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    public void Open_U_Background_Market()
    {
        U_Background_Market.SetActive(true);
    }
    public void Close_U_Background_Market()
    {
        U_Background_Market.SetActive(false);
    }
    public void Open_U_Odul()
    {
        U_Odul.SetActive(true);
    }
    public void Close_U_Odul()
    {
        U_Odul.SetActive(false);
    }
    public void Open_I_Info()
    {
        U_Info.SetActive(true);
    }
    public void Close_U_Info()
    {
        U_Info.SetActive(false);
    }
    public void Open_Kazi_Kazan_Video()
    {
        Kazi_Kazan_Video.SetActive(true);
    }
    public void Close_Kazi_Kazan_Video()
    {
        Kazi_Kazan_Video.SetActive(false);
    }
    public void Open_Kazi_Kazan_Coin()
    {
        Kazi_Kazan_Coin.SetActive(true);
    }
    public void Close_Kazi_Kazan_Coin()
    {
        Kazi_Kazan_Coin.SetActive(false);
    }

}
