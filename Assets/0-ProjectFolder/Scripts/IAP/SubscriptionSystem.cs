﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using CodeStage.AntiCheat.Storage;

public class SubscriptionSystem : MonoBehaviour
{
    public static SubscriptionSystem Instance;
    public enum SubscriptionStatus
    {
        None,
        Silver,
        Gold,
        Platinium,
        Endless
    }
    [ReadOnly] public SubscriptionStatus subscriptionStatus;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }

        SetSubcriptionStatus();
    }

    public void SetSubcriptionStatus()
    {
        var savedSubscriptionStatus = ObscuredPrefs.GetString("SubscriptionStatus");
        switch (savedSubscriptionStatus)
        {
            case "Silver":
                subscriptionStatus = SubscriptionStatus.Silver;
                break;
            case "Gold":
                subscriptionStatus = SubscriptionStatus.Gold;
                break;
            case "Platinium":
                subscriptionStatus = SubscriptionStatus.Platinium;
                break;
            case "Endless":
                subscriptionStatus = SubscriptionStatus.Endless;
                break;
            default:
                subscriptionStatus = SubscriptionStatus.None;
                break;
        }
    }

    void Start()
    {
        // Create a builder using the GooglePlayStoreModule.
        //  var configurationBuilder =
        //      ConfigurationBuilder.Instance(Google.Play.Billing.GooglePlayStoreModule.Instance());

        /*
        ConfigurationBuilder builder;
        if (Application.platform == RuntimePlatform.Android
               && SelectedAndoidAppStore == AppStore.GooglePlay)
        {
            builder = ConfigurationBuilder.Instance(
                Google.Play.Billing.GooglePlayStoreModule.Instance());
        }
        else
        {
            builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        }
        */

        // configurationBuilder.AddProduct
    }

    private void OnEnable()
    {
        //CheckSubscription(); BOZUK isSubscribed() HER ZAMAN TRUE DÖNDÜRÜYOR NE OLURSA OLSUN
        // https://developer.android.com/google/play/billing/unity#obfuscated-profile-ids bu sdk çözüm olabilir
    }

    public void CheckSubscription() //BOZUK
    {
        if (subscriptionStatus != SubscriptionStatus.Endless)
        {
            SubscriptionInfo info = new SubscriptionInfo(IAPManager.instance.silverPackage1Month);
            SubscriptionInfo silver3 = new SubscriptionInfo(IAPManager.instance.silverPackage3Months);
            SubscriptionInfo silver6 = new SubscriptionInfo(IAPManager.instance.silverPackage6Months);

            //PLATINIUMDAN AŞAĞI DOĞRU GİDECEK
            if ((info.isSubscribed() == Result.True && info.isExpired() != Result.True) ||
                (silver3.isSubscribed() == Result.True && silver3.isExpired() != Result.True) ||
                (silver6.isSubscribed() == Result.True && silver6.isExpired() != Result.True))
            {
                //SİLVER ÜYE
                ObscuredPrefs.SetString("SubscriptionStatus", "Silver");
                print("Bu kullanıcı Silver Üye");
            }
            else
            {
                ObscuredPrefs.SetString("SubscriptionStatus", "None");
                print("Bu kullanıcı Premium üye değil");
            }

            Debug.Log("product id is: " + info.getProductId());
            Debug.Log("purchase date is: " + info.getPurchaseDate());
            Debug.Log("subscription next billing date is: " + info.getExpireDate());
            Debug.Log("is subscribed? " + info.isSubscribed().ToString());
            Debug.Log("is expired? " + info.isExpired().ToString());
            Debug.Log("is cancelled? " + info.isCancelled());
            Debug.Log("product is in free trial peroid? " + info.isFreeTrial());
            Debug.Log("product is auto renewing? " + info.isAutoRenewing());
            Debug.Log("subscription remaining valid time until next billing date is: " + info.getRemainingTime());
            Debug.Log("is this product in introductory price period? " + info.isIntroductoryPricePeriod());
            Debug.Log("the product introductory localized price is: " + info.getIntroductoryPrice());
            Debug.Log("the product introductory price period is: " + info.getIntroductoryPricePeriod());
            Debug.Log("the number of product introductory price period cycles is: " + info.getIntroductoryPricePeriodCycles());

        }

    }

  //  private void Update()
  //  {
  //      if (Input.GetKeyDown(KeyCode.Escape))
  //          CheckSubscription();
  //  }

}