﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.Storage;

public class IAP_UIController : MonoBehaviour
{
    public static IAP_UIController Instance;

    public Button removeAds;

    public Text removeAdsPrice;  //PRICES

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        removeAdsPrice.text = removeAdsPriceString;

        SetInteractable();
     //   Invoke("SetIAPPrices", 1f); //FİYAT OYUNDAN KALDIRILDI
     //   SetIAPPrices();
    }

    public void SetInteractable() //FOR NON-CONSUMABLE PRODUCTS
    {
        if (ObscuredPrefs.GetInt("RemoveAdsBought") == 1)
            removeAds.interactable = false;

    }

    public void BuyRemoveAdsButton()
    {
        IAPManager.instance.BuyRemoveAds();
    }
    public void MonthlySubscribeButtons(string whichPackage)
    {
        IAPManager.instance.SubscribePackages(whichPackage);
    }

    public static string removeAdsPriceString;
    public void SetIAPPrices()
    {
        if (IAPManager.m_StoreController != null)
        {

            removeAdsPriceString = IAPManager.m_StoreController.products.WithID("remove_ads").metadata.localizedPriceString;
            removeAdsPrice.text = removeAdsPriceString;

        }
        else
            IAPManager.instance.InitializePurchasing();

    }


}
