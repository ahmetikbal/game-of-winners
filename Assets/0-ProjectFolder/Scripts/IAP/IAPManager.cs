﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;
using CodeStage.AntiCheat.Storage;
using com.adjust.sdk;
//using AppodealAds.Unity.Api;

public class IAPManager : MonoBehaviour, IStoreListener
{
    public static IAPManager instance;

    public static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    //Step 1 create your products
    private string removeAds = "remove_ads";

    public string silverPackage1Month = "silverpackage_1month";
    public string silverPackage3Months = "silverpackage_3months";
    public string silverPackage6Months = "silverpackage_6months";

    public string goldPackage1Month = "goldpackage_1month";
    public string goldPackage3Months = "goldpackage_3months";
    public string goldPackage6Months = "goldpackage_6months";

    public string platiniumPackage1Month = "platiniumpackage_1month";
    public string platiniumPackage3Months = "platiniumpackage_3months";
    public string platiniumPackage6Months = "platiniumpackage_6months";

    public string unlimitedPackage = "unlimitedpackage";

    //************************** Adjust these methods **************************************
    public void InitializePurchasing()
    {
        if (IsInitialized()) { return; }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        //Step 2 choose if your product is a consumable or non consumable
        builder.AddProduct(removeAds, ProductType.NonConsumable);

        builder.AddProduct(silverPackage1Month, ProductType.Subscription);
        builder.AddProduct(silverPackage3Months, ProductType.Subscription);
        builder.AddProduct(silverPackage6Months, ProductType.Subscription);

        builder.AddProduct(goldPackage1Month, ProductType.Subscription);
        builder.AddProduct(goldPackage3Months, ProductType.Subscription);
        builder.AddProduct(goldPackage6Months, ProductType.Subscription);

        builder.AddProduct(platiniumPackage1Month, ProductType.Subscription);
        builder.AddProduct(platiniumPackage3Months, ProductType.Subscription);
        builder.AddProduct(platiniumPackage6Months, ProductType.Subscription);

        builder.AddProduct(unlimitedPackage, ProductType.NonConsumable);

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    //Step 3 Create methods
    public void BuyRemoveAds()
    {
        BuyProductID(removeAds);
    }
    public void SubscribePackages(string whichPackage)
    {
        BuyProductID(whichPackage);
    }


    //Step 4 modify purchasing
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, removeAds, StringComparison.Ordinal))
        {
            Debug.Log("Remove ads succesful");
            ObscuredPrefs.SetInt("RemoveAdsBought", 1);
            IAP_UIController.Instance.SetInteractable();

            ReklamScript.BannerGizle();
            
            AdjustEvent adjustEvent = new AdjustEvent("o2pdmh");
            adjustEvent.setRevenue((double)m_StoreController.products.WithID("remove_ads").metadata.localizedPrice, m_StoreController.products.WithID("remove_ads").metadata.isoCurrencyCode);
            Adjust.trackEvent(adjustEvent);

        //    Appodeal.trackInAppPurchase((double)m_StoreController.products.WithID("remove_ads").metadata.localizedPrice, m_StoreController.products.WithID("remove_ads").metadata.isoCurrencyCode);

        }
        else if (String.Equals(args.purchasedProduct.definition.id, silverPackage1Month, StringComparison.Ordinal) || String.Equals(args.purchasedProduct.definition.id, silverPackage3Months, StringComparison.Ordinal) || String.Equals(args.purchasedProduct.definition.id, silverPackage6Months, StringComparison.Ordinal))
        {
            //SILVER ABONELIK
            ObscuredPrefs.SetString("SubscriptionStatus", "Silver");
            SubscriptionSystem.Instance.SetSubcriptionStatus();
            Debug.Log("Silver Abone olundu");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, goldPackage1Month, StringComparison.Ordinal) || String.Equals(args.purchasedProduct.definition.id, goldPackage3Months, StringComparison.Ordinal) || String.Equals(args.purchasedProduct.definition.id, goldPackage6Months, StringComparison.Ordinal))
        {
            //GOLD ABONELIK
            ObscuredPrefs.SetString("SubscriptionStatus", "Gold");
            SubscriptionSystem.Instance.SetSubcriptionStatus();
            Debug.Log("Gold Abone olundu");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, platiniumPackage1Month, StringComparison.Ordinal) || String.Equals(args.purchasedProduct.definition.id, platiniumPackage3Months, StringComparison.Ordinal) || String.Equals(args.purchasedProduct.definition.id, platiniumPackage6Months, StringComparison.Ordinal))
        {
            //PLATINIUM ABONELIK
            ObscuredPrefs.SetString("SubscriptionStatus", "Platinium");
            SubscriptionSystem.Instance.SetSubcriptionStatus();
            Debug.Log("Platinium Abone olundu");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, unlimitedPackage, StringComparison.Ordinal))
        {
            //SINIRSIZ ABONELIK
            ObscuredPrefs.SetString("SubscriptionStatus", "Endless");
            SubscriptionSystem.Instance.SetSubcriptionStatus();
            Debug.Log("Endless Abone olundu");
        }
        else
        {
            Debug.Log("Purchase Failed");
        }
        return PurchaseProcessingResult.Complete;
    }









    //**************************** Dont worry about these methods ***********************************
    private void Awake()
    {
        TestSingleton();
    }

    void Start()
    {
        if (m_StoreController == null) { InitializePurchasing(); }
    }

    private void TestSingleton()
    {
        if (instance != null) { Destroy(gameObject); return; }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) => {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}