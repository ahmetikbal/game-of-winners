﻿using UnityEngine;

public class RestorePurchases : MonoBehaviour
{
    void Start()
    {
        if (Application.platform != RuntimePlatform.IPhonePlayer ||
            Application.platform != RuntimePlatform.OSXPlayer) {
            gameObject.SetActive(false);
        }
        else
        {
            IAPManager.instance.RestorePurchases();
        }
    }

    public void ClickRestorePurchaseButton() {
        IAPManager.instance.RestorePurchases();
    }
}
