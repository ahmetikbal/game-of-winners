﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    public GameObject loginArea, registerArea;

 //   public static int firstOpen = 0;

    private void Awake()
    {
        if (Instance == null) Instance = this;

        /* SPLASH SCREENDEN SONRA GEREK KALMADI
        if(firstOpen == 0) {

            firstOpen = 1;

            GameManager.Instance.LoadCurrentGame();
        }
        */
    }

    private void Start()
    {
        FirebaseAuthWithMail.Instance.statusLoginText.text = "";
        FirebaseAuthWithMail.Instance.statusRegisterText.text = "";
    }

    public void BackToLoginAreaButton()
    {
        loginArea.SetActive(true);
        registerArea.SetActive(false);

        FirebaseAuthWithMail.Instance.statusLoginText.text = "";
        FirebaseAuthWithMail.Instance.statusLoginText.color = Color.yellow;

        UIController_MainMenu.Instance.currentMenuText.text = LanguageManager.Instance.GetValue("login");
    }
    public void BackToRegisterAreaButton()
    {
        registerArea.SetActive(true);
        loginArea.SetActive(false);

        FirebaseAuthWithMail.Instance.statusRegisterText.text = "";
        FirebaseAuthWithMail.Instance.statusRegisterText.color = Color.yellow;

        UIController_MainMenu.Instance.currentMenuText.text = LanguageManager.Instance.GetValue("register");
    }

    public void CloseRegistration()
    {
        GameManager.Instance.LoadCurrentGame();
    }

}
