﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class Scenes
{
    [MenuItem("SCENES/Splash",priority = 0)]
    private static void OpenSplashScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-ProjectFolder/Scenes/SplashScreen.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/Main Menu", priority = 1)]
    private static void OpenMainMenuScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-ProjectFolder/Scenes/MainMenu_Main.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/Flappy Dunk/Menu")]
    private static void OpenFlappyDunkMenuScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-GAMES/FlappyDunk/Scenes/Start.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/Water Shooter/Game")]
    private static void OpenWaterShooterScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-GAMES/WaterShooter/Water_Shoter/Scenes/WaterShooter.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/BlockChaser/Menu")]
    private static void OpenBlockChaserMenuScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-GAMES/BlockChaser/Scenes/MainMenu.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/SpaceOrbit/Menu")]
    private static void OpenSpaceOrbitMenuScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-GAMES/SpaceOrbit/Scenes/Spaceorbit_MainMenu.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/CurvyPath/Game")]
    private static void OpenCurvyPathGameScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-GAMES/_CurvyPath/Scenes/CurvyPath.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/BlockMerger/Menu")]
    private static void OpenBlockMergerMenuScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/0-GAMES/BlockMerger/Scenes/Menu_BlockMerger.unity", OpenSceneMode.Single);
        }
    }

}
