﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;
using Firebase;
using Firebase.Extensions;
using Firebase.Firestore;
using System.Linq;
using System.IO;
using TMPro;
using CodeStage.AntiCheat.Storage;
using System.Threading.Tasks;

public class FireStoreManager : MonoBehaviour
{
    public static FireStoreManager Instance;

    private FirebaseFirestore fsDB;

    public CollectionReference users;

    //Force Update
    public CollectionReference forceupdate;

    //Remaining Time
    //public CollectionReference remainingtime; //OLD SYSTEM

    //Game Settings
    public CollectionReference gamesettings;

    //GAMES
    //public CollectionReference flappydunk;
    //public CollectionReference watershooter;
    //public CollectionReference blockchaser;
    //public CollectionReference spaceorbit;
    //public CollectionReference curvypath;
    public CollectionReference currentGameCollection; //YENİ SİSTEMLE TÜM OYUNLAR BUNU KULLARAK SCRIPTABLE OBJECTINDE YAZAN VERİTABANI İSMİYLE KOLEKSİYONU TANIMLAYACAK

    //custom structures
    internal struct fsUsers
    {
        internal string email;
        internal string nickname;
        internal string userid;
        internal string referencecode;
        internal object regtime;
        //      internal object lastlog;

        internal Dictionary<string, object> toDistionary()
        {
            Dictionary<string, object> userData = new Dictionary<string, object>();

            userData.Add("email", this.email);
            userData.Add("nickname", this.nickname);
            userData.Add("userid", this.userid);
            userData.Add("referencecode", this.referencecode);
            userData.Add("regtime", this.regtime);
            //          userData.Add("lastlog", this.lastlog);

            return userData;
        }
    }

    internal struct gameScore
    {
        internal string nickname;
        internal string email;
        internal int score;
        internal int gametime;

        internal Dictionary<string, object> toDistionary()
        {
            Dictionary<string, object> userData = new Dictionary<string, object>();

            userData.Add("nickname", this.nickname);
            userData.Add("email", this.email);
            userData.Add("score", this.score);
            userData.Add("gametime", this.gametime);

            return userData;
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        /*
        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            if (task.Result == DependencyStatus.Available)
            {
                SetupFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + task.Result);
            }
        });
        */

        Invoke("SetupFirebase", 0.5f);
        
    }

    /*
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
            SetupFirebase();

        if (Input.GetKeyDown(KeyCode.T))
            doTesting();

        if (Input.GetKeyDown(KeyCode.B))
            GetCurrentGameRanking(flappydunk);
    }
    */

    public void SetupFirebase()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {

            fsDB = FirebaseFirestore.DefaultInstance;
            users = fsDB.Collection("users");

            forceupdate = fsDB.Collection("forceupdate");
            //remainingtime = fsDB.Collection("remainingtime"); //OLD SYSTEM
            gamesettings = fsDB.Collection("gamesettings");

            //GAMES
            //flappydunk = fsDB.Collection("flappydunk");
            //watershooter = fsDB.Collection("watershooter2");
            //blockchaser = fsDB.Collection("blockchaser");
            //spaceorbit = fsDB.Collection("spaceorbit2");
            //curvypath = fsDB.Collection("curvypath");

            ForceUpdate();
            //RemainingTime(); OLD SYSTEM

            GameManager.Instance.firebaseSetupCompleted = true;

            CancelInvoke("SetupFirebase");

        });

    }

    public void SetOpenedGameCollection(string openedGameCollectionName)
    {
      //  FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
      //  {
            currentGameCollection = fsDB.Collection(openedGameCollectionName);
      //  });
    }

    [HideInInspector] public bool nicknameIsTaken = true; //kontrol ile false olmayı bekliyor
    [HideInInspector] public bool queryFinished = false;

    public void NicknameCheck(string nickname)
    {
        //Önceden bu kullanıcı adıyla doküman oluşturuldu mu
        Query query = users.WhereEqualTo("nickname", nickname); //nickname isimli field varsa
        query.GetSnapshotAsync().ContinueWithOnMainThread(querySnapshotTask =>
        {
            queryFinished = true;

            if (querySnapshotTask.IsFaulted)
            {
                Debug.Log("Kullanıcı bulunamadı sorgu gerçekleşemedi " + querySnapshotTask.Exception.InnerException.Message);
                FirebaseAuthWithMail.Instance.statusRegisterText.text = LanguageManager.Instance.GetValue("serverError"); //"Sunucu hatası. Tekrar deneyin!";
            }

            if (querySnapshotTask.Result.Documents.Count() < 1) // DAHA ÖNCE AYNI NICKNAMELI BİR KULLANICI YOKSA
            {
                nicknameIsTaken = false;
                FirebaseAuthWithMail.Instance.RegisterButton();
            }
            else
            {
                //AYNI NICKNAMELI KULLANICI ZATEN VAR
                nicknameIsTaken = true;

                FirebaseAuthWithMail.Instance.statusRegisterText.text = LanguageManager.Instance.GetValue("nicknameIsAlreadyTaken"); //"Kullanıcı adı alındı";

                Debug.Log("Bu nickname kayıtlı : " + nickname);
            }
        });
    }

    internal void fsAddNewUser(string nickname, Dictionary<string, object> userData)
    {
        DocumentReference docRef = users.Document(nickname);

        //   userData["regtime"] = FieldValue.ServerTimestamp;

        docRef.SetAsync(userData).ContinueWithOnMainThread(task =>
        {

            ////ALAN OLUŞTURMA TAMAMLANDIĞINDA
            ////LOADING SETACTIVE FALSE

            if (task.IsFaulted)
                Debug.Log("Kullanıcı oluşturulamadı " + task.Exception.InnerException.Message);
            else
                Debug.Log("Kullanıcı oluşturuldu " + docRef.Id);
         
        });
    }
    internal void fsAddNewScore(string nickname, Dictionary<string, object> userData, CollectionReference whichGameCollection)
    {
        DocumentReference docRef = whichGameCollection.Document(nickname);

        docRef.SetAsync(userData).ContinueWithOnMainThread(task =>
        {

            if (task.IsFaulted)
                Debug.Log("Skor gönderilemedi " + task.Exception.InnerException.Message);
            else
                Debug.Log("Skor gönderildi " + docRef.Id);

        });
    }

    public void CreateUserInformations()
    {
        var _fsUser = new fsUsers();
        _fsUser.userid = SystemInfo.deviceUniqueIdentifier;
        _fsUser.nickname = FirebaseAuthWithMail.Instance.usernameRegisterField.text;
        _fsUser.email = FirebaseAuthWithMail.Instance.emailRegisterField.text;
        _fsUser.regtime = FieldValue.ServerTimestamp;
        //      _fsUser.lastlog = FieldValue.ServerTimestamp;
        if (FirebaseAuthWithMail.Instance.referenceCodeRegisterField.text != "") _fsUser.referencecode = FirebaseAuthWithMail.Instance.referenceCodeRegisterField.text;

        fsAddNewUser(_fsUser.nickname, _fsUser.toDistionary());
    }

    /*
    public void CreateFlappyDunkScore()
    {
        if (PlayerPrefs.GetInt("LoggedIn") == 1)
        {
            var _fsScore = new gameScore();
            _fsScore.nickname = PlayerPrefs.GetString("username");
            _fsScore.email = PlayerPrefs.GetString("email");
            _fsScore.score = ObscuredPrefs.GetInt("Highscore"); //BallMove.Instance.score
            if (BallMove.Instance != null) _fsScore.gametime = Mathf.RoundToInt(BallMove.Instance.gameTimer);

            fsAddNewScore(_fsScore.nickname, _fsScore.toDistionary(), flappydunk);
        }
    }

    public void CreateWaterShooterScore()
    {
        if (PlayerPrefs.GetInt("LoggedIn") == 1)
        {
            var _fsScore = new gameScore();
            _fsScore.nickname = PlayerPrefs.GetString("username");
            _fsScore.email = PlayerPrefs.GetString("email");
            _fsScore.score = ObscuredPrefs.GetInt("High_Score_Index");
            //     if (BallMove.Instance != null) _fsScore.gametime = Mathf.RoundToInt(BallMove.Instance.gameTimer);

            fsAddNewScore(_fsScore.nickname, _fsScore.toDistionary(), watershooter);
        }
    }

    public void CreateBlockChaserScore()
    {
        if (PlayerPrefs.GetInt("LoggedIn") == 1)
        {
            var _fsScore = new gameScore();
            _fsScore.nickname = PlayerPrefs.GetString("username");
            _fsScore.email = PlayerPrefs.GetString("email");
            _fsScore.score = ObscuredPrefs.GetInt("BlockChaser_Highscore2");
            if (GameManager_BlockChaser.Instance != null) _fsScore.gametime = Mathf.RoundToInt(GameManager_BlockChaser.Instance.elapsedTime);

            fsAddNewScore(_fsScore.nickname, _fsScore.toDistionary(), blockchaser);
        }
    }

    public void CreateSpaceOrbitScore(int level)
    {
        if (PlayerPrefs.GetInt("LoggedIn") == 1)
        {
            var _fsScore = new gameScore();
            _fsScore.nickname = PlayerPrefs.GetString("username");
            _fsScore.email = PlayerPrefs.GetString("email");
            _fsScore.score = ObscuredPrefs.GetInt(GameManager.Instance.currentHighScorePlayerPrefsName);
            _fsScore.gametime = level;
            //GAMETIME YERINE LEVEL SAYISI GİDİYOR
            fsAddNewScore(_fsScore.nickname, _fsScore.toDistionary(), spaceorbit);
        }
    }
    */

    public void CreateCurrentGameScore(float? elapsedTimeOrLevelInTheGame = null) //NEW SYSTEM GENERAL FUNCTION
    {
        if (PlayerPrefs.GetInt("LoggedIn") == 1)
        {
            if ((GameManager.Instance.goWSettings.currentGameSettingsAsset._gameStatus == CurrentGameSettings.gameStatus.Active) //OYUN AKTİFSE
                || (PlayerPrefs.GetString("email") == "test@medusa.games")) //OYUN AKTİF DEĞİLSE BİLE test@medusa.games SCORE YOLLAR
            {

                var _fsScore = new gameScore();
                _fsScore.nickname = PlayerPrefs.GetString("username");
                _fsScore.email = PlayerPrefs.GetString("email");
                _fsScore.score = ObscuredPrefs.GetInt(GameManager.Instance.currentHighScorePlayerPrefsName);
                if (elapsedTimeOrLevelInTheGame != null) _fsScore.gametime = Mathf.RoundToInt((float)elapsedTimeOrLevelInTheGame);

                fsAddNewScore(_fsScore.nickname, _fsScore.toDistionary(), currentGameCollection);

            }
        }
    }

    void doTesting() //create test user
    {
        var _fsUser = new fsUsers();
        _fsUser.userid = SystemInfo.deviceUniqueIdentifier;
        _fsUser.nickname = "ahmetikbal";
        _fsUser.email = "ikbal@ikbal.com";
        _fsUser.regtime = FieldValue.ServerTimestamp;
        //      _fsUser.lastlog = FieldValue.ServerTimestamp;
        _fsUser.referencecode = "MEDUSA";

        fsAddNewUser(_fsUser.nickname, _fsUser.toDistionary());
        
    }

    //public ArrayList score_arrayList = new ArrayList();

    /* NOT TOUCHED
    public void GetCurrentGameRanking()
    {
        int i = 0;

        Query siralama = currentGameCollection.OrderByDescending("score").Limit(10);
        siralama.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) =>
        {
            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents) {
                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user) {
                    if (pair.Key != "email" && pair.Key != "gametime") {
                        Debug.Log(pair.Key + " " + pair.Value);

                        if (pair.Key == "nickname") {
                            if (i < 3) {
                                RankCanvas.Instance.rankingTexts[i].text += pair.Value + "\n\n";
                                i++;
                            }
                            else RankCanvas.Instance.rankingTexts[3].text += pair.Value + "\n\n";
                            
                        } else if (pair.Key == "score") {

                            var currentGame = GameManager.Instance.goWSettings.currentGameSettingsAsset;
                            var remainingTime = currentGame.remainingTime;
                            //AKTİF OYUNDA 3 GÜNDEN AZ KALMIŞSA SKORLAR AÇILACAK (PAZARTESİ), OYUN AKTİF DEĞİLSE DİREKT AÇILACAK. test@medusa.games mailinde direkt açılacak
                            if ((remainingTime.Days <= 3) || (currentGame._gameStatus != CurrentGameSettings.gameStatus.Active) || (PlayerPrefs.GetString("email") == "test@medusa.games")) //(remainingTime.Days <= 0 && remainingTime.Hours <= 5) YILBASINA 6 SAAT KALA
                                RankCanvas.Instance.scoreTexts.text += pair.Value + "\n\n";
                            else
                                RankCanvas.Instance.scoreTexts.text += "?" + "\n\n";
                        }
                    }
                }
            }
        });
    }
    */

    public void GetCurrentGameRanking() {
        int i = 0;

        Query siralama = currentGameCollection.OrderByDescending("score").Limit(10);
        siralama.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) => {
            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents) {
                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user) {
                    if (pair.Key != "email" && pair.Key != "gametime") {
                        Debug.Log(pair.Key + " " + pair.Value);

                        if (pair.Key == "nickname") {
                            if (i < 3) {
                                RankCanvas.Instance.rankingTexts[i].text += pair.Value + "\n\n";
                                i++;
                            }
                            else RankCanvas.Instance.rankingTexts[3].text += pair.Value + "\n\n";

                        }
                        else if (pair.Key == "score") {

                            var currentGame = GameManager.Instance.goWSettings.currentGameSettingsAsset;
                            var remainingTime = currentGame.remainingTime;
                            //AKTİF OYUNDA 3 GÜNDEN AZ KALMIŞSA SKORLAR AÇILACAK (PAZARTESİ), OYUN AKTİF DEĞİLSE DİREKT AÇILACAK. test@medusa.games mailinde direkt açılacak
                            if ((remainingTime.Days <= 3) || (currentGame._gameStatus != CurrentGameSettings.gameStatus.Active) || (PlayerPrefs.GetString("email") == "test@medusa.games")) //(remainingTime.Days <= 0 && remainingTime.Hours <= 5) YILBASINA 6 SAAT KALA
                                RankCanvas.Instance.scoreTexts.text += pair.Value + "\n\n";
                            else
                                RankCanvas.Instance.scoreTexts.text += "?" + "\n\n";
                        }
                    }
                }
            }
        });
    }

    /* BACKUP
    public void GetCurrentPremiumGameRanking() {
        int i = 0;
        string nickname = "";

        Query firstQuery = currentGameCollection.OrderByDescending("score").Limit(10);
        firstQuery.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) => {
            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents) {
                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user) {
                    // Debug.Log(pair.Key + " " + pair.Value);

                    if (pair.Key == "nickname") {
                        Debug.Log(pair.Key + " " + pair.Value);

                        if (i < 3) {
                            PremiumRankCanvas.Instance.rankingTexts[i].text = pair.Value + "";
                        }
                        else {
                            if (i == 2)
                                i++;

                            nickname = pair.Value.ToString();
                        }
                    }
                    else if (pair.Key == "score") {
                        Debug.Log(pair.Key + " " + pair.Value);

                        if (i < 3) {
                            PremiumRankCanvas.Instance.rankingTexts[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = pair.Value.ToString();
                        }
                        else {
                            PremiumRankCanvas.Instance.CreatePlayer(i, nickname, pair.Value.ToString());
                        }

                        i++;
                    }
                }
            }
        });
    }
    */

    public void Test() {
        for (int i = 20; i < 40; i++) {
            var _fsScore = new gameScore();
            _fsScore.nickname = "Medusa" + i;
            _fsScore.score = Random.Range(1, 1000);
            fsAddNewScore(_fsScore.nickname, _fsScore.toDistionary(), currentGameCollection);
        }
    }

    int lastDocument = 0, fetchIndex = 0;

    public void GetCurrentPremiumGameRanking() {
        string nickname = "";

        Query firstQuery = currentGameCollection.OrderByDescending("score").Limit(10);
        firstQuery.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) => {
            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents) {
                lastDocument = documentSnapshot.GetValue<int>("score");

                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user) {
                    // Debug.Log(pair.Key + " " + pair.Value);

                    if (pair.Key == "nickname") {
                        Debug.Log(pair.Key + " " + pair.Value);

                        if (fetchIndex < 3) {
                            PremiumRankCanvas.Instance.rankingTexts[fetchIndex].text = pair.Value + "";
                        }
                        else {
                            nickname = pair.Value.ToString();
                        }
                    }
                    else if (pair.Key == "score") {
                        Debug.Log(pair.Key + " " + pair.Value);

                        if (fetchIndex < 3) {
                            PremiumRankCanvas.Instance.rankingTexts[fetchIndex].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = pair.Value.ToString();
                        }
                        else {
                            PremiumRankCanvas.Instance.CreatePlayer(fetchIndex, nickname, pair.Value.ToString());
                        }

                        if (fetchIndex == 2)
                            fetchIndex++;

                        fetchIndex++;
                    }
                }
            }
        });
    }

    public void FetchNext() {
        string nickname = "";
        print("lastDocument: " + lastDocument);
        print("fetchIndex: " + fetchIndex);

        Query secondQuery = currentGameCollection.OrderByDescending("score").StartAfter(lastDocument).Limit(10);
        Task<QuerySnapshot> secondQuerySnapshot = secondQuery.GetSnapshotAsync();
        secondQuery.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) => {
            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents) {
                lastDocument = documentSnapshot.GetValue<int>("score");

                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user) {

                    if (pair.Key == "nickname") {
                        Debug.Log(pair.Key + " " + pair.Value);
                        nickname = pair.Value.ToString();
                    }
                    else if (pair.Key == "score") {
                        Debug.Log(pair.Key + " " + pair.Value);
                        PremiumRankCanvas.Instance.CreatePlayer(fetchIndex, nickname, pair.Value.ToString());

                        fetchIndex++;
                    }
                }
            }
        });
    }

    public void GetPreviousNickname()
    {
        //DAHA ÖNCE KAYITLI OLDUĞU BİR HESABA GİRİŞ YAPTIYSA VE -----TELEFONDA DEPOLANMIŞ KULLANICI ADI YOKSA(KAYDOLMADAN DİREKT GİRİŞ YAPMIŞSA) KALDIRILDI------FIRESTOREDAN NICKI ÇEK)

        Query nickname = users.WhereEqualTo("email", FirebaseAuthWithMail.Instance.emailLoginField.text);
        nickname.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) =>
        {
       //     Debug.Log(querySnapshotTask.Result.Documents.Count());

            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents)
            {
                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user)
                {
                    if (pair.Key == "nickname")
                    {
                        PlayerPrefs.SetString("username", pair.Value.ToString());
                        PlayerPrefs.SetString("email", FirebaseAuthWithMail.Instance.emailLoginField.text);

                        /* YENI SİSTEMLE GEREK KALMADI
                        //GAMES
                        if (UIController.instance != null) UIController.instance.nicknameText.text = pair.Value.ToString();
                        else if(Level_Manager_Script.instance != null) Level_Manager_Script.instance.nicknameText.text = pair.Value.ToString();
                        else if(UIController_BlockChaser.Instance != null) UIController_BlockChaser.Instance.nicknameText.text = pair.Value.ToString();
                        else if(UiController_SpaceOrbit.Instance != null) UiController_SpaceOrbit.Instance.nicknameText.text = pair.Value.ToString();
                        //    else
                        //    {       //NEW AUTOMOTIC SYSTEM AMA ÇALIŞMIYOR
                        //        Text nicknameTextInTheGame = GameObject.FindWithTag("NicknameText").GetComponent<Text>();
                        //        if(nicknameTextInTheGame != null) nicknameTextInTheGame.text = pair.Value.ToString();
                        //    }
                        */

                        Debug.Log("NICKNAME VERİTABANINDAN ÇEKİLDİ: " + pair.Value);
                    }
                }
            }
        });
    }

    public void ForceUpdate()
    {
        Query query = forceupdate;
        query.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) =>
        {
            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents)
            {
                Dictionary<string, object> user = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in user)
                {
                    if (pair.Key == "version")
                    {
                        GameManager.Instance.newVersion = float.Parse(pair.Value.ToString());
                        Debug.Log("YENİ SÜRÜM VAR: " + pair.Value);
                    }
                }
            }
        });
    }

    /* OLD SYSTEM
    public void RemainingTime()
    {
        Query query = remainingtime;
        query.GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) =>
        {
            foreach (DocumentSnapshot documentSnapshot in querySnapshotTask.Result.Documents)
            {
                Dictionary<string, object> time = documentSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in time)
                {

                    if (pair.Key == "yilaygun")
                    {
                        TimeManager.Instance.CreateDatabaseDate(pair.Value.ToString());
                        Debug.Log("Veritabanı Yeni Oyun Tarihi: " + pair.Value);
                    }

                }
            }
        });
    }
    */

    public void GetGameSettings(CurrentGameSettings gameSettingsAsset, Menu_GameArea requestingGamePanel)
    {
        gamesettings.Document(gameSettingsAsset.documentNameInGameSettings).GetSnapshotAsync().ContinueWithOnMainThread((querySnapshotTask) =>
        {
            Dictionary<string, object> settings = querySnapshotTask.Result.ToDictionary();
            foreach (KeyValuePair<string, object> pair in settings)
            {
                switch (pair.Key)
                {
                    case "start":
                        gameSettingsAsset.startTime = pair.Value.ToString();
                        break;
                    case "end":
                        gameSettingsAsset.endTime = pair.Value.ToString();
                        break;
                    case "whichcollection":
                        gameSettingsAsset.collectionNameForSaveScore = pair.Value.ToString();
                        break;
                }
            }

            requestingGamePanel.SetGameTimes();

        });
    }
}