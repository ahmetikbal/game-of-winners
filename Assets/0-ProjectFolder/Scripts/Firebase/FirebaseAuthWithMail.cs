﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using TMPro;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.Storage;
using MoreMountains.NiceVibrations;
using com.adjust.sdk;

public class FirebaseAuthWithMail : MonoBehaviour
{
    public static FirebaseAuthWithMail Instance;

    //Firebase variables
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser User;

    //Login variables
    [Header("Login")]
    public TMP_InputField emailLoginField;
    public TMP_InputField passwordLoginField;
    public TMP_Text statusLoginText;
 //   public TMP_Text confirmLoginText;

    //Register variables
    [Header("Register")]
    public TMP_InputField usernameRegisterField;
    public TMP_InputField emailRegisterField;
    public TMP_InputField passwordRegisterField;
    public TMP_InputField referenceCodeRegisterField;
 //   public TMP_InputField passwordRegisterVerifyField;
    public TMP_Text statusRegisterText;


    void Awake()
    {
        if (Instance == null) Instance = this;

        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //If they are avalible Initialize Firebase
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        //Set the authentication instance object
        auth = FirebaseAuth.DefaultInstance;
    }

    //Function for the login button
    public void LoginButton()
    {
        //Call the login coroutine passing the email and password
     //   StartCoroutine(Login(emailLoginField.text, passwordLoginField.text));     ŞİFRE KOYMA KALDIRILDI OTOMATİK 123456 VERİLİYOR TÜM KULLANICILARA
        StartCoroutine(Login(emailLoginField.text, "123456"));

        if(emailLoginField.text != "") FireStoreManager.Instance.GetPreviousNickname();
    }
    //Function for the register button
    public void RegisterButton()
    {
        //Call the register coroutine passing the email, password, and username
        //    StartCoroutine(Register(emailRegisterField.text, passwordRegisterField.text, usernameRegisterField.text));    ŞİFRE KOYMA KALDIRILDI OTOMATİK 123456 VERİLİYOR TÜM KULLANICILARA
        StartCoroutine(Register(emailRegisterField.text, "123456", usernameRegisterField.text));
    }
    public void ResetNicknameCheck()
    {
            FireStoreManager.Instance.queryFinished = false;
            FireStoreManager.Instance.nicknameIsTaken = true; // EĞER ALINMAMIŞSA FALSE OLACAK VE REGISTER NUMERATORI TEKRAR ÇALIŞACAK
    }
    private IEnumerator Login(string _email, string _password)
    {
        statusLoginText.text = LanguageManager.Instance.GetValue("loading"); //"Yükleniyor";

        //Call the Firebase auth signin function passing the email and password
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        //Wait until the task completes
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

        if (LoginTask.Exception != null)
        {
            //If there are errors handle them
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            string message = LanguageManager.Instance.GetValue("loginFailed"); //"Giriş Başarısız!";
            switch (errorCode)
            {
                case AuthError.MissingEmail:
                    message = LanguageManager.Instance.GetValue("missingEmail"); //"Email Giriniz";
                    break;
                case AuthError.MissingPassword:
                    message = LanguageManager.Instance.GetValue("missingPassword"); //"Şifre Giriniz";
                    break;
                case AuthError.WrongPassword:
                    message = LanguageManager.Instance.GetValue("wrongPassword"); //"Yanlış Şifre";
                    break;
                case AuthError.InvalidEmail:
                    message = LanguageManager.Instance.GetValue("invalidEmail"); //"Geçersiz Email";
                    break;
                case AuthError.UserNotFound:
                    message = LanguageManager.Instance.GetValue("userNotFound"); //"Hesap mevcut değil";
                    break;
            }
            if (Application.internetReachability == NetworkReachability.NotReachable) message = LanguageManager.Instance.GetValue("networkError");
            statusLoginText.text = message;
        }
        else
        {
            //User is now logged in
            //Now get the result
            User = LoginTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", User.DisplayName, User.Email);

            statusLoginText.color = Color.green;
            statusLoginText.text = LanguageManager.Instance.GetValue("succesfullyLoggedIn"); //"Giriş Başarılı!";
            //    confirmLoginText.text = "Logged In";

            /* LOGIN BUTONUNA TAŞINDI
            if (PlayerPrefs.HasKey("LoggedIn"))
            {
           //     if (!PlayerPrefs.HasKey("username"))
           //     {
                    //DAHA ÖNCE KAYITLI OLDUĞU BİR HESABA GİRİŞ YAPTIYSA VE ----kaldırıldı TELEFONDA DEPOLANMIŞ KULLANICI ADI YOKSA(KAYDOLMADAN DİREKT GİRİŞ YAPMIŞSA) ----- FIRESTOREDAN NICKI ÇEK)
                    FireStoreManager.Instance.GetPreviousNickname();
           //     }

            }
            */ 

            if (!PlayerPrefs.HasKey("LoggedIn"))
            {
                /*
                if (ObscuredPrefs.HasKey("Highscore"))
                {
                    FireStoreManager.Instance.CreateFlappyDunkScore();
                }
                else if (ObscuredPrefs.HasKey("High_Score_Index"))
                {
                    FireStoreManager.Instance.CreateWaterShooterScore();
                }
                else if (ObscuredPrefs.HasKey("BlockChaser_Highscore2"))
                {
                    FireStoreManager.Instance.CreateBlockChaserScore();
                }
                else if (ObscuredPrefs.HasKey("BestSavedScore3"))
                {
                    FireStoreManager.Instance.CreateSpaceOrbitScore(0);
                }
                */

                //GOWSETTINGSTEKİ TÜM ASSETLARIN İÇİNE GİRİP HEPSİ İÇİN KONTROL YAPILABİLİR
                if (ObscuredPrefs.HasKey(GameManager.Instance.currentHighScorePlayerPrefsName)) //NEW SYSTEM GENERAL FUNCTION
                {
                    FireStoreManager.Instance.CreateCurrentGameScore();
                }

                PlayerPrefs.SetInt("LoggedIn", 1);

            }

            OneSignal.SetEmail(_email);

            AdjustEvent adjustEvent = new AdjustEvent("mkxaaz");
            Adjust.trackEvent(adjustEvent);

            MMVibrationManager.Haptic(HapticTypes.Success);

            PlayerPrefs.SetString("email", _email);

            /*
            if (PlayerPrefs.GetString("email") == "test@medusa.games")
            {
                GameManager.Instance.currentGame = 1;
                PlayerPrefs.SetInt("currentGame", 1);
            }
            */

            //    Invoke("OpenGame", 1f);
            if (SceneManager.GetActiveScene().name == "MainMenu_Main")
                Invoke("OpenProfileArea", .5f);
            else
                GameManager.Instance.LoadCurrentGame();
        }
    }

   // public void OpenGame() { SceneManager.LoadScene(1); CancelInvoke("OpenGame"); }
    public void OpenProfileArea() { UIController_MainMenu.Instance.RegisterAndProfileButton(); CancelInvoke("OpenProfileArea"); }


    private IEnumerator Register(string _email, string _password, string _username)
    {
        statusRegisterText.text = LanguageManager.Instance.GetValue("loading"); //"Yükleniyor";

        if (_username == "")
        {
            //If the username field is blank show a warning
            statusRegisterText.text = LanguageManager.Instance.GetValue("emptyNickname"); //"Kullanıcı Adı Giriniz";
        }
        else if (_username.Length < 4 || _username.Length > 16)
        {
            statusRegisterText.text = LanguageManager.Instance.GetValue("nicknameWarning"); //"Kullanıcı Adı 4-16 Karakter Uzunluğunda Olmalıdır";
        }
        /*
        else if (passwordRegisterField.text != passwordRegisterVerifyField.text)
        {
            //If the password does not match show a warning
            statusRegisterText.text = "Password Does Not Match!";
        }
        */
        else
        {
            if(!FireStoreManager.Instance.queryFinished) 
                FireStoreManager.Instance.NicknameCheck(_username);

            if (FireStoreManager.Instance.queryFinished)
            {
                if (!FireStoreManager.Instance.nicknameIsTaken) { 

                //Call the Firebase auth signin function passing the email and password
                var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
                //Wait until the task completes
                yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

                    if (RegisterTask.Exception != null)
                    {
                        //If there are errors handle them
                        Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                        FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                        string message = LanguageManager.Instance.GetValue("registrationFailed"); //"Kayıt Başarısız!";
                        switch (errorCode)
                        {
                            case AuthError.MissingEmail:
                                message = LanguageManager.Instance.GetValue("missingEmail"); //"Email Giriniz";
                                break;
                            case AuthError.InvalidEmail:
                                message = LanguageManager.Instance.GetValue("invalidEmail"); //"Geçersiz Email";
                                break;
                            case AuthError.MissingPassword:
                                message = LanguageManager.Instance.GetValue("missingPassword"); //"Şifre Giriniz";
                                break;
                            case AuthError.WeakPassword:
                                message = "Güçsüz Şifre";
                                break;
                            case AuthError.EmailAlreadyInUse:
                                message = LanguageManager.Instance.GetValue("emailAlreadyInUse"); //"Email Zaten Kullanılıyor";
                                break;
                        }
                        if (Application.internetReachability == NetworkReachability.NotReachable) message = LanguageManager.Instance.GetValue("networkError");
                        statusRegisterText.text = message;
                    }
                    else
                    {
                        //User has now been created
                        //Now get the result
                        User = RegisterTask.Result;

                        if (User != null)
                        {
                            //Create a user profile and set the username
                            UserProfile profile = new UserProfile { DisplayName = _username };

                            //Call the Firebase auth update user profile function passing the profile with the username
                            var ProfileTask = User.UpdateUserProfileAsync(profile);
                            //Wait until the task completes
                            yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                            if (ProfileTask.Exception != null)
                            {
                                //If there are errors handle them
                                Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                                FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                          //      statusRegisterText.text = "Username Set Failed!";
                            }
                            else
                            {
                                //Username is now set

                                PlayerPrefs.SetString("username", _username);
                                PlayerPrefs.SetString("email", _email);
                                FireStoreManager.Instance.CreateUserInformations(); //ADD NEW USER WITH INFORMATIONS

                                statusRegisterText.color = Color.green;
                                statusRegisterText.text = LanguageManager.Instance.GetValue("registrationSuccessful"); //"Kayıt Başarılı!";

                                //Now return to login screen

                                //    Invoke("TurnLoginMenu", 1f);

                                AdjustEvent adjustEvent = new AdjustEvent("60ozwo");
                                Adjust.trackEvent(adjustEvent);

                                StartCoroutine(Login(PlayerPrefs.GetString("email"), "123456"));
                            }
                        }
                    }
                }
                else
                {
                    print("bu kullanıcı adı alındı kayıt yapılamıyor");
                }
            }
        }
    }

 //   public void TurnLoginMenu() { UIManager.Instance.BackToLoginAreaButton(); CancelInvoke("TurnLoginMenu"); }
}
